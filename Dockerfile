FROM node:lts-alpine
EXPOSE 3000:3000
WORKDIR /usr/src/app
COPY . .
#RUN npm install --loglevel error

# RUN wget http://192.168.225.247:48080/eab-121-conference-fe-nm.tar.gz -P /app
# RUN cd /app && tar zxf eab-121-conference-fe-nm.tar.gz && ls node_modules

RUN npm install --registry http://192.168.222.170:4873/

CMD ["npm", "run", "start"]
