import axios from "axios";

import config from "../config/config.json";

const eSignAxios = axios.create({
  baseURL: config.eSign.backend,
  headers: {
    "Content-Type": "application/json",
  },
});

const cafeAxios = axios.create({
  baseURL: config.cafe.apiHost,
  headers: {
    "Content-Type": "application/json",
  },
});

axios.defaults.baseURL = config.apiEndpoint;
axios.defaults.headers.Authorization = `Bearer ${localStorage.getItem(
  "token",
)}`;

axios.interceptors.request.use(
  config => {
    config.headers["Content-Type"] = "application/json";
    console.log(config);
    return config;
  },
  error => Promise.reject(error),
);

// User related
export function register({ userName, password, email, name }) {
  const data = {
    userName,
    password,
    email,
    name,
  };
  return axios.post(`/user/register`, data);
}

export function Login(data) {
  return axios
    .post(`/user/login`, data)
    .catch(error => ({ error: error.response.data }));
}

export function logout() {
  return axios.post(`/user/logout`);
}

export function invite({ applicationId, password, emails, session }) {
  const data = {
    applicationId,
    password,
    emails,
    session,
  };
  return axios.post(`/application/invite`, data);
}

export function submitApplication(applicationId) {
  console.log(applicationId);
  return axios.post(`/application/${applicationId}/submit`);
}

export function getApplicationNumber(sessionId) {
  return axios
    .get(`/application/session/${sessionId}`)
    .then(result => result)
    .catch(error => error.response);
}

export function resetPassword({ username }) {
  const data = {
    username,
  };
  return axios.put(`/user/login`, data);
}

export function resetProfilePassword({ newPassword }) {
  const data = {
    newPassword,
  };
  return axios.put(`/user/login`, data);
}

// Product related
export function getProductList() {
  return axios.get(`/product/list`);
}

// Application related
export function getApplicationList() {
  return axios.get(`/application/list`);
}

export function getApplicationDetail({ applicationId }) {
  return axios.get(`/application/${applicationId}`);
}

export function UploadApplicationProposal(productId, pdf) {
  const data = {
    productId,
    proposal: pdf,
  };
  return axios.post(`/application`, data);
}

export function deleteApplication(applicationId) {
  return axios.delete(`/application/${applicationId}`);
}

export function updatedApplication(
  applicationId,
  productId,
  formId,
  responseId,
  clientName,
  proposalPDFBase64,
  applicationPdfs,
  idCardFileBase64,
  addressFileBase64,
  status,
  cafe,
) {
  const data = {
    productId,
    cafe,
    clientName: `${cafe.NAME_LAST} ${cafe.NAME_FIRST}`,
    status,
    proposal: proposalPDFBase64,
    applicationPdfs,
    supportingDocuments: [
      {
        key: "idCardFile",
        data: idCardFileBase64,
      },
      {
        key: "DDRESSFile",
        data: addressFileBase64,
      },
    ],
  };
  console.log(data);
  console.log(applicationId);
  return axios.put(`/application/${applicationId}`, data);
}

export function applicationJoin(session, password) {
  const data = {
    session, // session === roomId
    password,
  };
  return axios
    .post(`/application/join`, data)
    .then(result => result)
    .catch(error => error.response);
}

// #region eSign Services
export function generatePdfByTemplate({ data }) {
  return eSignAxios.post(`/eSignature/session_request`, data);
}

export function getSignedPdf({ sessionId, docId }) {
  return eSignAxios.get(
    `eSignature/loadSignedPDF?sessionID=${sessionId}&docID=${docId}`,
    { responseType: "arraybuffer" },
  );
}
// #endregion

// #region Cafe Services
export function getCafeResponse({ responseId, publishFormId }) {
  return cafeAxios.post(`/response/getResponseDataToDP`, {
    responseId,
    publishFormId,
    // ownerID:
  });
}
// #endregion
