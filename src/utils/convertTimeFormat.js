const convertTimeFormat = milliseconds => {
  let seconds = (milliseconds / 1000).toFixed(2);
  let minutes = Math.floor(seconds / 60);
  let hours = "";

  hours = Math.floor(minutes / 60);
  hours = hours >= 10 ? hours : `0${hours}`;
  minutes = minutes - hours * 60;
  minutes = minutes >= 10 ? minutes : `0${minutes}`;

  seconds = Math.floor(seconds % 60);
  seconds = seconds >= 10 ? seconds : `0${seconds}`;
  return `${hours}:${minutes}:${seconds}`;
};

export default convertTimeFormat;
