import { createMuiTheme } from "@material-ui/core/styles";

// Here is the Theme of the Material UI.

// This function take the name of the variable in CSS.
export function getDefaultTheme(property) {
  return getComputedStyle(document.body)
    .getPropertyValue(property)
    .trim();
}

export default createMuiTheme({
  typography: {
    fontFamily: getDefaultTheme("--nf2f-font-family"),
    useNextVariants: true,
    h1: {
      fontSize: getDefaultTheme("--nf2f-fontSizeXL"),
      fontWeight: getDefaultTheme("--fontNormal"),
    },
    body1: {
      fontSize: getDefaultTheme("--nf2f-fontSizeM"),
    },
    button: {},
  },
  palette: {
    primary: {
      main: getDefaultTheme("--nf2f-primary-color"),
    },
    secondary: {
      main: getDefaultTheme("--nf2f-secondary-color"),
    },
    text: {
      white: getDefaultTheme("--nf2f-font-white"),
      black: getDefaultTheme("--nf2f-font-black"),
    },
  },
  overrides: {
    MuiButton: {
      contained: {
        padding: `${getDefaultTheme("--nf2f-padding16")} ${getDefaultTheme(
          "--nf2f-padding26",
        )}`,
        fontSize: getDefaultTheme("--nf2f-fontSizeS"),
        fontWeight: getDefaultTheme("--fontSemiBold"),
        borderRadius: getDefaultTheme("--nf2f-borderRadiusS"),
        "&.Mui-disabled": {
          background: getDefaultTheme("--nf2f-disable-color"),
          color: getDefaultTheme("--nf2f-color-white"),
        },
      },
      containedPrimary: {},
      outlined: {
        padding: `${getDefaultTheme("--nf2f-padding16")} ${getDefaultTheme(
          "--nf2f-padding26",
        )}`,
        fontSize: getDefaultTheme("--nf2f-fontSizeS"),
        fontWeight: getDefaultTheme("--fontSemiBold"),
        borderRadius: getDefaultTheme("--nf2f-borderRadiusS"),
      },
      outlinedPrimary: {},
      text: {
        padding: `${getDefaultTheme("--nf2f-padding16")} ${getDefaultTheme(
          "--nf2f-padding26",
        )}`,
        fontSize: getDefaultTheme("--nf2f-fontSizeS"),
        fontWeight: getDefaultTheme("--fontSemiBold"),
        borderRadius: getDefaultTheme("--nf2f-borderRadiusS"),
      },
    },
    MuiFab: {
      label: {
        fontSize: getDefaultTheme("--nf2f-fontSizeXS"),
        textTransform: "capitalize",
      },
    },
    MuiOutlinedInput: {
      root: {
        borderRadius: getDefaultTheme("--nf2f-borderRadiusS"),
        fontSize: getDefaultTheme("--nf2f-fontSizeM"),
      },
      adornedEnd: {
        paddingRight: 0,
      },
    },
    MuiDialog: {

    },
  },
});
