import attachment from "./attachment";
import page from "./page";
import plugin from "./plugin";

export default {
  page,
  plugin,
  attachment,
};
