/* Directory */
export const ROUTE_EXAMPLE = "/example";
export const ROUTE_AGENT = "/agent";
export const ROUTE_CLIENT = "/client";
export const ROUTE_EAPP = "/eapp";

/* Page */
export const ROUTE_COMPONENT = "/component";
export const ROUTE_LOGIN = "/login";
export const ROUTE_LANDING = "/landing";

export const ROUTE_APPLICATION = "/application";
export const ROUTE_SIGNATURE = "/sign";
export const ROUTE_SUMMARY = "/summary";
export const ROUTE_SUPPORTINGDOCUMENT = "/supportdocs";
export const ROUTE_UPLOADPROPOSAL = "/upload";
export const ROUTE_SUBMIT = "/submit";

export default {
  ROUTE_EXAMPLE,
  ROUTE_COMPONENT,
  ROUTE_LOGIN,
  ROUTE_AGENT,
  ROUTE_CLIENT,
  ROUTE_LANDING,
  ROUTE_APPLICATION,
  ROUTE_SIGNATURE,
  ROUTE_SUMMARY,
  ROUTE_SUPPORTINGDOCUMENT,
  ROUTE_UPLOADPROPOSAL,
  ROUTE_SUBMIT,
};
