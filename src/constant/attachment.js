export const PDF_PROPOSAL = "PROPOSAL";
export const PDF_DISCLOSURE = "DISCLOSURE";

export default {
  PDF_PROPOSAL,
  PDF_DISCLOSURE,
};
