import clsx from "clsx";
import React, { useEffect, useImperativeHandle, useRef } from "react";

// import config from "../../config/config.json";
import styles from "./SignViewer.module.css";

function SignViewer(props, ref) {
  const { className, dataSrc } = props;

  return (
    <object
      className={clsx(styles.container, className)}
      aria-label="pdfReader"
      type="application/pdf"
      data={`data:application/pdf;base64,${dataSrc}`}
    />
  );
}

const forwardRef = (props, ref) => SignViewer({ ...props }, ref);

export default React.forwardRef(forwardRef);
