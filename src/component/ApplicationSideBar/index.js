import {
  Button,
  Step,
  StepButton,
  StepConnector,
  StepContent,
  StepLabel,
  Stepper,
} from "@material-ui/core";
import clsx from "clsx";
import PropTypes from "prop-types";
import React, { useState } from "react";

import styles from "./ApplicationSideBar.module.css";

function ApplicationSideBar(props) {
  const {
    className,
    step: propsStep,
    dataList: propsDataList,
    // hasConnector,
    ...otherProps
  } = props;

  //   const [step, setStep] = useState(propsStep);
  //   const [dataList, setDataList] = useState(propsDataList);

  //   const changeStepHandler = newStep => {
  //     setStep(newStep);
  //   };

  const getStep = (item, index) => (
    <Step key={item.key}>
      <StepButton
        onClick={item.onClick}
        disabled={item.disabled}
        classes={item.classes}
        completed={item.completed}
      >
        {item.label}
      </StepButton>
      {/* <StepContent>
        <Button
          onClick={item.onClick}
          disabled={item.disabled}
          classes={item.classes}
        >
          ABC
        </Button>
      </StepContent> */}
    </Step>
  );

  return (
    <div className={clsx(styles.container, className)}>
      <Stepper
        connector={
          <StepConnector classes={{ lineVertical: styles.lineVertical }} />
        }
        {...otherProps}
      >
        {(propsDataList || []).map((item, index) => getStep(item, index))}
      </Stepper>
    </div>
  );
}

ApplicationSideBar.propTypes = {
  className: PropTypes.string,
  orientation: PropTypes.oneOf(["vertical", "horizontal"]),
  step: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  //   hasConnector: PropTypes.bool,
  dataList: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      classes: PropTypes.object,
      label: PropTypes.string.isRequired,
      disabled: PropTypes.bool,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      onClick: PropTypes.func,
    }),
  ),
};

ApplicationSideBar.defaultProps = {
  //   hasConnector: false,
  orientation: "vertical",
  step: 0,
};

export default ApplicationSideBar;
