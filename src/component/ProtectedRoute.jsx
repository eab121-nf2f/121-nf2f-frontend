import React from "react";
import { connect } from "react-redux";
import { Redirect, Route, withRouter } from "react-router-dom";

import {
  changeRoomIdAction,
  chnageClientLoginSuccessRedirectToAction,
} from "../action/socketAction";
import { ROUTE_AGENT, ROUTE_LOGIN } from "../constant/route";

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  identity: state.auth.identity,
  roomId: state.socket.roomId,
});

const mapDispatchToProps = dispatch => ({
  chnageClientLoginSuccessRedirectToHandler: redirectToURL => {
    dispatch(chnageClientLoginSuccessRedirectToAction(redirectToURL));
  },
  changeRoomIdHandler: roomId => {
    dispatch(changeRoomIdAction(roomId));
  },
});

const ProtectedRoute = ({ component: Component, ...restProps }) => (
  <Route
    {...restProps}
    render={routeProps =>
      (restProps.isAuthenticated ? (
        <Component {...routeProps} />
      ) : (
        <Redirect
          to={{ pathname: `/client/login/${restProps.roomId}` }}
        />
      ))
    }
  />
);

export default connect(mapStateToProps, mapDispatchToProps)(ProtectedRoute);
