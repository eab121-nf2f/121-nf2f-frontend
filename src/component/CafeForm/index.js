import { useTheme } from "@material-ui/core";
import CafeInterfaceEngine from "@eab-cafe/display";
import React from "react";

import config from "../../config/config.json";

const { cafe: cafeConfig } = config;

const cafeStaticProps = {
  isPreview: false,
  isReadonly: false,
  formID: "5e7814ead3f9f10019a2e3b2",
  apiHost: cafeConfig.apiHost,
  pageIndex: 0,
  showButtonControl: false,
  showLanguageSelector: false,
};

const debugProps = {
  onSave: ({ ...allProps } = {}) => {
    console.warn("CafeForm:: onSave:: ", allProps);
  },
  onDone: ({ ...allProps } = {}) => {
    console.warn("CafeForm:: onDone:: ", allProps);
  },
  onFormLoaded: ({ ...allProps } = {}) => {
    console.warn("CafeForm:: onFormLoaded:: ", allProps);
  },
};
function CafeFormEngine(props) {
  const theme = useTheme();
  return (
    <CafeInterfaceEngine
      {...cafeStaticProps}
      {...debugProps}
      {...props}
      theme={theme}
    />
  );
}

export default CafeFormEngine;
