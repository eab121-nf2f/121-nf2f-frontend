import {
  Button,
  ClickAwayListener,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@material-ui/core";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import clsx from "clsx";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";

import { logoutThunkAction } from "../../action/authAction";
import companyLogo from "../../assets/images/logo.png";
import { ReactComponent as LogoutIcon } from "../../assets/images/icons/logout.svg";
import { LOGOUT_DIALOG } from "../../constant/page";
import styles from "./Header.module.css";

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  username: state.auth.userInfo,
  identity: state.auth.identity,
});

const mapDispatchToProps = dispatch => ({
  logoutHandler: async () => await dispatch(logoutThunkAction()),
});

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLangDropdwonOpen: false,
      isLogoutDialogOpen: false,
      list: [
        { lang: "en_US", path: "en_US" },
        { lang: "zh_TW", path: "zh_TW" },
        { lang: "zh_CN", path: "zh_CN" },
      ],
    };
  }

  getLanguageLabel = () => {
    const { t, i18n } = this.props;
    if (i18n.language) {
      return t(`TOOLBAR.LANGUAGE.${i18n.language}`);
    }
    return t(`TOOLBAR.LANGUAGE.en_US`);
  };

  clickedLogout = async () => {
    console.log("clickedLogout");
    try {
      await this.props.logoutHandler();
      this.props.history.push("/agent/login");
    } catch (err) {
      console.log(err);
    }
  };

  openDropdown = () => {
    this.setState({
      isLangDropdwonOpen: !this.state.isLangDropdwonOpen,
    });
  };

  changeLanguageHandler = language => {
    const { i18n } = this.props;
    i18n.changeLanguage(language);
  };

  render() {
    const { isLogoutDialogOpen } = this.state;
    const { t } = this.props;

    const logoutDialog = (
      <Dialog
        onClose={this.onClickShareDialog}
        aria-labelledby="shareDialog-title"
        open={isLogoutDialogOpen}
        classes={{ root: styles.logoutDialog, paper: styles.logoutDialogPaper }}
        maxWidth={"md"}
      >
        <DialogTitle className={styles.logoutDialogTitle}>
          <Typography
            variant="h1"
            className={styles.logoutDialogTitleBold}
            display="inline"
          >
            {t(`${LOGOUT_DIALOG}.TITLE_BOLD`)}
          </Typography>
        </DialogTitle>
        <DialogContent className={styles.logoutDialogContent}>
          <div className={styles.shareDialogContentRow}>
            <div>
              <Typography
                className={styles.shareDialogTitleSemiBold}
                variant="body1"
              >
                {t(`${LOGOUT_DIALOG}.DESCRIPTION`)}
              </Typography>
            </div>
          </div>
        </DialogContent>
        <DialogActions className={styles.logoutDialogFooter}>
          <Button
            color="primary"
            variant="outlined"
            onClick={() => {
              this.setState({
                ...this.state,
                isLogoutDialogOpen: false,
              });
            }}
          >
            {t(`BUTTON.CANCEL`)}
          </Button>
          <Button
            color="primary"
            variant="contained"
            onClick={() => {
              this.clickedLogout();
              this.setState({
                ...this.state,
                isLogoutDialogOpen: false,
              });
            }}
          >
            {t(`BUTTON.LOGOUT`)}
          </Button>
        </DialogActions>
      </Dialog>
    );

    return (
      <>
        {logoutDialog}
        <div className={styles.ToolBarWrapper}>
          <div className={styles.LeftSection}>
            <Link>
              <img src={companyLogo} alt="logo" />
            </Link>
            <div className={styles.EBAConferenceTitle}>EAB Conference</div>
          </div>
          <div className={styles.RightSection}>
            {(this.props.isAuthenticated && this.props.identity === "agent") && (
              <div className={styles.Name}>
                <div>{this.props.username}</div>
                <div
                  onClick={() =>
                    this.setState({ ...this.state, isLogoutDialogOpen: true })
                  }
                >
                  <LogoutIcon />
                </div>
              </div>
            )}
            <ClickAwayListener
              mouseEvent="onMouseDown"
              onClickAway={() => {
                this.setState({
                  ...this.state,
                  isLangDropdwonOpen: false,
                });
              }}
            >
              <div className={styles.Language} onClick={this.openDropdown}>
                <div>
                  <div>{this.getLanguageLabel()}</div>
                  <div>
                    {this.state.isLangDropdwonOpen ? (
                      <ExpandLessIcon />
                    ) : (
                        <ExpandMoreIcon />
                      )}
                  </div>
                </div>
                {this.state.isLangDropdwonOpen && (
                  <ul>
                    {this.state.list.map((listItem, index) => (
                      <li
                        key={index}
                        onClick={() =>
                          this.changeLanguageHandler(listItem.lang)
                        }
                      >
                        <Typography>{t(`TOOLBAR.LANGUAGE.${listItem.path}`)}</Typography>
                      </li>
                    ))}
                  </ul>
                )}
              </div>
            </ClickAwayListener>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Header)),
);
