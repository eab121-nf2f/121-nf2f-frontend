import { useMediaQuery } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useEffect, useImperativeHandle, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { getDefaultTheme } from "../../theme/theme";

import { generatePdfByTemplate } from "../../api";
import config from "../../config/config.json";
import { PDF_DISCLOSURE, PDF_PROPOSAL } from "../../constant/attachment";

const { eSign: eSignConfig } = config;

function SignHandler(props, ref) {
  const { className, onSave, docID, dataSrc, signRules } = props;

  const isIPad = useMediaQuery(
    "(min-device-width : 768px) and (max-device-width : 1024px)",
  );
  const { i18n } = useTranslation();
  const frameRef = useRef(null);

  const [sessionID, setSessionID] = useState(null);
  const [isError, setError] = useState(null);

  const systemLanguage = i18n.language || "en_US";

  console.warn("SigHandler:: isIpad:: ", isIPad);
  useImperativeHandle(ref, () => frameRef);

  const openPdf = async () => {
    // ? We first create the session
    const formData = {
      localization: systemLanguage,
      binary: dataSrc,
      docID,
      signatureRules: signRules,
      customize: {
        localization: systemLanguage,
        fontFamily: getDefaultTheme("--nf2f-font-family"),
        canvasMaxWidth: isIPad ? 300 : 600,
        canvasMaxHeight: isIPad ? 200 : 400,
      },
    };
    const pdfResponse = await generatePdfByTemplate({
      data: formData,
    }).catch(error => ({ error }));
    if (pdfResponse.error) {
      setError(pdfResponse.error);
      return;
    }
    const { sessionID: resSessionID } = pdfResponse.data;
    console.warn(
      "SignHandler:: useEffect:: openPdf:: ",
      formData,
      sessionID,
      docID,
    );
    setSessionID(resSessionID);
    if (!frameRef.current) {
      return;
    }
    const esignatureFrontend = frameRef.current;
    esignatureFrontend.contentWindow.postMessage(
      { sessionID: resSessionID, docID },
      "*",
    );
  };

  useEffect(() => {
    console.warn(
      "SignHandler:: useEffect:: Language updated:: ",
      systemLanguage,
    );
    openPdf();
  }, [systemLanguage]);

  useEffect(() => {
    if (!sessionID) {
      return;
    }
    const onReceivedMessage = async event => {
      //   console.warn("Signature:: openPdf:: onReceivedMessage:: ", event);
      if (event.data.save && event.data.docId === docID) {
        onSave(sessionID);
      }
    };
    window.addEventListener("message", onReceivedMessage);
    return () => {
      window.removeEventListener("message", onReceivedMessage);
    };
  }, [sessionID]);

  return (
    <iframe
      ref={frameRef}
      title={"eSignature"}
      className={className}
      src={eSignConfig.frontend}
      // onLoad={openPdf}
    />
  );
}

SignHandler.propTypes = {
  className: PropTypes.string,
  onSave: PropTypes.func.isRequired,
  docID: PropTypes.oneOf([PDF_PROPOSAL, PDF_DISCLOSURE]).isRequired,
  dataSrc: PropTypes.string.isRequired,
  signRules: PropTypes.array.isRequired,
};

const forwardRef = (props, ref) => SignHandler({ ...props }, ref);

export default React.forwardRef(forwardRef);
