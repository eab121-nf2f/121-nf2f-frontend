import { Avatar, ClickAwayListener, Typography } from "@material-ui/core";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";

import { ReactComponent as AvatarIcon } from "../../assets/images/icons/avatar.svg";
import { ReactComponent as MoreVertIcon } from "../../assets/images/icons/moreVert.svg";
import { ReactComponent as TrashIcon } from "../../assets/images/icons/trash.svg";
import { getRandomInt } from "../../utils";
import styles from "./ClientCard.module.css";

class ClientCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpenDropdown: false,
      randomIndex: getRandomInt(1, 8),
    };
  }

  render() {
    const { t } = this.props;

    let statusFlag = null;
    switch (this.props.status) {
      case "I": {
        statusFlag = (
          <div className={[styles.Status, styles.InProgress].join(" ")}>
            {t("CLIENT_CARD.IN_PROGRESS")}
          </div>
        );
        break;
      }
      case "S": {
        statusFlag = (
          <div className={[styles.Status, styles.Submitted].join(" ")}>
            {t("CLIENT_CARD.SUBMITTED")}
          </div>
        );
        break;
      }
      case "D": {
        statusFlag = (
          <div className={[styles.Status, styles.Deleted].join(" ")}>
            {t("CLIENT_CARD.DELETED")}
          </div>
        );
        break;
      }
      default: {
        statusFlag = (
          <div className={[styles.Status, styles.InProgress].join(" ")}>
            {t("CLIENT_CARD.IN_PROGRESS")}
          </div>
        );
        break;
      }
    }

    const concatNames = "".concat(this.props.clientName).split(" ");
    const abbreviateNames = (concatNames || []).map(str => str[0]);
    abbreviateNames.length = 2;

    let advanceOptionList = [
      {
        key: "DELETE",
        label: t(`CLIENT_CARD.DELETE`),
        icon: <TrashIcon />,
        onClick: () => this.props.onClickDelete(),
        disabled: this.props.status !== "I",
        hidden: this.props.status !== "I",
      },
    ];

    advanceOptionList = advanceOptionList.filter(item => !item.hidden);

    return (
      <div className={styles.ClientCard}>
        <div className={styles.StatusWrapper}>
          {statusFlag}
          <ClickAwayListener
            mouseEvent="onMouseDown"
            onClickAway={() => {
              this.setState({
                ...this.state,
                isOpenDropdown: false,
              });
            }}
          >
            <div className={styles.AdvanceSetting}>
              <div
                onClick={() => {
                  this.setState({
                    ...this.state,
                    isOpenDropdown: !this.state.isOpenDropdown,
                  });
                }}
              >
                <MoreVertIcon className={styles.AdvanceExpandIcon} />
              </div>
              {this.state.isOpenDropdown && advanceOptionList.length > 0 && (
                <ul>
                  {advanceOptionList.map((listItem, index) => (
                    <li
                      key={index}
                      className={styles.advanceOptionItem}
                      onClick={() => {
                        if (listItem.disabled) {
                          return;
                        }
                        this.setState({
                          ...this.state,
                          isOpenDropdown: false,
                        });
                        listItem.onClick();
                      }}
                    >
                      {listItem.icon}
                      <Typography className={styles.advanceOptionText}>
                        {listItem.label}
                      </Typography>
                    </li>
                  ))}
                </ul>
              )}
            </div>
          </ClickAwayListener>
        </div>
        <div className={styles.ApplicationDetails}>
          <div className={styles.AvatarBox}>
            <Avatar
              classes={{ root: styles.Avatar }}
              style={{
                backgroundColor: `var(--nf2f-avatar-backgroundColor-${this.state.randomIndex})`,
                color: `var(--nf2f-avatar-textColor-${this.state.randomIndex})`,
              }}
            >
              {abbreviateNames.join("").toUpperCase()}
            </Avatar>
            {/* <AvatarIcon className={styles.Avatar} /> */}
          </div>
          <div className={styles.Content}>
            <div className={styles.ClientName}>{this.props.clientName}</div>
            <div className={styles.ProductName}>{this.props.productName}</div>
            <div className={styles.ContentItem}>
              <span className={styles.ItemTitle}>
                {t(
                  this.props.status === "S"
                    ? "CLIENT_CARD.POLICY_NUMBER"
                    : "CLIENT_CARD.APPLICATION_NUMBER",
                )}
              </span>
              <span className={styles.ItemValue}>
                {this.props.applicationNumber}
              </span>
            </div>
            {/* <div className={styles.ContentItem}>
              <span className={styles.ItemTitle}>
                {t("CLIENT_CARD.PRODUCT_NAME")}
              </span>
              <span className={styles.ItemValue}>{this.props.productName}</span>
            </div> */}
            <div className={styles.ContentItem}>
              <span className={styles.ItemTitle}>
                {t("CLIENT_CARD.CREATION_DATE")}
              </span>
              <span className={styles.ItemValue}>
                {this.props.creationDate}
              </span>
            </div>
            <div className={styles.ContentItem}>
              <span className={styles.ItemTitle}>
                {t("CLIENT_CARD.LAST_UPDATED_DATA")}
              </span>
              <span className={styles.ItemValue}>
                {this.props.lastUpdatedDate}
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(ClientCard);
