import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(
  theme => ({
    topBar: {
      position: "fixed",
      width: "100%",
      top: 0,
      height: 0,
      left: 0,
      textAlign: "center"
    },
    topBarContainer: {
      height: "inherit",
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
    },
    topBarItem: {
      height: "var(--nf2f-broadcastBar-height)",
      width: "auto",
      color: "var(--nf2f-color-white)",
      fontFamily: "var(--nf2f-font-family)",
      boxShadow: "0px 3px 6px #00000029",
      cursor: "pointer",
    },
    buttonLabel: {
      paddingLeft: "var(--nf2f-padding10)"
    },
    dropdown: {
      position: "relative",
      display: "inline-block",
    },
    dropdownButton: {
      height: "100%",
      // display: "flex",
      alignItems: "center",
      padding: "0px var(--nf2f-padding16)"
    },
    stopSharingcontainer: {
      background: "var(--nf2f-pause-color)"
    },
    participantContainer: {
      backgroundColor: "var(--nf2f-color-black)"
    },
    dropdownContent: {
      display: "none",
      position: "absolute",
      background: "var(--nf2f-color-black)",
      minWidth: 220,
      boxShadow: "0px 8px 16px 0px rgba(0,0,0,0.2)",
      zIndex: 1,
      // left: -8
    },
    showDropdownContent: {
      display: "block",
    },
    dropdownItem: {
      color: "var(--nf2f-font-white)",
      height: "var(--nf2f-broadcastBar-height)",
      display: "flex",
      flexDirection: "row",
      justifyContent: 'space-between',
      alignItems: "center",
      margin: "0px var(--nf2f-padding16)"
    },
    dropdownTitle: {
      fontSize: "var(--nf2f-fontSizeS)"
    },
    dropdownAction: {
      fontSize: "var(--nf2f-fontSizeXS)"
    },
    middle: {
      height: 'inherit',
      verticalAlign: "middle"
    }
  }),
  {
    name: "BroadcastTopBar_JSS",
  },
);

export default useStyles;
