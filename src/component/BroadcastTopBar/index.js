import { Button } from "@material-ui/core";
import React, { cloneElement, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import { useSelector } from "react-redux";

import { ReactComponent as StopIcon } from "../../assets/images/icons/stop.svg";
import { ReactComponent as PeopleIcon } from "../../assets/images/icons/people.svg";

import { FAB_ICON } from "../../constant/component";
import useStyles from "./style";
import { eventEmitList } from "../../sockects/wsBroadTopbar";

function BroadcastTopbar(props) {
  const { onClickStopShare, onClickPassControl, participantList = ["a"] } = props;
  const { t } = useTranslation();
  const styles = useStyles();
  const ws = useSelector(state => state.ws);
  const socket = useSelector(state => state.socket);

  const [showMenu, setShowMenu] = useState(false);
  const [clientNowHaveContol, setClientNowHaveContol] = useState(false);

  const onMouseOverParticipantButton = () => {
    setShowMenu(true);
  };

  useEffect(() => {
    const data = {
      roomId: socket.roomId,
    }
    console.log(data);
    ws.wsBroadTopBar.emit(eventEmitList.JOIN_NSP, data);
  }, []);

  const closeShareModeHandler = () => {
    console.log("closeShareModeEvent");
    const data = {
      roomId: socket.roomId,
      closeShareModeEvent: "closeShareModeEvent",
    };
    console.log(ws);
    console.log(ws.wsBroadTopBar);
    ws.wsBroadTopBar.emit(eventEmitList.CLOSE_CLIENT_SHARE_MODE, data);
  };

  const startControlModeHandler = () => {
    console.log("startControlModeEvent");
    const data = {
      roomId: socket.roomId,
      startShareModeEvent: "startControlModeEvent",
    };
    ws.wsBroadTopBar.emit(eventEmitList.START_CLIENT_CONTROL_MODE, data);
  };

  const endControlModeHandler = () => {
    console.log("endControlModeEvent");
    const data = {
      roomId: socket.roomId,
      startShareModeEvent: "endControlModeEvent",
    };
    ws.wsBroadTopBar.emit(eventEmitList.END_CLIENT_CONTROL_MODE, data);
  };

  const participantListJSX = (participantList || []).map((peopleItem) => (
    <div className={styles.dropdownItem}>
      <div className={styles.dropdownTitle}>{t("BROADCAST_TOPBAR.CUSTOMER")}</div>
      <div className={styles.dropdownAction}>{t("BROADCAST_TOPBAR.CONTROL")}</div>
    </div>
  ))

  return (
    <div className={styles.topBar}>
      <div className={styles.topBarContainer}>
        <div className={clsx(styles.topBarItem, styles.dropdown, styles.stopSharingcontainer)} onClick={onClickStopShare}>
          <div className={styles.dropdownButton} onClick={closeShareModeHandler}>
            <StopIcon className={styles.middle} />
            <span className={clsx(styles.buttonLabel, styles.middle)}>Stop Sharing</span>
          </div>
        </div>
        <div className={clsx(styles.topBarItem, styles.dropdown, styles.participantContainer)} onMouseEnter={onMouseOverParticipantButton}
          onMouseLeave={() => setShowMenu(false)}
        >
          <div className={styles.dropdownButton}>
            <PeopleIcon className={styles.middle} />
            <span className={clsx(styles.buttonLabel, styles.middle)}>Participants</span>
          </div>
          <div 
            className={clsx(styles.dropdownContent, {
            [styles.showDropdownContent]: showMenu
            })}
            onClick={() => {
              if (clientNowHaveContol) {
                endControlModeHandler();
                setClientNowHaveContol(false);
              } else {
                startControlModeHandler();
                setClientNowHaveContol(true);
              }
            }}
          >
            {participantListJSX}
          </div>
        </div>

      </div>
    </div>
  );
}

export default BroadcastTopbar;
