import CheckIcon from "@material-ui/icons/Check";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { withTranslation } from "react-i18next";
import webSocket from "socket.io-client";

import { chnageStepNumberAction } from "../../action/stepperAction";
import config from "../../config/config.json";
import styles from "./ApplicationStepper.module.css";

const mapStateToProps = state => ({
  roomId: state.socket.roomId,
  isEnableStepUploadPropsoal: state.stepper.isEnableStepUploadPropsoal,
  isEnableStepApplication: state.stepper.isEnableStepApplication,
  isEnableStepSignature: state.stepper.isEnableStepSignature,
  isEnableStepSupportDoc: state.stepper.isEnableStepSupportDoc,
  isEnableStepSubmit: state.stepper.isEnableStepSubmit,
  stepNumber: state.stepper.stepNumber,
  identity: state.auth.identity,
});

const mapDispatchToProps = dispatch => ({
  chnageStepNumberHandler: number => {
    dispatch(chnageStepNumberAction(number));
  },
});

class ApplicationStepper extends Component {
  constructor(props) {
    super(props);
    let stepNum = 1;
    const path = props.location.pathname;
    if (path.includes("/eapp/upload/")) {
      stepNum = 1;
    } else if (path.includes("/eapp/application/")) {
      stepNum = 2;
    } else if (path.includes("/eapp/sign/")) {
      stepNum = 3;
    } else if (path.includes("/eapp/supportdocs/")) {
      stepNum = 4;
    } else if (path.includes("/eapp/submit/")) {
      stepNum = 5;
    } else {
      stepNum = 1;
    }
    this.state = {
      onStep: stepNum,
      ws: null,
    };
  }

  componentDidMount = () => {
    this.connectWebSocket();
  };

  connectWebSocket = () => {
    const webSocketInstance = webSocket(
      `${config.socketHost}/eapp/applicationStepper`,
    );
    const data = {
      username: this.state.username,
      roomId: this.props.roomId,
    };
    webSocketInstance.emit("joinRoom", data);

    webSocketInstance.on("newClientJoin", data => {
      console.log(data);
    });

    webSocketInstance.on("changeURLRouteEventToOther", data => {
      console.log(data);
      this.setState({ onStep: data.onStep });
      this.props.chnageStepNumberHandler(data.onStep);
      this.props.history.push(data.urlRoute);
    });

    this.setState({ ws: webSocketInstance });
  };

  changeStepHandler = step => {
    console.log(step);
    this.props.chnageStepNumberHandler(step);
    this.setState({ onStep: step });
    switch (step) {
      case 1: {
        const data = {
          roomId: this.props.roomId,
          onStep: 1,
          urlRoute: `/eapp/upload/${this.props.roomId}`,
        };
        this.state.ws.emit("changeURLRouteEvent", data);
        break;
      }
      case 2: {
        const data = {
          roomId: this.props.roomId,
          onStep: 2,
          urlRoute: `/eapp/application/${this.props.roomId}`,
        };
        this.state.ws.emit("changeURLRouteEvent", data);
        break;
      }
      case 3: {
        const data = {
          roomId: this.props.roomId,
          onStep: 3,
          urlRoute: `/eapp/sign/${this.props.roomId}`,
        };
        this.state.ws.emit("changeURLRouteEvent", data);
        break;
      }
      case 4: {
        const data = {
          roomId: this.props.roomId,
          onStep: 4,
          urlRoute: `/eapp/supportdocs/${this.props.roomId}`,
        };
        this.state.ws.emit("changeURLRouteEvent", data);
        break;
      }
      case 5: {
        const data = {
          roomId: this.props.roomId,
          onStep: 5,
          urlRoute: `/eapp/submit/${this.props.roomId}`,
        };
        this.state.ws.emit("changeURLRouteEvent", data);
        break;
      }
      default: {
        const data = {
          roomId: this.props.roomId,
          onStep: 1,
          urlRoute: `/eapp/upload/${this.props.roomId}`,
        };
        this.state.ws.emit("changeURLRouteEvent", data);
        break;
      }
    }
  };

  render() {
    const { t } = this.props;

    return (
      <div className={styles.ApplicationStepper}>
        {this.props.isEnableStepUploadPropsoal && this.props.identity === "agent" ? (
          <Link
            to={`/eapp/upload/${this.props.roomId}`}
            className={styles.Link}
          >
            <div
              className={styles.ApplicationStep}
              onClick={() => this.changeStepHandler(1)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 1 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 1 ? <CheckIcon className={styles.checkIcon}  /> : 1}
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 1 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_1`)}
              </div>
            </div>
          </Link>
        ) : (
          <Link className={styles.Link}>
            <div
              className={styles.ApplicationStep}
              // onClick={() => this.changeStepHandler(1)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 1 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 1 ? <CheckIcon className={styles.checkIcon}  /> : 1 }
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 1 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_1`)}
              </div>
            </div>
          </Link>
        )}

        <div className={styles.LineBreak}></div>

        {this.props.isEnableStepApplication && this.props.identity === "agent" ? (
          <Link
            to={`/eapp/application/${this.props.roomId}`}
            className={styles.Link}
          >
            <div
              className={styles.ApplicationStep}
              onClick={() => this.changeStepHandler(2)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 2 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 2 ? <CheckIcon className={styles.checkIcon}  /> : 2 }
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 2 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_2`)}
              </div>
            </div>
          </Link>
        ) : (
          <Link className={styles.Link}>
            <div
              className={styles.ApplicationStep}
              // onClick={() => this.changeStepHandler(2)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 2 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 2 ? <CheckIcon className={styles.checkIcon}  /> : 2 }
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 2 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_2`)}
              </div>
            </div>
          </Link>
        )}

        <div className={styles.LineBreak}></div>

        {this.props.isEnableStepSignature && this.props.identity === "agent" ? (
          <Link to={`/eapp/sign/${this.props.roomId}`} className={styles.Link}>
            <div
              className={styles.ApplicationStep}
              onClick={() => this.changeStepHandler(3)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 3 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 3 ? <CheckIcon className={styles.checkIcon}  /> : 3 }
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 3 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_3`)}
              </div>
            </div>
          </Link>
        ) : (
          <Link className={styles.Link}>
            <div
              className={styles.ApplicationStep}
              // onClick={() => this.changeStepHandler(3)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 3 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 3 ? <CheckIcon className={styles.checkIcon}  /> : 3 } 
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 3 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_3`)}
              </div>
            </div>
          </Link>
        )}

        <div className={styles.LineBreak}></div>

        {this.props.isEnableStepSupportDoc && this.props.identity === "agent" ? (
          <Link
            to={`/eapp/supportdocs/${this.props.roomId}`}
            className={styles.Link}
          >
            <div
              className={styles.ApplicationStep}
              onClick={() => this.changeStepHandler(4)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 4 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 4 ? <CheckIcon className={styles.checkIcon}  /> : 4 }
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 4 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_4`)}
              </div>
            </div>
          </Link>
        ) : (
          <Link className={styles.Link}>
            <div
              className={styles.ApplicationStep}
              // onClick={() => this.changeStepHandler(4)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 4 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 4 ? <CheckIcon className={styles.checkIcon}  /> : 4 }
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 4 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_4`)}
              </div>
            </div>
          </Link>
        )}

        <div className={styles.LineBreak}></div>

        {this.props.isEnableStepSubmit && this.props.identity === "agent" ? (
          <Link
            to={`/eapp/submit/${this.props.roomId}`}
            className={styles.Link}
          >
            <div
              className={styles.ApplicationStep}
              onClick={() => this.changeStepHandler(5)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 5 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 5 ? <CheckIcon className={styles.checkIcon}  /> : 5 } 
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 5 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_5`)}
              </div>
            </div>
          </Link>
        ) : (
          <Link className={styles.Link}>
            <div
              className={styles.ApplicationStep}
              // onClick={() => this.changeStepHandler(5)}
            >
              <div
                className={[
                  styles.Number,
                  this.props.stepNumber >= 5 ? styles.NumberFinish : null,
                ].join(" ")}
              >
                {this.props.stepNumber > 5 ? <CheckIcon className={styles.checkIcon}  /> : 5 }
              </div>
              <div
                className={[
                  styles.Title,
                  this.props.stepNumber >= 5 ? styles.TitleFinish : null,
                ].join(" ")}
              >
                {t(`APPLICATION_STEPPER.STEP_5`)}
              </div>
            </div>
          </Link>
        )}
      </div>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withTranslation()(ApplicationStepper)),
);
