import React, { Component } from 'react';
import socketIoClient from 'socket.io-client';

class TestComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      socket: null,
      response: 0,
      roomId: "aaaaa",
      endpoint: "http://127.0.0.1:3000"
    }
    this.sendMessage = this.sendMessage.bind(this);
  }

  componentDidMount() {
    const { endpoint } = this.state;
    const socketInstance = socketIoClient(endpoint);
    socketInstance.emit("joinExistingRoom", { room: "aaaaa" });
    socketInstance.on("sendMessage", (data) => {
      console.log(data);
      this.setState({ response: data });
    });
    this.setState({
      socket: socketInstance
    })
  }

  sendMessage() {
    const data = {
      room: this.state.roomId,
      msg: "I am test message"
    }
    this.state.socket.emit("sendMessage", data);
  }

  render() {
    return (
      <div>
        <div>{this.state.response}</div>
        <button onClick={this.sendMessage}>send message</button>
      </div>
    )
  }
}

export default TestComponent;