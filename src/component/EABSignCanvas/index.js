import { Button } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import classnames from "classnames";
// import { MEDIUM } from "constants/eabComponents";
// import CafeProps from "context";
import _ from "lodash";
import PropTypes from "prop-types";
import React, { useContext, useEffect, useState } from "react";
import SignaturePad from "react-signature-canvas";

import styles from "./EABSignCanvas.module.css";
function EABSignCanvas(props) {
  const { onChange, onClose, fromDataURL, ...otherProps } = props;
  const sigPad = React.createRef();
  const [isClear, setClear] = useState(true);
  // const { i18n } = useContext(CafeProps);
  useEffect(() => {
    if (fromDataURL !== null && !_.isEmpty(sigPad)) {
      sigPad.current.fromDataURL(fromDataURL);
      setClear(false);
    }
  }, [fromDataURL]);

  const clear = () => {
    setClear(true);
    sigPad.current.clear();
  };
  const signBegin = () => {
    setClear(false);
  };
  const signEnd = () => {
    setClear(false);
  };
  const trim = () => {
    if (onChange) {
      if (isClear) {
        onChange(null);
      } else {
        // onChange(sigPad.getTrimmedCanvas().toDataURL("image/png"));
        onChange(sigPad.current.getSignaturePad().toDataURL("image/png"));
      }
    }
    if (onClose) {
      onClose();
    }
  };
  const close = () => {
    if (onClose) {
      onClose();
    }
  };
  return (
    <div className={styles.content}>
      <div className={styles.topBar}>
        <ClearIcon className={styles.clearIcon} onClick={close} />

        <Button
          // size={MEDIUM}
          variant="contained"
          // className={styles.clearBtnText}
          onClick={() => clear()}
        >
          {/* {i18n({ path: "button.clear" })} */}
          Clear
        </Button>
        <div className={styles.handwritingTitle}>
          {/* {i18n({ path: "formEdit.basicQuestion.handwriting.signTitle" })} */}
          Please Sign
        </div>
        <Button
          // size={MEDIUM}
          variant="contained"
          color="secondary"
          // className={classnames(styles.btnTxt)}
          onClick={() => trim()}
        >
          {/* {i18n({ path: "button.done" })} */}
          Done
        </Button>
      </div>
      <div className={styles.sigContainer}>
        <SignaturePad
          {...otherProps}
          ref={sigPad}
          penColor="black" // color default black
          minWidth={0.5}
          maxWidth={2.5} // size default max 2.5 and min 0.5
          onEnd={signEnd}
          onBegin={signBegin}
        />
      </div>
    </div>
  );
}

EABSignCanvas.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func,
  fromDataURL: PropTypes.any,
};

EABSignCanvas.defaultProps = {
  className: "",
  onClose: () => {},
  fromDataURL: null,
};

export default EABSignCanvas;
