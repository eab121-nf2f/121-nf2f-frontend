import { Button, TextField } from "@material-ui/core";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";

import { ReactComponent as CloseIcon } from "../../assets/images/icons/close.svg";
import styles from "./ResetPasswordDialog.module.css";

class ResetPasswordDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      confirmPassword: "",
    };
  }

  render() {
    const { t } = this.props;
    return (
      <div className={styles.ResetPasswordDialog}>
        <div className={styles.CloseIcon} onClick={this.props.close}>
          <CloseIcon />
        </div>
        <div className={styles.Wrapper}>
          <div className={styles.Title}>
            <span>{t("RESET_PASSWORD_DIALOG.TITLE_BOLD")}</span>
            <span>{t("RESET_PASSWORD_DIALOG.TITLE_NORMAL")}</span>
          </div>
          <div className={styles.Subtitle}>
            <p>{t("RESET_PASSWORD_DIALOG.SUBTITLE")}</p>
          </div>
          <div className={styles.InputBox}>
            <TextField
              fullWidth
              variant="outlined"
              color="secondary"
              value={this.state.password}
              onChange={this.changePasswordHandler}
              placeholder={t("RESET_PASSWORD_DIALOG.PASSWORD_PLACEHOLDER")}
            />
          </div>
          <div className={styles.InputBox}>
            <TextField
              fullWidth
              variant="outlined"
              color="secondary"
              value={this.state.password}
              onChange={this.changePasswordHandler}
              placeholder={t(
                "RESET_PASSWORD_DIALOG.CONFIRM_PASSWORD_PLACEHOLDER",
              )}
            />
          </div>
          <Button
            className={styles.ConfirmButton}
            color="primary"
            variant="contained"
            onClick={this.loginHandler}
          >
            {t("RESET_PASSWORD_DIALOG.BUTTON_CONFIRM")}
          </Button>
        </div>
      </div>
    );
  }
}

export default withTranslation()(ResetPasswordDialog);
