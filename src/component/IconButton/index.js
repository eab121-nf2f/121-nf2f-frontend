import { Button as MIconButton } from "@material-ui/core";
import clsx from "clsx";
import React, { cloneElement } from "react";

import { FAB_ICON } from "../../constant/component";
import useStyles from "./style";

function IconButton(props) {
  const {
    styleType,
    transparent,
    color = "secondary",
    variant,
    ...otherProps
  } = props;
  const styles = useStyles();

  return (
    <MIconButton
      classes={{
        root: styles.iconButton,
        label: clsx({
          [styles.iconButtonTransparentLabel]: transparent,
        }),
      }}
      color={color}
      variant={transparent ? "text" : "contained"}
      {...props}
    />
  );
}

export default IconButton;
