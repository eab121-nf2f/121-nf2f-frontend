import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(
  theme => ({
    iconButton: {
      padding: "var(--nf2f-padding16)",
      borderTopLeftRadius: 0,
      borderBottomLeftRadius: 0,
      paddingTop: "var(--nf2f-padding20)",
      paddingBottom: "var(--nf2f-padding20)",
      minWidth: "auto",
    },
    // ? Dirty override
    iconButtonTransparentLabel: {
      "&>svg>path": {
        fill: "inherit",
      },
    },
  }),
  {
    name: "Fab_JSS",
  },
);

export default useStyles;
