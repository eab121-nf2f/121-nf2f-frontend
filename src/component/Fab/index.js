import { Fab as MFab } from "@material-ui/core";
import React, { cloneElement } from "react";

import { FAB_ICON } from "../../constant/component";
import useStyles from "./style";

function Fab(props) {
  const { styleType, children, icon, ...otherProps } = props;
  const styles = useStyles();

  const wrapIcon = icon =>
    cloneElement(icon, {
      className: styles.icon,
    });
  switch (styleType) {
    case FAB_ICON:
      return (
        <MFab
          color="secondary"
          classes={{ root: styles.fabArea, label: styles.flexContainer }}
          {...otherProps}
        >
          {wrapIcon(icon)}
          {children}
        </MFab>
      );
    default:
      return <MFab {...props} />;
  }
}

export default Fab;
