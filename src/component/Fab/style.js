import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(
  theme => ({
    flexContainer: {
      display: "flex",
      flexDirection: "column",
    },
    fabArea: {
      height: 75,
      width: 75,
    },
    icon: {
      height: 25,
      width: 25,
    },
  }),
  {
    name: "Fab_JSS",
  },
);

export default useStyles;
