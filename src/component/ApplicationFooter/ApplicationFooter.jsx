import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  InputAdornment,
  Popper,
  TextField,
  Typography,
} from "@material-ui/core";
import { responsiveFontSizes, ThemeProvider } from "@material-ui/core/styles";
import ChatIcon from "@material-ui/icons/Chat";
import CloseIcon from "@material-ui/icons/Close";
import ShareIcon from "@material-ui/icons/Share";
import clsx from "clsx";
import React, { Component, createRef } from "react";
import {
  addResponseMessage,
  dropMessages,
  Widget as ChatWidget,
} from "react-chat-widget";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import webSocket from "socket.io-client";

import {
  changePolicyNumberAction,
  onApplicationSubmitIntercept,
  onSendInvitationIntercept,
  uploadProposalActionThunk,
} from "../../action/applicationAction";
import { EAPP_ACTION_CHAT_SET } from "../../action/chatAction";
import {
  endShareModeAction,
  startShareModeAction,
} from "../../action/socketAction";
import { chnageStepNumberAction } from "../../action/stepperAction";
import { submitApplication, updatedApplication } from "../../api";
import { ReactComponent as LinkIcon } from "../../assets/images/icons/link.svg";
import { ReactComponent as PlusIcon } from "../../assets/images/icons/plus.svg";
import { Fab, IconButton } from "../../component";
import config from "../../config/config.json";
import { FAB_ICON } from "../../constant/component";
import { INVITE_DIALOG } from "../../constant/page";
import { CHAT } from "../../constant/plugin";
import theme from "../../theme/theme";
import randomRoomId from "../../utils/randomRoomId";
import styles from "./ApplicationFooter.module.css";

const mapStateToProps = state => ({
  applicationId: state.application.applicationId,
  productId: state.application.productId,
  cafe: state.cafe,
  chat: state.chat,
  formId: state.cafe.formId,
  responseId: state.cafe.responseId,
  clientName: state.application.clientName,
  proposalPDFBase64: state.application.proposalPDFBase64,
  applicationPdfs: state.application.applicationPdfs,
  idCardFileBase64: state.application.idCardFileBase64,
  addressFileBase64: state.application.addressFileBase64,
  status: state.application.status,
  roomId: state.socket.roomId,
  isEnableNextUploadPropsoal: state.nextBtn.isEnableNextUploadPropsoal,
  isEnableNextApplication: state.nextBtn.isEnableNextApplication,
  isEnableNextSignature: state.nextBtn.isEnableNextSignature,
  isEnableNextSupportDoc: state.nextBtn.isEnableNextSupportDoc,
  isEnableNextSubmit: state.nextBtn.isEnableNextSubmit,
  onClick: state.buttonAction.onClick,
  shareMode: state.socket.shareMode,
  identity: state.auth.identity,
});

// helping function
const pathToStepNumber = goToURL => {
  if (goToURL.includes("/eapp/upload/")) {
    return 1;
  } else if (goToURL.includes("/eapp/application/")) {
    return 2;
  } else if (goToURL.includes("/eapp/sign/")) {
    return 3;
  } else if (goToURL.includes("/eapp/supportdocs/")) {
    return 4;
  } else if (goToURL.includes("/eapp/submit/")) {
    return 5;
  } else {
    return 1;
  }
};

const mapDispatchToProps = dispatch => ({
  uploadProposalHanlder: (productId, proposalBase64) => {
    dispatch(uploadProposalActionThunk(productId, proposalBase64));
  },
  sendInvitationHandler: ({ password, emails }) => {
    dispatch(onSendInvitationIntercept({ password, emails }));
  },
  submitApplication: () => {
    dispatch(onApplicationSubmitIntercept());
  },
  changeStepNumberHandler: goToURL => {
    const stepNumber = pathToStepNumber(goToURL);
    console.log(stepNumber);
    dispatch(chnageStepNumberAction(stepNumber));
  },
  startShareModeHandler: () => {
    dispatch(startShareModeAction());
  },
  endShareModeHandler: () => {
    dispatch(endShareModeAction());
  },
  changePolicyNumberHandler: policyNumber => {
    dispatch(changePolicyNumberAction(policyNumber));
  },
  resetChat: () => {
    dispatch({
      type: EAPP_ACTION_CHAT_SET,
      nextReset: false,
    });
  },
});

class ApplicationFooter extends Component {
  constructor() {
    super();
    this.state = {
      isShareDialogOpen: false,
      password: this.generateRandomPassword(6),
      email: "",
      emailList: [],
      anchorEl: false,
      passwordLengthError: false,
      passwordStrError: false,
      emailStrError: false,
    };
    this.linkRef = createRef();
    this.webSocketInstance = webSocket(`${config.socketHost}/eapp/chat`);
  }

  onClickShareDialog = () => {
    const { isShareDialogOpen } = this.state;
    this.setState({
      isShareDialogOpen: !isShareDialogOpen,
    });
  };

  onClickRemoveEmail = index => () => {
    const { emailList } = this.state;
    emailList.splice(index, 1);
    this.setState({
      ...this.state,
      emailList,
    });
  };

  onClickRemoveChats = () => {
    dropMessages();
  };

  generateRandomPassword = (length = 6) => {
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/;
    let randomPassword = "";
    do {
      randomPassword = randomRoomId(length);
    } while (!passwordRegex.test(randomPassword));
    console.warn("generateRandomPassword:: ", randomPassword);
    return randomPassword;
  };

  copyLink = () => {
    const copyText = this.linkRef;
    copyText.select();
    // copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    alert(`Copied the text: ${copyText.value}`);
  };

  clickSaveHandler = async () => {
    switch (this.props.location.pathname) {
      case `/eapp/upload/${this.props.roomId}`: {
        try {
          console.log(this.props);
          const { productId, proposalPDFBase64 } = this.props;
          console.log(productId, proposalPDFBase64);
          await this.props.uploadProposalHanlder(productId, proposalPDFBase64);
        } catch (err) {
          console.log(err);
        }
        break;
      }
      case `/eapp/submit/${this.props.roomId}`:
      case `/eapp/sign/${this.props.roomId}`: {
        try {
          const {
            applicationId,
            productId,
            formId,
            responseId,
            cafe,
            clientName,
            proposalPDFBase64,
            applicationPdfs,
            idCardFileBase64,
            addressFileBase64,
            status,
          } = this.props;
          updatedApplication(
            applicationId,
            productId,
            formId,
            responseId,
            clientName,
            proposalPDFBase64,
            applicationPdfs,
            idCardFileBase64,
            addressFileBase64,
            status,
            cafe,
          )
            .then(res => {
              console.log(res);
            })
            .catch(err => {
              console.log(err);
            });
        } catch (err) {
          console.log(err);
        }
        break;
      }
      case `/eapp/application/${this.props.roomId}`: {
        this.props.onClick();
        break;
      }
      case `/eapp/supportdocs/${this.props.roomId}`:
        try {
          console.log(this.props.applicationId);
          const {
            applicationId,
            productId,
            formId,
            responseId,
            cafe,
            clientName,
            proposalPDFBase64,
            applicationPdfs,
            idCardFileBase64,
            addressFileBase64,
            status,
          } = this.props;
          await updatedApplication(
            applicationId,
            productId,
            formId,
            responseId,
            clientName,
            proposalPDFBase64,
            applicationPdfs,
            idCardFileBase64,
            addressFileBase64,
            status,
            cafe,
          );
          const res = await submitApplication(this.props.applicationId);
          console.log("ApplicationFooter:: submitApplication:: ", res.data);
          this.props.changePolicyNumberHandler(res.data.policyNumber);
        } catch (err) {
          console.log(err);
        }

        break;
      default: {
        break;
      }
    }
  };

  componentDidUpdate() {
    // ? Reset
    const { chat, resetChat } = this.props;
    // console.warn("ApplicationFooter:: componentDidUpdate:: ACTION:: CHAT:: ", chat.nextReset);
    if (chat.nextReset) {
      this.onClickRemoveChats();
      resetChat();
    }
  }

  componentDidMount() {
    // ? Reset
    const { chat, resetChat } = this.props;
    // console.warn("ApplicationFooter:: componentDidUpdate:: ACTION:: CHAT:: ", chat.nextReset);
    if (chat.nextReset) {
      this.onClickRemoveChats();
      resetChat();
    }
    // ? socket.io
    const data = {
      roomId: this.props.roomId,
      username: "test user",
    };
    this.webSocketInstance.emit("joinChatRoomEvent", data);
    this.webSocketInstance.on("newClientJoinChatRoomEvent", data => {
      addResponseMessage("New client join chat room");
    });
    this.webSocketInstance.on("receiveMessageEvent", data => {
      addResponseMessage(data.message);
      // this.handleNewUserMessage(data.message);
    });
    // addResponseMessage(
    //   "Hi, what is your name? Please use 'My name:' to state it",
    // );
  }

  componentWillUnmount() {
    this.webSocketInstance.close();
    this.webSocketInstance = null;
  }

  handleNewUserMessage = newMessage => {
    const data = {
      roomId: this.props.roomId,
      message: newMessage,
    };
    this.webSocketInstance.emit("sendMessageEvent", data);
    console.log(`CHAT:: New message incoming! ${newMessage}`);
    // if (newMessage.search("My name: ") > -1) {
    //   addResponseMessage(
    //     `Hi ${newMessage.slice(newMessage.search("My name:") + 8)}!`,
    //   );
    // } else if (newMessage.search("fuck") > -1) {
    //   addResponseMessage("Language!!!");
    // } else {
    //   // addResponseMessage("How can I help you?");
    //   addResponseMessage(newMessage);
    // }
  };

  isEmailPassed = emailStr => {
    const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    if (typeof emailStr === "string" && emailRegex.test(emailStr)) {
      return true;
    }
    return false;
  };

  isPasswordPassed = passwordStr => {
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/;
    if (typeof passwordStr === "string" && passwordRegex.test(passwordStr)) {
      return true;
    }
    return false;
  };

  isPasswordLengthPassed = passwordStr => {
    if (typeof passwordStr === "string" && passwordStr.length > 5) {
      return true;
    }
    return false;
  };

  render() {
    const {
      isShareDialogOpen,
      password,
      email,
      emailList,
      anchorEl,
      isLinkCopied,
      passwordLengthError,
      passwordStrError,
      emailStrError,
    } = this.state;
    const {
      t,
      location,
      sendInvitationHandler,
      submitApplication,
    } = this.props;

    const isButtonEnabled =
      this.isPasswordPassed(password) && emailList.length > 0;

    const responsiveTheme = responsiveFontSizes(theme);

    console.warn(
      "isButtonEnabled:: ",
      isButtonEnabled,
      this.isPasswordPassed(password),
    );
    const shareDialog = (
      <Dialog
        onClose={this.onClickShareDialog}
        aria-labelledby="shareDialog-title"
        open={isShareDialogOpen}
        classes={{ root: styles.shareDialog }}
        maxWidth={"md"}
      >
        <CloseIcon
          className={styles.shareDialogClose}
          onClick={this.onClickShareDialog}
        />
        <DialogTitle className={styles.shareDialogTitle}>
          <Typography
            variant="h1"
            className={styles.shareDialogTitleBold}
            display="inline"
          >
            {t(`${INVITE_DIALOG}.TITLE_BOLD`)}
            <Typography variant="h1" display="inline">
              {t(`${INVITE_DIALOG}.TITLE_NORMAL`)}
            </Typography>
          </Typography>
        </DialogTitle>
        <DialogContent className={styles.shareDialogContent}>
          <div className={styles.shareDialogContentRow}>
            <div>
              <Typography
                className={styles.shareDialogTitleSemiBold}
                variant="body1"
              >
                {t(`${INVITE_DIALOG}.PASSWORD`)}
              </Typography>
            </div>
            <div className={styles.shareDialogContentColumnPadRight}>
              <TextField
                fullWidth
                variant="outlined"
                color="secondary"
                value={password}
                onChange={e =>
                  this.setState({
                    ...this.state,
                    password: e.target.value,
                    passwordLengthError: !this.isPasswordLengthPassed(
                      e.target.value,
                    ),
                    passwordStrError: !this.isPasswordPassed(e.target.value),
                  })
                }
              />
            </div>
          </div>
          <div className={styles.shareDialogContentRow}>
            <div></div>
            <ul className={styles.shareDialogContentColumnPadLeft}>
              <li>
                <Typography
                  color={passwordLengthError ? "error" : "secondary"}
                  variant="body1"
                >
                  {t(`${INVITE_DIALOG}.PASSWORD1`)}
                </Typography>
              </li>
              <li>
                <Typography
                  color={passwordStrError ? "error" : "secondary"}
                  variant="body1"
                >
                  {t(`${INVITE_DIALOG}.PASSWORD2`)}
                </Typography>
              </li>
            </ul>
          </div>
          <div className={styles.shareDialogContentRow}>
            <div>
              <Typography
                className={styles.shareDialogTitleSemiBold}
                variant="body1"
              >
                {t(`${INVITE_DIALOG}.COPY`)}
              </Typography>
            </div>
            <div className={styles.shareDialogContentColumnPadRight}>
              <Popper
                id={"linkButton"}
                open={Boolean(isLinkCopied)}
                anchorEl={anchorEl}
              >
                <Typography>{t(`${INVITE_DIALOG}.COPIED`)}</Typography>
              </Popper>
              <TextField
                inputRef={ref => {
                  this.linkRef = ref;
                }}
                fullWidth
                variant="outlined"
                color="secondary"
                value={window.location.href}
                disabled
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <CopyToClipboard
                        text={window.location.href}
                        onCopy={() =>
                          this.setState({ ...this.state, isLinkCopied: true })
                        }
                      >
                        <IconButton>
                          <LinkIcon />
                        </IconButton>
                      </CopyToClipboard>
                    </InputAdornment>
                  ),
                }}
              />
            </div>
          </div>
          <div
            className={clsx(
              styles.shareDialogContentRow,
              styles.shareDialogEmailRegion,
            )}
          >
            <div>
              <Typography
                className={styles.shareDialogTitleSemiBold}
                variant="body1"
              >
                {t(`${INVITE_DIALOG}.EMAIL`)}
              </Typography>
            </div>
            <div className={styles.shareDialogContentColumnPadRight}>
              <TextField
                type="email"
                fullWidth
                variant="outlined"
                color="secondary"
                value={email}
                onChange={e =>
                  this.setState({
                    ...this.state,
                    email: e.target.value,
                    emailStrError: !this.isEmailPassed(email),
                  })
                }
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-describedby={"linkButton"}
                        onClick={() => {
                          if (this.isEmailPassed(email)) {
                            this.setState({
                              ...this.state,
                              email: "",
                              emailList: [...this.state.emailList, email],
                            });
                          }
                        }}
                      >
                        <PlusIcon />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </div>
          </div>
          <div className={styles.shareDialogContentRow}>
            <div />
            <div>
              <ul>
                <li>
                  <Typography
                    color={emailStrError ? "error" : "secondary"}
                    variant="body1"
                  >
                    {t(`${INVITE_DIALOG}.EMAIL1`)}
                  </Typography>
                </li>
              </ul>
            </div>
          </div>
          <div className={styles.shareDialogContentRow}>
            <div></div>
            <div className={styles.shareDialogContentColumnPadLeft}>
              {(emailList || []).map((str, index) => (
                <div key={str} className={styles.emailRow}>
                  <div className={styles.emailStr}>
                    <Typography>{str}</Typography>
                  </div>
                  <div
                    className={styles.closeIcon}
                    onClick={this.onClickRemoveEmail(index)}
                  >
                    <CloseIcon className={styles.closeIcon} />
                  </div>
                </div>
              ))}
            </div>
          </div>
        </DialogContent>
        <DialogActions className={styles.shareDialogFooter}>
          <Button
            color="primary"
            variant="contained"
            disabled={!isButtonEnabled}
            onClick={() => {
              sendInvitationHandler({ password, emails: emailList });
              this.props.startShareModeHandler();
              this.setState({
                ...this.state,
                isShareDialogOpen: false,
              });
            }}
          >
            {t(`${INVITE_DIALOG}.SHARE`)}
          </Button>
        </DialogActions>
      </Dialog>
    );

    let isDisableBtn = true;
    const path = this.props.location.pathname;
    let goToURL = this.props.location.pathname;
    if (path.includes("/eapp/upload/")) {
      isDisableBtn = this.props.isEnableNextUploadPropsoal;
      goToURL = `/eapp/application/${this.props.roomId}`;
    } else if (path.includes("/eapp/application/")) {
      isDisableBtn = this.props.isEnableNextApplication;
      goToURL = `/eapp/sign/${this.props.roomId}`;
    } else if (path.includes("/eapp/sign/")) {
      isDisableBtn = this.props.isEnableNextSignature;
      goToURL = `/eapp/supportdocs/${this.props.roomId}`;
    } else if (path.includes("/eapp/supportdocs/")) {
      isDisableBtn = this.props.isEnableNextSupportDoc;
      goToURL = `/eapp/submit/${this.props.roomId}`;
    } else if (path.includes("/eapp/submit/")) {
      isDisableBtn = this.props.isEnableNextSubmit;
      goToURL = `/client/login/${this.props.roomId}`;
    }
    console.log("ApplicationFooter:: ", isDisableBtn);

    return (
      <ThemeProvider theme={responsiveTheme}>
        {shareDialog}
        <div className={styles.BottomWrapper}>
          <div className={styles.fabContainer}>
            <ChatWidget
              title={t(`${CHAT}.TITLE`)}
              subtitle={t(`${CHAT}.SUBTITLE`)}
              handleNewUserMessage={this.handleNewUserMessage}
              launcher={handleToggle => (
                <Fab
                  // className={styles.}
                  styleType={FAB_ICON}
                  icon={<ChatIcon />}
                  onClick={handleToggle}
                  role="button"
                  tabIndex={0}
                  onKeyPress={handleToggle}
                >
                  {t(`BUTTON.CHAT`)}
                </Fab>
              )}
            />
            <br />
            {this.props.identity === "agent" && (
              <Fab
                styleType={FAB_ICON}
                icon={<ShareIcon />}
                onClick={this.onClickShareDialog}
              >
                {t(`BUTTON.SHARE`)}
              </Fab>
            )}
          </div>
          <div className={styles.ButtonContainer}>
            {this.props.identity === "agent" && (
              <>
                <div className={styles.ButtonBox}>
                  <Button
                    onClick={async () => {
                      if (this.props.identity === "agent") {
                        await this.clickSaveHandler();
                      }
                    }}
                    color="primary"
                    variant="outlined"
                    className={styles.Button}
                  >
                    {t("BUTTON.SAVE")}
                  </Button>
                </div>
                <div className={styles.ButtonBox}>
                  <Button
                    disabled={!isDisableBtn}
                    color="primary"
                    variant="contained"
                    className={styles.Button}
                    onClick={async () => {
                      if (this.props.identity === "agent") {
                        await this.clickSaveHandler();
                        this.props.history.push(goToURL);
                        this.props.changeStepNumberHandler(goToURL);
                      }
                    }}
                  >
                    {t("BUTTON.NEXT")}
                  </Button>
                </div>
              </>
            )}
          </div>
        </div>
      </ThemeProvider>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withTranslation()(ApplicationFooter)),
);
