export const TYPE_CHANGE_WS_BROADTOPBAR = "TYPE_CHANGE_WS_BROADTOPBAR";

export const changeWsBroadTopBar = wsBroadTopBar => ({
  type: TYPE_CHANGE_WS_BROADTOPBAR,
  payload: {
    wsBroadTopBar,
  },
});
