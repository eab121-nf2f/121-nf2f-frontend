export const TYPE_CHANGE_ROOM_ID = "TYPE_CHANGE_ROOM_ID";
export const TYPE_ADD_CLIENT = "TYPE_ADD_CLIENT";
export const TYPE_DELETE_CLIENT = "TYPE_DELETE_CLIENT";
export const TYPE_CHANGE_CLIENT_LOGIN_SUCCESS_REDIRECT = "TYPE_CHANGE_CLIENT_LOGIN_SUCCESS_REDIRECT";
export const TYPE_START_SHARE_MODE = "TYPE_START_SHARE_MODE";
export const TYPE_END_SHARE_MODE = "TYPE_END_SHARE_MODE";
export const TYPE_START_CONTROL_MODE = "TYPE_START_CONTROL_MODE";
export const TYPE_END_CONTROL_MODE = "TYPE_END_CONTROL_MODE";

export const changeRoomIdAction = roomId => ({
  type: TYPE_CHANGE_ROOM_ID,
  payload: {
    roomId,
  },
});

export const addClientAction = () => ({
  type: TYPE_CHANGE_ROOM_ID,
});

export const deleteClientAction = () => ({
  type: TYPE_CHANGE_ROOM_ID,
});

export const chnageClientLoginSuccessRedirectToAction = redirectToURL => ({
  type: TYPE_CHANGE_CLIENT_LOGIN_SUCCESS_REDIRECT,
  payload: {
    redirectToURL,
  },
});

export const startShareModeAction = () => ({
  type: TYPE_START_SHARE_MODE,
});

export const endShareModeAction = () => ({
  type: TYPE_END_SHARE_MODE,
});

export const startControlModeAction = () => ({
  type: TYPE_START_CONTROL_MODE,
});

export const endControlModeAction = () => ({
  type: TYPE_END_CONTROL_MODE,
});
