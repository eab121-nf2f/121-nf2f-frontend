// for stepper
export const TYPE_ENABLE_STEP_UPLOAD_PROPOSAL =
  "TYPE_ENABLE_STEP_UPLOAD_PROPOSAL";
export const TYPE_DISABLE_STEP_UPLOAD_PROPOSAL =
  "TYPE_DISABLE_STEP_UPLOAD_PROPOSAL";

export const TYPE_ENABLE_STEP_APPLICATION = "TYPE_ENABLE_STEP_APPLICATION";
export const TYPE_DISABLE_STEP_APPLICATION = "TYPE_DISABLE_STEP_APPLICATION";

export const TYPE_ENABLE_STEP_SIGNATURE = "TYPE_ENABLE_STEP_SIGNATURE";
export const TYPE_DISABLE_STEP_SIGNATURE = "TYPE_DISABLE_STEP_SIGNATURE";

export const TYPE_ENABLE_STEP_SUPPORTING_DOC =
  "TYPE_ENABLE_STEP_SUPPORTING_DOC";
export const TYPE_DISABLE_STEP_SUPPORTING_DOC =
  "TYPE_DISABLE_STEP_SUPPORTING_DOC";

export const TYPE_ENABLE_STEP_SUBMIT = "TYPE_ENABLE_STEP_SUBMIT";
export const TYPE_DISABLE_STEP_SUBMIT = "TYPE_DISABLE_STEP_SUBMIT";

export const TYPE_CHANGE_STEP_NUMBER = "TYPE_CHANGE_STEP_NUMBER";

// for stepper
export const enableStepUploadProposalAction = () => ({
  type: TYPE_ENABLE_STEP_UPLOAD_PROPOSAL,
});

export const disabletepUploadProposalAction = () => ({
  type: TYPE_DISABLE_STEP_UPLOAD_PROPOSAL,
});

export const enableStepApplicationAction = () => ({
  type: TYPE_ENABLE_STEP_APPLICATION,
});

export const disableStepApplicationAction = () => ({
  type: TYPE_DISABLE_STEP_APPLICATION,
});

export const enableStepSignatureAction = () => ({
  type: TYPE_ENABLE_STEP_SIGNATURE,
});

export const disableStepSignatureAction = () => ({
  type: TYPE_DISABLE_STEP_SIGNATURE,
});

export const enableStepSupportingDocAction = () => ({
  type: TYPE_ENABLE_STEP_SUPPORTING_DOC,
});

export const disableStepSupportingDocAction = () => ({
  type: TYPE_DISABLE_STEP_SUPPORTING_DOC,
});

export const enableStepSubmitAction = () => ({
  type: TYPE_ENABLE_STEP_SUBMIT,
});

export const disableStepSubmitAction = () => ({
  type: TYPE_DISABLE_STEP_SUBMIT,
});

export const chnageStepNumberAction = stepNumber => ({
  type: TYPE_CHANGE_STEP_NUMBER,
  payload: {
    stepNumber,
  },
});
