import * as auth from "./authAction";
import * as application from "./applicationAction";

export default {
    auth,
    application
};
