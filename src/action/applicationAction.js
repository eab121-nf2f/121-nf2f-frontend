import {
  getCafeResponse,
  getSignedPdf,
  invite,
  submitApplication,
  UploadApplicationProposal,
} from "../api";

// ? Reconstruct State form DB
export const EAPP_ACTION_RECONSTRUCT = "EAPP_ACTION_RECONSTRUCT";

// ? General Application Actions
export const EAPP_ACTION_SUBMIT_SUCCESS = "EAPP_ACTION_SUBMIT_SUCCESS";
export const EAPP_ACTION_SUBMIT_FAIL = "EAPP_ACTION_SUBMIT_FAIL";
export const EAPP_ACTION_INVITE_SUCCESS = "EAPP_ACTION_INVITE_SUCCESS";
export const EAPP_ACTION_INVITE_FAIL = "EAPP_ACTION_INVITE_FAIL";

// ? eSign Services Actions
export const EAPP_ACTION_SIGN_SUCCESS = "EAPP_ACTION_SIGN_SUCCESS";
export const EAPP_ACTION_SIGN_FAIL = "EAPP_ACTION_SIGN_FAIL";

// ? Cafe Services Actions
export const EAPP_ACTION_CAFE_SAVE_SUCCESS = "EAPP_ACTION_CAFE_SAVE_SUCCESS";
export const EAPP_ACTION_CAFE_SAVE_FAIL = "EAPP_ACTION_CAFE_SAVE_FAIL";

// ? Application Proposal Upload
export const TYPE_APPLICATION_PROPOSAL_UPLOAD_SUCCESS =
  "TYPE_APPLICATION_PROPOSAL_UPLOAD_SUCCESS";
export const TYPE_APPLICATION_PROPOSAL_UPLOAD_FAILURE =
  "TYPE_APPLICATION_PROPOSAL_UPLOAD_FAILURE";

// ? Application Reset
export const EAPP_ACTION_RESET_APPLICATION = "EAPP_ACTION_RESET_APPLICATION";

// chnage state
export const TYPE_CHANGE_PRODUCT_ID = "TYPE_CHANGE_PRODUCT_ID";
export const TYPE_CHNAGE_PROPOSAL_BASE64 = "TYPE_CHNAGE_PROPOSAL_BASE64";
export const TYPE_CHANGE_ID_CARD_BASE64 = "TYPE_CHANGE_ID_CARD_BASE64";
export const TYPE_CHANGE_ADDRESS_BASE64 = "TYPE_CHANGE_ADDRESS_BASE64";
export const TYPE_CHANGE_PROPOSAL_NAME = "TYPE_CHANGE_PROPOSAL_NAME";
export const TYPE_CHANGE_SELECTED_PRODUCT = "TYPE_CHANGE_SELECTED_PRODUCT";
export const TYPE_CHANGE_ID_CARD_NAME = "TYPE_CHANGE_ID_CARD_NAME";
export const TYPE_CHANGE_ADDRESS_NAME = "TYPE_CHANGE_ADDRESS_NAME";
export const TYPE_CHANGE_POLICY_NUMBER = "TYPE_CHANGE_POLICY_NUMBER";

export const onApplicationSubmitIntercept = () => async (
  dispatch,
  getState,
) => {
  try {
    const { application } = getState();
    const { applicationId } = application;
    const submitResponse = await submitApplication({ applicationId });
    dispatch({
      type: EAPP_ACTION_SUBMIT_SUCCESS,
    });
  } catch (error) {
    dispatch({
      type: EAPP_ACTION_SUBMIT_FAIL,
      error,
    });
  }
};

export const onApplicationSignIntercept = (docId, sessionId) => async (
  dispatch,
  getState,
) => {
  console.warn("Action:: application:: onApplicationSignIntercept:: ", docId);
  try {
    const signedPdfResponse = await getSignedPdf({
      sessionId,
      docId,
    });
    const base64 = new Buffer(signedPdfResponse.data, "binary").toString(
      "base64",
    );
    console.warn(
      "Action:: application:: onApplicationSignIntercept:: EAPP_ACTION_SIGN_SUCCESS",
      base64,
    );
    dispatch({
      type: EAPP_ACTION_SIGN_SUCCESS,
      key: docId,
      data: base64,
    });
  } catch (error) {
    console.warn(
      "Action:: application:: onApplicationSignIntercept:: EAPP_ACTION_SIGN_FAIL",
      error,
    );
    dispatch({
      type: EAPP_ACTION_SIGN_FAIL,
      error,
    });
  }
};

export const onCafeSaveIntercept = (resID, fID) => async (
  dispatch,
  getState,
) => {
  try {
    const cafeExportResponse = await getCafeResponse({
      responseId: resID,
      formId: fID,
    });
    const {
      data: { result: cafeExportResult },
    } = cafeExportResponse;
    const { responseID, formID, ...otherProps } = cafeExportResult;
    dispatch({
      type: EAPP_ACTION_CAFE_SAVE_SUCCESS,
      responseId: responseID,
      formId: formID,
      ...otherProps,
    });
  } catch (error) {
    dispatch({
      type: EAPP_ACTION_CAFE_SAVE_FAIL,
      error,
    });
  }
};

export const onSendInvitationIntercept = ({ password, emails }) => async (
  dispatch,
  getState,
) => {
  const { application, socket } = getState();
  try {
    const inviteResponse = await invite({
      applicationId: application.applicationId,
      password,
      emails,
      session: socket.roomId,
    });
    console.warn(
      "applicationAction:: onSendInvitationIntercept::",
      inviteResponse,
    );
    dispatch({
      type: EAPP_ACTION_INVITE_SUCCESS,
    });
  } catch (error) {
    dispatch({
      type: EAPP_ACTION_INVITE_FAIL,
      error,
    });
  }
};

export const uploadProposalActionThunk = (
  productId,
  propsoalBase64,
) => async dispatch => {
  try {
    console.log("productId", productId);
    console.log("propsoalBase64", propsoalBase64);
    const response = await UploadApplicationProposal(productId, propsoalBase64);
    console.log(response.data);
    dispatch(uploadProposalSuccessAction(response.data));
  } catch (err) {
    console.log(err);
    dispatch(uploadProposalFailureAction(err));
  }
};

const uploadProposalSuccessAction = data => ({
  type: TYPE_APPLICATION_PROPOSAL_UPLOAD_SUCCESS,
  payload: {
    applicationId: data,
  },
});

const uploadProposalFailureAction = error => ({
  type: TYPE_APPLICATION_PROPOSAL_UPLOAD_FAILURE,
  payload: {
    error,
  },
});

export const changeProductIdAction = productId => ({
  type: TYPE_CHANGE_PRODUCT_ID,
  payload: {
    productId,
  },
});

export const changeProposalBase64Action = proposalPDFBase64 => ({
  type: TYPE_CHNAGE_PROPOSAL_BASE64,
  payload: {
    proposalPDFBase64,
  },
});

export const changeIdCardBase64Action = idCardFIleBase64 => ({
  type: TYPE_CHANGE_ID_CARD_BASE64,
  payload: {
    idCardFIleBase64,
  },
});

export const changeAddressBase64Action = addressFileBase64 => ({
  type: TYPE_CHANGE_ADDRESS_BASE64,
  payload: {
    addressFileBase64,
  },
});

export const changeProposalNameAction = proposalName => ({
  type: TYPE_CHANGE_PROPOSAL_NAME,
  payload: {
    proposalName,
  },
});

export const changeSelectedProductAction = selectedProduct => ({
  type: TYPE_CHANGE_SELECTED_PRODUCT,
  payload: {
    selectedProduct,
  },
});

export const changeIdCardFileNameAction = idCardFileName => ({
  type: TYPE_CHANGE_ID_CARD_NAME,
  payload: {
    idCardFileName,
  },
});

export const changeAddressFileNameAction = addressFileName => ({
  type: TYPE_CHANGE_ADDRESS_NAME,
  payload: {
    addressFileName,
  },
});

export const changePolicyNumberAction = policyNumber => ({
  type: TYPE_CHANGE_POLICY_NUMBER,
  payload: {
    policyNumber,
  },
});
