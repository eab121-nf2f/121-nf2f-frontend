export const TYPE_CHANGE_MEETING_NAME = "TYPE_CHANGE_MEETING_NAME";
export const TYPE_CHANGE_PARTICIPANT_NAME = "TYPE_CHANGE_PARTICIPANT_NAME";

export const changeMeetingNameAction = meetingName => ({
  type: TYPE_CHANGE_MEETING_NAME,
  payload: {
    meetingName,
  },
});

export const changeParticipantNameAction = clientName => ({
  type: TYPE_CHANGE_PARTICIPANT_NAME,
  payload: {
    clientName,
  },
});
