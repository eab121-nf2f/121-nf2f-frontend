import axios from "axios";

import { applicationJoin, Login, logout } from "../api";

export const TYPE_LOGIN_START = "TYPE_LOGIN_START";
export const TYPE_LOGIN_SUCCESS = "TYPE_LOGIN_SUCCESS";
export const TYPE_LOGIN_FAILURE = "TYPE_LOGIN_FAILURE";
export const TYPE_LOGOUT = "TYPE_LOGOUT";
export const TYPE_TEMP_CLIENT_IS_AUTHENTICATED =
  "TYPE_TEMP_CLIENT_IS_AUTHENTICATED";
export const TYPE_CLIENT_LOGIN_SUCCESS = "TYPE_CLIENT_LOGIN_SUCCESS";
export const TYPE_CLIENT_LOGIN_FAILURE = "TYPE_CLIENT_LOGIN_FAILURE";

export const createLoginError = ({ id, message, status }) => ({
  message,
  id,
  status,
});

export const loginThunkAction = (
  username,
  password,
  callback = () => {},
) => async dispatch => {
  dispatch(loginStartAction());
  try {
    const data = {
      userName: username,
      password,
    };
    console.warn("LoginThunk:: Login", data);
    const response = await Login(data);
    if (response.error === "error" || !response.data) {
      console.warn("LoginThunk:: ERROR", response);
      dispatch(
        loginFailureAction({
          error: {
            message: response.error.message,
            status: response.error.status,
          },
        }),
      );
      return;
    }
    console.warn("CHECK");
    const jwtToken = response.data.t;
    console.warn("CHECK:: ", response.data);
    localStorage.setItem("token", jwtToken);
    axios.defaults.headers.Authorization = `Bearer ${localStorage.getItem(
      "token",
    )}`;
    console.warn("CHECK2");
    dispatch(loginSuccessAction(response.data));
    callback();
  } catch (error) {
    console.warn("LoginThunk:: CATCH", error);
    dispatch(loginFailureAction(error));
  }
};

const loginStartAction = () => ({
  type: TYPE_LOGIN_START,
});

const loginSuccessAction = userInfo => ({
  type: TYPE_LOGIN_SUCCESS,
  payload: {
    ...userInfo,
  },
});

const loginFailureAction = error => ({
  type: TYPE_LOGIN_FAILURE,
  payload: {
    ...error,
  },
});

export const logoutThunkAction = () => async dispatch => {
  try {
    const response = await logout();
    localStorage.removeItem("token");
    console.log(response.data);
    dispatch(logoutAction(response.data));
  } catch (err) {
    console.log(err);
  }
};

export const logoutAction = () => ({
  type: TYPE_LOGOUT,
});

export const tempClientAuthAction = (isAuthenticated, identity) => ({
  type: TYPE_TEMP_CLIENT_IS_AUTHENTICATED,
  payload: {
    isAuthenticated,
    identity,
  },
});

export const clientLoginThunkAction = (
  roomId,
  password,
  callback = () => {},
) => async dispatch => {
  const response = await applicationJoin(roomId, password);
  console.warn("response:: ", response);
  if (response.status === 400 || response.data.status === "error") {
    console.warn("applicationJoin:: ERROR", response);
    dispatch(
      clientLoginFailureAction({
        error: {
          message: response.data.message.error,
          status: response.data.status,
        },
      }),
    );
    return;
  }
  const jwtToken = response.data.t;
  localStorage.setItem("token", jwtToken);
  axios.defaults.headers.Authorization = `Bearer ${localStorage.getItem(
    "token",
  )}`;
  dispatch(clientLoginSuccessAction(response.data));
  callback();
};

const clientLoginSuccessAction = userInfo => ({
  type: TYPE_CLIENT_LOGIN_SUCCESS,
  payload: {
    userInfo,
  },
});

const clientLoginFailureAction = error => ({
  type: TYPE_CLIENT_LOGIN_FAILURE,
  payload: {
    ...error,
  },
});
