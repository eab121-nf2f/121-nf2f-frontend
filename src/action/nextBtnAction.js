// for next button
export const TYPE_ENABLE_NEXT_BTN_UPLOAD_PROPOSAL = "TYPE_ENABLE_NEXT_BTN_UPLOAD_PROPOSAL";
export const TYPE_DISABLE_NEXT_BTN_UPLOAD_PROPOSAL = "TYPE_DISABLE_NEXT_BTN_UPLOAD_PROPOSAL";

export const TYPE_ENABLE_NEXT_BTN_APPLICATION = "TYPE_ENABLE_NEXT_BTN_APPLICATION";
export const TYPE_DISABLE_NEXT_BTN_APPLICATION = "TYPE_DISABLE_NEXT_BTN_APPLICATION";

export const TYPE_ENABLE_NEXT_BTN_SIGNATURE = "TYPE_ENABLE_NEXT_BTN_SIGNATURE";
export const TYPE_DISABLE_NEXT_BTN_SIGNATURE = "TYPE_DISABLE_NEXT_BTN_SIGNATURE";

export const TYPE_ENABLE_NEXT_BTN_SUPPORTING_DOC = "TYPE_ENABLE_NEXT_BTN_SUPPORTING_DOC";
export const TYPE_DISABLE_NEXT_BTN_SUPPORTING_DOC = "TYPE_DISABLE_NEXT_BTN_SUPPORTING_DOC";

export const TYPE_ENABLE_NEXT_BTN_SUBMIT = "TYPE_ENABLE_NEXT_BTN_SUBMIT";
export const TYPE_DISABLE_NEXT_BTN_SUBMIT = "TYPE_DISABLE_NEXT_BTN_SUBMIT";

// for next button
export const enableNextBtnUploadProposalAction = () => ({
  type: TYPE_ENABLE_NEXT_BTN_UPLOAD_PROPOSAL,
});

export const disableNextBtnUploadProposalAction = () => ({
  type: TYPE_DISABLE_NEXT_BTN_UPLOAD_PROPOSAL,
});

export const enableNextBtnApplicationAction = () => ({
  type: TYPE_ENABLE_NEXT_BTN_APPLICATION,
});

export const disableNextBtnApplicationAction = () => ({
  type: TYPE_DISABLE_NEXT_BTN_APPLICATION,
});

export const enableNextBtnSignatureAction = () => ({
  type: TYPE_ENABLE_NEXT_BTN_SIGNATURE,
});

export const disableNextBtnSignatureAction = () => ({
  type: TYPE_DISABLE_NEXT_BTN_SIGNATURE,
});

export const enableNextBtnSupportingDocAction = () => ({
  type: TYPE_ENABLE_NEXT_BTN_SUPPORTING_DOC,
});

export const disableNextBtnSupportingDocAction = () => ({
  type: TYPE_DISABLE_NEXT_BTN_SUPPORTING_DOC,
});

export const enableNextBtnSubmitAction = () => ({
  type: TYPE_ENABLE_NEXT_BTN_SUBMIT,
});

export const disableNextBtnSubmitAction = () => ({
  type: TYPE_DISABLE_NEXT_BTN_SUBMIT,
});
