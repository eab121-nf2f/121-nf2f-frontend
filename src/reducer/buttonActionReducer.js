import { EAPP_ACTION_RESET_APPLICATION } from "../action/applicationAction";
import { BUTTON_ACTION_SETONCLICK } from "../action/buttonAction";

const initialState = {
  onClick: () => {},
};

const buttonAction = (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case BUTTON_ACTION_SETONCLICK:
      return {
        ...state,
        onClick: action.onClick,
      };
    case EAPP_ACTION_RESET_APPLICATION: {
      return {
        ...initialState,
      };
    }
    default:
      return state;
  }
};

export default buttonAction;
