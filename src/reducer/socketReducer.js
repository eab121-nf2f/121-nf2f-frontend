import {
  TYPE_ADD_CLIENT,
  TYPE_CHANGE_CLIENT_LOGIN_SUCCESS_REDIRECT,
  TYPE_CHANGE_ROOM_ID,
  TYPE_DELETE_CLIENT,
  TYPE_START_SHARE_MODE,
  TYPE_END_SHARE_MODE,
  TYPE_START_CONTROL_MODE,
  TYPE_END_CONTROL_MODE,
} from "../action/socketAction";

const initialState = {
  roomId: "",
  clientList: [],
  ownSocketId: "",
  clientLoginSuccessRedirectTo: "",
  shareMode: false,
  controlMode: false,
};

const socketReducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case TYPE_CHANGE_ROOM_ID: {
      return {
        ...state,
        roomId: action.payload.roomId,
      };
    }
    case TYPE_ADD_CLIENT: {
      return {
        ...state,
        clientList: [...state.clientList, action.payload.clientList],
      };
    }
    case TYPE_DELETE_CLIENT: {
      return {
        ...state,
        roomId: [...state.clientList],
      };
    }
    case TYPE_CHANGE_CLIENT_LOGIN_SUCCESS_REDIRECT: {
      return {
        ...state,
        clientLoginSuccessRedirectTo: action.payload.redirectToURL,
      };
    }
    case TYPE_START_SHARE_MODE: {
      return {
        ...state,
        shareMode: true,
      };
    }
    case TYPE_END_SHARE_MODE: {
      return {
        ...state,
        shareMode: false,
      };
    }
    case TYPE_START_CONTROL_MODE: {
      return {
        ...state,
        controlMode: true,
      };
    }
    case TYPE_END_CONTROL_MODE: {
      return {
        ...state,
        controlMode: false,
      };
    }
    default: {
      return state;
    }
  }
};

export default socketReducer;
