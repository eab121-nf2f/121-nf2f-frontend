import {
  TYPE_CHANGE_MEETING_NAME,
  TYPE_CHANGE_PARTICIPANT_NAME,
} from "../action/meetingAction";

const initialState = {
  meetingName: "",
  clientName: "",
};

const meetingReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPE_CHANGE_MEETING_NAME: {
      return {
        ...state,
        meetingName: action.payload.meetingName,
      };
    }
    case TYPE_CHANGE_PARTICIPANT_NAME: {
      return {
        ...state,
        clientName: action.payload.clientName,
      };
    }
    default: {
      return state;
    }
  }
};

export default meetingReducer;
