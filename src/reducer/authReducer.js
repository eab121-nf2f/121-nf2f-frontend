import {
  TYPE_CLIENT_LOGIN_FAILURE,
  TYPE_CLIENT_LOGIN_SUCCESS,
  TYPE_LOGIN_FAILURE,
  TYPE_LOGIN_START,
  TYPE_LOGIN_SUCCESS,
  TYPE_LOGOUT,
  TYPE_TEMP_CLIENT_IS_AUTHENTICATED,
} from "../action/authAction";

const initialState = {
  isAuthenticated: true,
  loading: false,
  userInfo: null,
  error: null,
  identity: "client", // only "client" or "agent"
};

const authReducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case TYPE_LOGIN_START:
      return {
        ...state,
        isAuthenticated: false,
        loading: true,
        userInfo: null,
        error: null,
      };
    case TYPE_LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        userInfo: action.payload.name,
        error: null,
        identity: "agent",
      };
    case TYPE_LOGIN_FAILURE:
      return {
        ...state,
        isAuthenticated: false,
        loading: false,
        error: action.payload.error,
      };
    case TYPE_LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
        loading: false,
        userInfo: null,
        error: null,
      };
    case TYPE_TEMP_CLIENT_IS_AUTHENTICATED: {
      return {
        ...state,
        isAuthenticated: action.payload.isAuthenticated,
        identity: action.payload.identity,
      };
    }
    case TYPE_CLIENT_LOGIN_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        identity: "client",
      };
    }
    case TYPE_CLIENT_LOGIN_FAILURE: {
      return {
        ...state,
        isAuthenticated: false,
        identity: "client",
        error: action.payload.error,
      };
    }
    default:
      return state;
  }
};

export default authReducer;
