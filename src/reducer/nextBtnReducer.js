import { EAPP_ACTION_RESET_APPLICATION } from "../action/applicationAction";
import {
  TYPE_DISABLE_NEXT_BTN_APPLICATION,
  TYPE_DISABLE_NEXT_BTN_SIGNATURE,
  TYPE_DISABLE_NEXT_BTN_SUBMIT,
  TYPE_DISABLE_NEXT_BTN_SUPPORTING_DOC,
  TYPE_DISABLE_NEXT_BTN_UPLOAD_PROPOSAL,
  TYPE_ENABLE_NEXT_BTN_APPLICATION,
  TYPE_ENABLE_NEXT_BTN_SIGNATURE,
  TYPE_ENABLE_NEXT_BTN_SUBMIT,
  TYPE_ENABLE_NEXT_BTN_SUPPORTING_DOC,
  TYPE_ENABLE_NEXT_BTN_UPLOAD_PROPOSAL,
} from "../action/nextBtnAction";

const initialState = {
  // for enabling next button
  isEnableNextUploadPropsoal: false,
  isEnableNextApplication: false,
  isEnableNextSignature: false,
  isEnableNextSupportDoc: false,
  isEnableNextSubmit: false,
};

const nextBtnReducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case TYPE_DISABLE_NEXT_BTN_APPLICATION: {
      return {
        ...state,
        isEnableNextApplication: false,
      };
    }
    case TYPE_DISABLE_NEXT_BTN_SIGNATURE: {
      return {
        ...state,
        isEnableNextSignature: false,
      };
    }
    case TYPE_DISABLE_NEXT_BTN_SUBMIT: {
      return {
        ...state,
        isEnableNextSubmit: false,
      };
    }
    case TYPE_DISABLE_NEXT_BTN_SUPPORTING_DOC: {
      return {
        ...state,
        isEnableNextSupportDoc: false,
      };
    }
    case TYPE_DISABLE_NEXT_BTN_UPLOAD_PROPOSAL: {
      return {
        ...state,
        isEnableNextUploadPropsoal: false,
      };
    }
    case TYPE_ENABLE_NEXT_BTN_APPLICATION: {
      return {
        ...state,
        isEnableNextApplication: true,
      };
    }
    case TYPE_ENABLE_NEXT_BTN_SIGNATURE: {
      return {
        ...state,
        isEnableNextSignature: true,
      };
    }
    case TYPE_ENABLE_NEXT_BTN_SUBMIT: {
      return {
        ...state,
        isEnableNextSubmit: true,
      };
    }
    case TYPE_ENABLE_NEXT_BTN_SUPPORTING_DOC: {
      return {
        ...state,
        isEnableNextSupportDoc: true,
      };
    }
    case TYPE_ENABLE_NEXT_BTN_UPLOAD_PROPOSAL: {
      return {
        ...state,
        isEnableNextUploadPropsoal: true,
      };
    }
    case EAPP_ACTION_RESET_APPLICATION: {
      return {
        ...initialState,
      };
    }
    default: {
      return state;
    }
  }
};

export default nextBtnReducer;
