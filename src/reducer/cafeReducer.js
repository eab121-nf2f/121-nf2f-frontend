import {
  EAPP_ACTION_CAFE_SAVE_FAIL,
  EAPP_ACTION_CAFE_SAVE_SUCCESS,
  EAPP_ACTION_RESET_APPLICATION,
  EAPP_ACTION_SUBMIT_FAIL,
  EAPP_ACTION_SUBMIT_SUCCESS,
} from "../action/applicationAction";
import config from "../config/config.json";

const { cafe: cafeConfig } = config;

// ? Felix
const initialState = {
  formId: cafeConfig.formID,
  responseId: null,
  error: null,
  isCompleted: false,
};

const cafeFormReducer = (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case EAPP_ACTION_CAFE_SAVE_SUCCESS: {
      const { responseId, formId, ...otherProps } = action;
      return {
        ...state,
        responseId,
        formId,
        ...otherProps,
      };
    }
    case EAPP_ACTION_CAFE_SAVE_FAIL: {
      const { error } = action;
      return {
        ...state,
        error,
      };
    }
    case EAPP_ACTION_RESET_APPLICATION: {
      return {
        ...initialState,
      };
    }
    default:
      return state;
  }
};

export default cafeFormReducer;
