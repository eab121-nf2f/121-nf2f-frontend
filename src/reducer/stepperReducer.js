import { EAPP_ACTION_RESET_APPLICATION } from "../action/applicationAction";
import {
  TYPE_CHANGE_STEP_NUMBER,
  TYPE_DISABLE_STEP_APPLICATION,
  TYPE_DISABLE_STEP_SIGNATURE,
  TYPE_DISABLE_STEP_SUBMIT,
  TYPE_DISABLE_STEP_SUPPORTING_DOC,
  TYPE_DISABLE_STEP_UPLOAD_PROPOSAL,
  TYPE_ENABLE_STEP_APPLICATION,
  TYPE_ENABLE_STEP_SIGNATURE,
  TYPE_ENABLE_STEP_SUBMIT,
  TYPE_ENABLE_STEP_SUPPORTING_DOC,
  TYPE_ENABLE_STEP_UPLOAD_PROPOSAL,
} from "../action/stepperAction";

const initialState = {
  // for enabling stepper
  isEnableStepUploadPropsoal: true,
  isEnableStepApplication: false,
  isEnableStepSignature: false,
  isEnableStepSupportDoc: false,
  isEnableStepSubmit: false,
  stepNumber: 1,
};

const stepperReducer = (state = initialState, action) => {
  console.warn("REDUCER:: STEPPER:: ", action);
  switch (action.type) {
    case TYPE_DISABLE_STEP_APPLICATION: {
      return {
        ...state,
        isEnableStepApplication: false,
      };
    }
    case TYPE_DISABLE_STEP_SIGNATURE: {
      return {
        ...state,
        isEnableStepSignature: false,
      };
    }
    case TYPE_DISABLE_STEP_SUBMIT: {
      return {
        ...state,
        isEnableStepSubmit: false,
      };
    }
    case TYPE_DISABLE_STEP_SUPPORTING_DOC: {
      return {
        ...state,
        isEnableStepSupportDoc: false,
      };
    }
    case TYPE_DISABLE_STEP_UPLOAD_PROPOSAL: {
      return {
        ...state,
        isEnableStepUploadPropsoal: false,
      };
    }
    case TYPE_ENABLE_STEP_APPLICATION: {
      return {
        ...state,
        isEnableStepApplication: true,
      };
    }
    case TYPE_ENABLE_STEP_SIGNATURE: {
      return {
        ...state,
        isEnableStepSignature: true,
      };
    }
    case TYPE_ENABLE_STEP_SUBMIT: {
      return {
        ...state,
        isEnableStepSubmit: true,
      };
    }
    case TYPE_ENABLE_STEP_SUPPORTING_DOC: {
      return {
        ...state,
        isEnableStepSupportDoc: true,
      };
    }
    case TYPE_ENABLE_STEP_UPLOAD_PROPOSAL: {
      return {
        ...state,
        isEnableStepUploadPropsoal: true,
      };
    }
    case TYPE_CHANGE_STEP_NUMBER: {
      return {
        ...state,
        stepNumber: action.payload.stepNumber,
      };
    }
    case EAPP_ACTION_RESET_APPLICATION: {
      return {
        ...initialState,
      };
    }
    default: {
      return state;
    }
  }
};

export default stepperReducer;
