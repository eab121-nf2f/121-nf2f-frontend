import { combineReducers } from "redux";
import { sessionReducer } from "redux-react-session";

import applicationReducer from "./applicationReducer";
import authReducer from "./authReducer";
import buttonActionReducer from "./buttonActionReducer";
import cafeReducer from "./cafeReducer";
import chatReducer from "./chatReducer";
import meetingReducer from "./meetingReducer";
import nextBtnReducer from "./nextBtnReducer";
import socketReducer from "./socketReducer";
import stepperReducer from "./stepperReducer";
import wsReducer from "./wsReducer";

// import i18n from "../plugin/i18n";

export default combineReducers({
  session: sessionReducer,
  auth: authReducer,
  application: applicationReducer,
  cafe: cafeReducer,
  socket: socketReducer,
  nextBtn: nextBtnReducer,
  stepper: stepperReducer,
  chat: chatReducer,
  buttonAction: buttonActionReducer,
  ws: wsReducer,
  meeting: meetingReducer,
  // i18n,
});
