import {
  EAPP_ACTION_RESET_APPLICATION,
  EAPP_ACTION_SIGN_FAIL,
  EAPP_ACTION_SIGN_SUCCESS,
  EAPP_ACTION_SUBMIT_FAIL,
  EAPP_ACTION_SUBMIT_SUCCESS,
  TYPE_APPLICATION_PROPOSAL_UPLOAD_SUCCESS,
  TYPE_CHANGE_ADDRESS_BASE64,
  TYPE_CHANGE_ADDRESS_NAME,
  TYPE_CHANGE_ID_CARD_BASE64,
  TYPE_CHANGE_ID_CARD_NAME,
  TYPE_CHANGE_POLICY_NUMBER,
  TYPE_CHANGE_PRODUCT_ID,
  TYPE_CHANGE_PROPOSAL_NAME,
  TYPE_CHANGE_SELECTED_PRODUCT,
  TYPE_CHNAGE_PROPOSAL_BASE64,
} from "../action/applicationAction";
import disclosureJson from "../assets/pdfs/disclosure.json";
import proposalJson from "../assets/pdfs/proposal.json";
import { PDF_DISCLOSURE, PDF_PROPOSAL } from "../constant/attachment";

const initialState = {
  id: null,
  clientName: null,
  status: null,
  proposalTemplates: [
    // ? Felix
    {
      key: PDF_PROPOSAL,
      data: proposalJson.data,
      rule: proposalJson.rule,
    },
    {
      key: PDF_DISCLOSURE,
      data: disclosureJson.data,
      rule: disclosureJson.rule,
    },
  ],
  applicationPdfs: [], // ? Felix
  supportingDocuments: [],
  applicationId: "",
  productId: "",
  proposalPDFBase64: "",
  proposal: null,
  idCardFileBase64: "",
  addressFileBase64: "",
  selectedProduct: null,
  proposalName: "",
  idCardFileName: "",
  addressFileName: "",
  policyNumber: "",
};

const updateDocument = (docArray, docItem) => {
  const targetIndex = (docArray || []).findIndex(
    item => item.key === docItem.key,
  );
  if (targetIndex > -1) {
    docArray[targetIndex] = docItem;
  } else {
    docArray = [...docArray, docItem];
  }
  console.warn("applicationReducer:: updateDocument:: ", docArray);
  return docArray;
};

const applicationReducer = (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case EAPP_ACTION_SIGN_SUCCESS: {
      const { applicationPdfs } = state;
      const { key, data } = action;
      return {
        ...state,
        applicationPdfs: updateDocument(applicationPdfs, { key, data }),
      };
    }
    case EAPP_ACTION_SUBMIT_SUCCESS:
    case EAPP_ACTION_SUBMIT_FAIL:
      return state;
    case TYPE_CHANGE_PRODUCT_ID: {
      console.log("applicationReducer:: ", `${TYPE_CHANGE_PRODUCT_ID} hitted`);
      return {
        ...state,
        productId: action.payload.productId,
      };
    }
    case TYPE_CHNAGE_PROPOSAL_BASE64: {
      console.log(
        "applicationReducer:: ",
        `${TYPE_CHNAGE_PROPOSAL_BASE64} hitted`,
      );
      return {
        ...state,
        proposalPDFBase64: action.payload.proposalPDFBase64,
      };
    }
    case TYPE_APPLICATION_PROPOSAL_UPLOAD_SUCCESS: {
      console.log(
        "applicationReducer:: ",
        `${TYPE_APPLICATION_PROPOSAL_UPLOAD_SUCCESS} hitted`,
      );
      return {
        ...state,
        applicationId: action.payload.applicationId,
      };
    }
    case TYPE_CHANGE_ID_CARD_BASE64: {
      console.log(
        "applicationReducer:: ",
        `${TYPE_CHANGE_ID_CARD_BASE64} hitted`,
      );
      return {
        ...state,
        idCardFileBase64: action.payload.idCardFIleBase64,
      };
    }
    case TYPE_CHANGE_ADDRESS_BASE64: {
      console.log(
        "applicationReducer:: ",
        `${TYPE_CHANGE_ADDRESS_BASE64} hitted`,
      );
      return {
        ...state,
        addressFileBase64: action.payload.addressFileBase64,
      };
    }
    case TYPE_CHANGE_PROPOSAL_NAME: {
      return {
        ...state,
        proposalName: action.payload.proposalName,
      };
    }
    case TYPE_CHANGE_SELECTED_PRODUCT: {
      return {
        ...state,
        selectedProduct: action.payload.selectedProduct,
      };
    }
    case TYPE_CHANGE_ID_CARD_NAME: {
      return {
        ...state,
        idCardFileName: action.payload.idCardFileName,
      };
    }
    case TYPE_CHANGE_ADDRESS_NAME: {
      return {
        ...state,
        addressFileName: action.payload.addressFileName,
      };
    }
    case TYPE_CHANGE_POLICY_NUMBER: {
      return {
        ...state,
        policyNumber: action.payload.policyNumber,
      };
    }
    case EAPP_ACTION_RESET_APPLICATION: {
      console.warn("REDUCER:: APPLICATION:: EAPP_ACTION_RESET_APPLICATION");
      return {
        ...initialState,
      };
    }
    case EAPP_ACTION_SIGN_FAIL:
    default:
      return state;
  }
};

export default applicationReducer;
