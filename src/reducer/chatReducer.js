import { EAPP_ACTION_RESET_APPLICATION } from "../action/applicationAction";
import { EAPP_ACTION_CHAT_SET } from "../action/chatAction";

const initialState = {
  nextReset: false,
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case EAPP_ACTION_RESET_APPLICATION:
      return {
        ...state,
        nextReset: true,
      };
    case EAPP_ACTION_CHAT_SET:
      return {
        ...state,
        nextReset: action.nextReset,
      };
    default:
      return state;
  }
};

export default chatReducer;
