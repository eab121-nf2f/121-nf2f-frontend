import { TYPE_CHANGE_WS_BROADTOPBAR } from "../action/wsAction";
import { wsBroadTopBar } from "../sockects/wsBroadTopbar";

const initialState = {
  wsBroadTopBar,
};

const wsReducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case TYPE_CHANGE_WS_BROADTOPBAR: {
      return {
        ...state,
        wsBroadTopBar: action.payload.wsBroadTopBar,
      };
    }
    default: {
      return state;
    }
  }
};

export default wsReducer;
