import theme from "../theme.js";
import collapsibleImage from "./Icon/collapsible.png";
import expandableImage from "./Icon/expandable.png";

const onCollapsibleClick = (event) => {
  console.warn("onCollapsibleClick:: ", event);
  const videoContainer = document.getElementById("face-video-container");
  const { children } = videoContainer;
  let isAllCollapsed = null;
  for (const child of children) {
    if (child.nodeName === "DIV") {
      continue;
    }
    const isCollapsed = child.getAttribute("data-collapsed") === "true";
    isAllCollapsed = isCollapsed;
    const styleOption = isCollapsed ? "unset" : "none";
    child.style.setProperty("display", styleOption);
    child.setAttribute("data-collapsed", !isCollapsed);
  }
  const collapsibleButton = document.getElementById("COLLAPSIBLE_BTN");
  collapsibleButton.setAttribute(
    "src",
    isAllCollapsed ? collapsibleImage : expandableImage,
  );
};

export default {
  collapsible: {
    style: {
      width: "100%",
      height: "32px",
      background: theme.background.dark,
      display: "flex",
      "flex-direction": "row",
      "justify-content": "space-around",
      "align-items": "center",
      "font-family": "'Source Sans Pro', sans-serif",
    },
    element: "div",
    children: [
      {
        element: "div",
        style: {
          display: "inherit",
          "flex-direction": "row",
          "align-items": "center",
          "justify-content": "flex-start",
          flex: "1 1 auto",
        },
        children: [
          {
            element: "span",
            style: {
              background: theme.recording,
              "border-radius": "50%",
              height: "10px",
              width: "10px",
              "margin-left": "10px",
              "margin-right": "10px",
            },
          },
          {
            element: "span",
            style: {
              color: "white",
              "font-size": "12px",
            },
            attribute: {
              id: "TIMESTAMP",
            },
            innerHTML: "1234",
            type: "TIMESTAMP",
          },
        ],
      },
      {
        element: "span",
        style: {
          color: "white",
          "font-size": "12px",
          flex: "1 1 auto",
        },
        attribute: {
          id: "RECORD_DATE",
        },
        innerHTML: "1234",
        type: "RECORD_DATE",
      },
      {
        element: "div",
        style: {
          display: "flex",
          "flex-direction": "row",
          "justify-content": "flex-end",
          flex: "1 1 auto",
        },
        children: [
          {
            element: "img",
            attribute: {
              src: collapsibleImage,
              id: "COLLAPSIBLE_BTN",
            },
            style: {
              "margin-right": "10px",
              width: "10px",
              height: "10px",
              "z-index": 10000,
              cursor: "pointer",
              "align-self": "center",
            },
            event: {
              click: onCollapsibleClick,
            },
          },
        ],
      },
    ],
  },
};
