import elementConfig from "./DraggableStyle.json";

function createDraggableElement() {
  let mouseOffsetX = 0;
  let mouseOffsetY = 0;
  let draggableDiv = null;

  function onDragStartHandler(event) {
    const domRect = draggableDiv.getBoundingClientRect();
    mouseOffsetX = event.clientX - domRect.left;
    mouseOffsetY = event.clientY - domRect.top;
  }

  function onDragEndHandler(event) {
    draggableDiv.style.bottom = ``;
    draggableDiv.style.left = `${event.clientX - mouseOffsetX}px`;
    draggableDiv.style.top = `${event.clientY - mouseOffsetY}px`;
  }

  function onTouchStartHandler(event) {
    const domRect = draggableDiv.getBoundingClientRect();
    mouseOffsetX = event.touches[0].clientX - domRect.left;
    mouseOffsetY = event.touches[0].clientY - domRect.top;
  }

  function onTouchEndHandler(event) {
    draggableDiv.style.bottom = ``;
    draggableDiv.style.left = `${event.touches[0].clientX - mouseOffsetX}px`;
    draggableDiv.style.top = `${event.touches[0].clientY - mouseOffsetY}px`;
  }

  function repositioningDraggableDiv() {
    draggableDiv.style.top = ``;
    draggableDiv.style.right = `20px`;
    draggableDiv.style.bottom = `20px`;
    draggableDiv.style.left = ``;
  }

  window.addEventListener("orientationchange", repositioningDraggableDiv);

  draggableDiv = document.createElement("div");
  draggableDiv.addEventListener("dragstart", onDragStartHandler);
  draggableDiv.addEventListener("dragend", onDragEndHandler);
  draggableDiv.addEventListener("touchstart", onTouchStartHandler);
  draggableDiv.addEventListener("touchmove", onTouchEndHandler);
  draggableDiv.setAttribute("id", "face-video-container");
  for (const attributeKey in elementConfig.draggableDiv.attribute) {
    draggableDiv.setAttribute(
      attributeKey,
      elementConfig.draggableDiv.attribute[attributeKey],
    );
  }
  for (const styleKey in elementConfig.draggableDiv.style) {
    draggableDiv.style.setProperty(
      styleKey,
      elementConfig.draggableDiv.style[styleKey],
    );
  }
  document.body.appendChild(draggableDiv);
  return draggableDiv;
}

export default createDraggableElement;
