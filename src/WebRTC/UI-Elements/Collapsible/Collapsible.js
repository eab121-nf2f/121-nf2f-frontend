import { createElement } from "../common";

function createCollapsibleElement(divContainer, config) {
  createElement(divContainer, config);
}

export default createCollapsibleElement;
