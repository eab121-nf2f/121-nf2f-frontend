function createTransitionBar(message) {
  const transitionDiv = document.createElement("div");
  transitionDiv.style.setProperty("width", "70%");
  transitionDiv.style.setProperty("height", "60px");
  transitionDiv.style.setProperty("background-color", "#333");
  transitionDiv.style.setProperty("border-radius", "4px");
  transitionDiv.style.setProperty("position", "absolute");
  transitionDiv.style.setProperty("left", "50%");
  transitionDiv.style.setProperty("top", "50%");
  transitionDiv.style.setProperty("transform", "translate(-50%, -50%)");
  transitionDiv.style.setProperty("text-align", "center");
  transitionDiv.style.setProperty("vertical-align", "baseline");
  transitionDiv.style.setProperty("color", "#fff");

  const pText = document.createElement("p");
  pText.innerText = message;
  transitionDiv.appendChild(pText);

  const wrapper = document.createElement("div");
  wrapper.style.setProperty("width", "100%");
  wrapper.style.setProperty("height", "60px");
  wrapper.style.setProperty("position", "fixed");
  wrapper.style.setProperty("bottom", "20px");
  wrapper.style.setProperty("z-index", "10000");
  wrapper.appendChild(transitionDiv);
  document.body.appendChild(wrapper);

  setTimeout(() => {
    document.body.removeChild(wrapper);
  }, 3000);
}

export default createTransitionBar;
