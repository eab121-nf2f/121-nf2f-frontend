const convertTimeFormat = (milliseconds) => {
  let seconds = (milliseconds / 1000).toFixed(2);
  let minutes = Math.floor(seconds / 60);
  let hours = "";

  hours = Math.floor(minutes / 60);
  hours = hours >= 10 ? hours : `0${hours}`;
  minutes = minutes - hours * 60;
  minutes = minutes >= 10 ? minutes : `0${minutes}`;

  seconds = Math.floor(seconds % 60);
  seconds = seconds >= 10 ? seconds : `0${seconds}`;
  return `${hours}:${minutes}:${seconds}`;
};

function createElement(rootContainer, data) {
  const { style, attribute, children, element, event, type, innerHTML } = data;
  const targetElement = document.createElement(element);
  for (const styleKey in style) {
    targetElement.style.setProperty(styleKey, style[styleKey]);
  }
  for (const attributeKey in attribute) {
    targetElement.setAttribute(attributeKey, attribute[attributeKey]);
  }
  for (const eventKey in event) {
    targetElement.addEventListener(eventKey, event[eventKey]);
  }
  if (type) {
    switch (type) {
      case "TIMESTAMP":
        setInterval(() => {
          const convertTimeFormat = (milliseconds) => {
            let seconds = (milliseconds / 1000).toFixed(2);
            let minutes = Math.floor(seconds / 60);
            let hours = "";

            hours = Math.floor(minutes / 60);
            hours = hours >= 10 ? hours : `0${hours}`;
            minutes = minutes - hours * 60;
            minutes = minutes >= 10 ? minutes : `0${minutes}`;

            seconds = Math.floor(seconds % 60);
            seconds = seconds >= 10 ? seconds : `0${seconds}`;
            return `${hours}:${minutes}:${seconds}`;
          };
          if (document.getElementById("TIMESTAMP")) {
            const currentTime = Date.now();
            const timeStampElement = document.getElementById("TIMESTAMP");
            const previousTime = Number(
              timeStampElement.getAttribute("date-previous"),
            );
            timeStampElement.innerHTML = convertTimeFormat(
              currentTime - previousTime,
            );
          }
        }, [1000]);
        targetElement.setAttribute("date-previous", Date.now());
        targetElement.innerHTML = convertTimeFormat(Date.now());
        break;
      case "RECORD_DATE":
        // targetElement.innerHTML = new Date().toString();
        break;
      // return targetElement.innerHTML = new Date();
    }
  } else if (innerHTML) {
    targetElement.innerHTML = innerHTML;
  }
  rootContainer.appendChild(targetElement);
  if (!children || children.length === 0) {
    return;
  }
  children.forEach((childData) => {
    createElement(targetElement, childData);
  });
}

function removeAllElement(rootContainer) {
  while (rootContainer?.firstChild) {
    rootContainer.removeChild(rootContainer.lastChild);
  }
}

export { createElement, removeAllElement };
