class Recorder {
  constructor({ stream }) {
    this.mediaRecorder = null;
    this.chunks = [];
    this.options = null;
    this.initiate(stream);
  }

  initiate(stream) {
    let options = null;
    try {
      if (MediaRecorder.isTypeSupport("video/mp4;codecs=vp8")) {
        options = {
          mimeType: "video/mp4;codecs=vp8",
        };
      } else {
        options = {
          mimeType: "video/webm;codecs=vp8",
        };
      }
      console.log(options);
    } catch (error) {
      console.warn(`Recorder.js initiate:: error`, error);
      options = {
        mimeType: "video/webm;codecs=vp8",
      };
    }
    this.options = options;

    this.mediaRecorder = new MediaRecorder(stream, this.options);

    this.mediaRecorder.ondataavailable = (event) => {
      console.warn(`Recorder.js ondataavailable:: `, event.data);
      if (event.data.size > 0) {
        this.chunks.push(event.data);
      }
    };

    this.onstart = () => {
      console.warn(`Recorder.js onstart::`);
    };

    this.onpause = () => {
      console.warn(`Recorder.js onpause::`);
    };

    this.onresume = () => {
      console.warn(`Recorder.js onresume::`);
    };

    this.onstop = () => {
      console.warn(`Recorder.js onstop::`);
      const blob = new Blob(this.chunks, {
        type: "video/webm;codecs=vp8",
      });
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement("a");
      a.download = "screen-record.webm";
      a.href = url;
      a.click();
    };
  }

  start(interval) {
    this.mediaRecorder.start(interval);
  }

  pause() {
    this.mediaRecorder.pause();
  }

  resume() {
    this.mediaRecorder.resume();
  }

  stop() {
    this.mediaRecorder.stop();
  }
}

export default Recorder;

// createMediaRecorder(stream) {
//   const mimeTypes = [
//     "video/webm",
//     "audio/webm",
//     "video/webm\;codecs=vp8",
//     "video/webm\;codecs=daala",
//     "video/webm\;codecs=h264",
//     "audio/webm\;codecs=opus",
//     "video/mpeg",
//     "video/mp4"
//   ];
//   let recordType = "video/webm";
//   // Fuck u ios
//   if (MediaRecorder && !MediaRecorder.isTypeSupported) {
//     recordType = "video/mp4";
//   }
//   for (const type of mimeTypes) { 
//     console.log( "Is " + type + " supported? " + (MediaRecorder.isTypeSupported(type) ? "Maybe!" : "Nope :(")); 
//     if (
//       MediaRecorder &&
//       MediaRecorder.isTypeSupported &&
//       MediaRecorder.isTypeSupported(type)
//     ) {
//       recordType = type;
//       // break;
//     }
//   }
//   const recorder = new MediaRecorder(stream);
//   recorder.ondataavailable = (event) => {
//     console.log(event.data);
//     console.log(this);
//     this.chunks.push(event.data);
//   };
//   recorder.onstop = (event) => {
//     const video = document.createElement("video");
//     const blob = new Blob(this.chunks, {
//       type: recordType,
//     });
//     const srcURL = window.URL.createObjectURL(blob);
//     video.src = srcURL;
//     video.setAttribute("controls", true);
//     video.setAttribute("autoplay", true);
//     document.getElementById("root").appendChild(video);
//   };
//   this.recorder = recorder;
//   this.recorder.start(1000);
// }
