import io from "socket.io-client";

import config from "./config.json";
import * as ACTION_TYPES from "./constants/ACTION_TYPES";

class Signaller {
  constructor({ walkie, socketURL }) {
    this.walkie = walkie;
    this.roomId = "";
    this.socket = io(socketURL || config.socketURL, {
      forceNew: true,
      withCredentials: true,
      reconnectionAttempts: "Infinity",
      timeout: 10000,
      // transports: ["websocket"],
    });
    this.socketId = "";
    this.listenMeetingWidget();
    this.listenPeerConnection();
    this.listenSocketServer();
  }

  joinRoom() {
    console.warn("Signaller.js joinRoom:: ");
    const joinRoomData = {
      fromSocketId: this.socketId,
      toSocketId: null,
      roomId: this.roomId,
    };
    this.socket.emit("join_room", joinRoomData);
  }

  sendOffer(sdpOffer, toSocketId) {
    console.warn("Signaller.js sendOffer:: ");
    const offerData = {
      fromSocketId: this.socketId,
      toSocketId: toSocketId,
      roomId: this.roomId,
      sdpOffer: sdpOffer,
    };
    this.socket.emit("offer", offerData);
  }

  sendAnswer(sdpAnswer, toSocketId) {
    console.warn("Signaller.js sendAnswer:: ");
    const answerData = {
      fromSocketId: this.socketId,
      toSocketId: toSocketId,
      roomId: this.roomId,
      sdpAnswer: sdpAnswer,
    };
    this.socket.emit("answer", answerData);
  }

  sendIceCandidate(iceCandidate, toSocketId) {
    console.warn("Signaller.js sendIceCandidate:: ");
    const candidateData = {
      fromSocketId: this.socketId,
      toSocketId: toSocketId,
      roomId: this.roomId,
      iceCandidate: iceCandidate,
    };
    this.socket.emit("candidate", candidateData);
  }

  sendMediaInfo(mediaInfo, toSocketId) {
    console.warn("Signaller.js sendIceCandidate:: ");
    const mediaInfoData = {
      fromSocketId: this.socketId,
      toSocketId: toSocketId,
      roomId: this.roomId,
      mediaInfo: mediaInfo,
    };
    this.socket.emit("media_info", mediaInfoData);
  }

  sendEditUserData(newUserData) {
    const userDataPackage = {
      fromSocketId: this.socketId,
      toSocketId: null,
      roomId: this.roomId,
      userData: newUserData,
    };
    this.socket.emit("USER/Edit", userDataPackage);
  }

  getRoomPeers() {
    this.socket.emit("ROOM/GetUsers", {
      fromSocketId: this.socketId,
      roomId: this.roomId,
    });
  }

  sendQuitRoom() {
    const quitRoomData = {
      fromSocketId: this.socketId,
      toSocketId: null,
      roomId: this.roomId,
    };
    this.socket.emit("quit_meeting", quitRoomData);
  }

  sendCloseMeeting() {
    const closeMeetingData = {
      fromSocketId: this.socketId,
      toSocketId: null,
      roomId: this.roomId,
    };
    this.socket.emit("close_meeting", closeMeetingData);
  }

  sendDataByIO(data) {
    const sendDataBySocketIO = {
      fromSocketId: this.socketId,
      toSocketId: null,
      roomId: this.roomId,
      ...data,
    };
    let stringifyData = "";
    try {
      stringifyData = JSON.stringify(sendDataBySocketIO);
    } catch (error) {
      console.error("sendDataByIO:: ERROR:: ", error);
    }
    this.socket.emit(ACTION_TYPES.SEND_DATA_SOCKETIO, stringifyData);
  }

  listenMeetingWidget() {
    this.walkie.addEventListener("join_room", (event) => {
      console.warn("Signaller.js listenMeetingWidget:: join_room");
      const { roomId } = event.detail;
      this.roomId = roomId;
      this.joinRoom();
    });

    this.walkie.addEventListener("send_offer", (event) => {
      console.warn("Signaller.js listenMeetingWidget:: send_offer");
      const { sdpOffer, toSocketId } = event.detail;
      this.sendOffer(sdpOffer, toSocketId);
    });

    this.walkie.addEventListener("send_answer", (event) => {
      console.warn("Signaller.js listenMeetingWidget:: send_answer");
      const { sdpAnswer, toSocketId } = event.detail;
      this.sendAnswer(sdpAnswer, toSocketId);
    });

    this.walkie.addEventListener("USER/Edit", (event) => {
      console.warn("Signaller.js listenMeetingWidget:: USER/Edit");
      const { userData } = event.detail;
      this.sendEditUserData(userData);
    });

    this.walkie.addEventListener("ROOM/GetUsers", (event) => {
      console.warn("Signaller.js listenMeetingWidget:: ROOM/GetUsers");
      this.getRoomPeers();
    });

    this.walkie.addEventListener("quit_meeting", () => {
      console.warn("Signaller.js listenMeetingWidget:: quit_meeting");
      this.sendQuitRoom();
    });

    this.walkie.addEventListener("close_meeting", () => {
      console.warn("Signaller.js listenMeetingWidget:: close_meeting");
      this.sendCloseMeeting();
    });

    this.walkie.addEventListener(ACTION_TYPES.SEND_DATA_SOCKETIO, (event) => {
      console.warn("Signaller.js: send data though socket.io");
      const { data } = event.detail;
      this.sendDataByIO(data);
    });
  }

  listenPeerConnection() {
    this.walkie.addEventListener("send_ice_candidate", (event) => {
      console.warn("Signaller.js listenPeerConnection:: send_ice_candidate");
      const { iceCandidate, toSocketId } = event.detail;
      this.sendIceCandidate(iceCandidate, toSocketId);
    });

    this.walkie.addEventListener("send_media_info", (event) => {
      console.warn("Signaller.js listenPeerConnection:: send_media_info");
      const { mediaInfo, toSocketId } = event.detail;
      this.sendMediaInfo(mediaInfo, toSocketId);
    });
  }

  listenSocketServer() {
    this.socket.on("your_socket_id", (socketId) => {
      console.warn(
        "Signaller.js listenSocketServer:: your_socket_id",
        socketId,
      );
      this.socketId = socketId;
    });

    this.socket.on("join_room_success", (allParticipantsId) => {
      console.warn(
        "Signaller.js listenSocketServer:: join_room_success",
        allParticipantsId,
      );
    });

    this.socket.on("new_participant", (participantId) => {
      console.warn(
        "Signaller.js listenSocketServer:: new_participant",
        participantId,
      );
      const event = new CustomEvent("new_participant", {
        detail: {
          participantId: participantId,
        },
      });
      this.walkie.dispatchEvent(event);
    });

    this.socket.on("offer", (offerData) => {
      console.warn("Signaller.js listenSocketServer:: offer", offerData);
      const event = new CustomEvent("receive_offer", {
        detail: {
          ...offerData,
        },
      });
      this.walkie.dispatchEvent(event);
    });

    this.socket.on("answer", (answerData) => {
      console.warn("Signaller.js listenSocketServer:: answer", answerData);
      const event = new CustomEvent("receive_answer", {
        detail: {
          ...answerData,
        },
      });
      this.walkie.dispatchEvent(event);
    });

    this.socket.on("candidate", (candidateData) => {
      console.warn(
        "Signaller.js listenSocketServer:: candidate",
        candidateData,
      );
      const event = new CustomEvent("receive_candidate", {
        detail: {
          ...candidateData,
        },
      });
      this.walkie.dispatchEvent(event);
    });

    this.socket.on("media_info", (mediaInfoData) => {
      console.warn(
        "Signaller.js listenSocketServer:: media_info",
        mediaInfoData,
      );
      const event = new CustomEvent("receive_media_info", {
        detail: {
          ...mediaInfoData,
        },
      });
      this.walkie.dispatchEvent(event);
    });

    this.socket.on("ACK/USER/Edit", (resData) => {
      console.warn("Signaller.js listenSocketServer:: ACK/USER/Edit", resData);
    });

    this.socket.on("ACK/ROOM/GetUsers", (resData) => {
      console.warn(
        "Signaller.js listenSocketServer:: ACK/ROOM/GetUsers",
        resData,
      );
      const event = new CustomEvent("ACK/ROOM/GetUsers", {
        detail: {
          users: resData?.users,
        },
      });
      this.walkie.dispatchEvent(event);
    });

    this.socket.on("remote_quit_meeting", (quitRoomData) => {
      console.warn(
        "Signaller.js listenSocketServer:: remote_quit_meeting",
        quitRoomData,
      );
      const event = new CustomEvent("notification", {
        detail: {
          type: "remote_quit_meeting",
          ...quitRoomData,
        },
      });
      this.walkie.dispatchEvent(event);
    });

    this.socket.on("close_meeting", (closeMeetingData) => {
      console.warn(
        "Signaller.js listenSocketServer:: close_meeting",
        closeMeetingData,
      );
      const event = new CustomEvent("notification", {
        detail: {
          type: "close_meeting",
          ...closeMeetingData,
        },
      });
      this.walkie.dispatchEvent(event);
    });

    this.socket.on(ACTION_TYPES.RECEIVE_DATA_SOCKETIO, (data) => {
      console.warn("Signaller.js ", ACTION_TYPES.RECEIVE_DATA_SOCKETIO, data);
      let parseData = {};
      try {
        parseData = JSON.parse(data);
      } catch (error) {
        console.error(
          "ACTION_TYPES.RECEIVE_DATA_SOCKETIO:: ERROR:: ",
          parseData,
        );
      }
      const event = new CustomEvent(ACTION_TYPES.RECEIVE_DATA_SOCKETIO, {
        detail: {
          type: ACTION_TYPES.RECEIVE_DATA_SOCKETIO,
          ...parseData,
        },
      });
      this.walkie.dispatchEvent(event);
    });
  }
}

export default Signaller;
