class UserDevice {
  constructor({ video, audio, screen }) {
    this.video = video;
    this.audio = audio;
    this.screen = screen;
  }

  async getAudioStream() {
    try {
      const constraints = {
        audio: {
          echoCancellation: true,
          noiseSuppression: true,
          sampleSize: { min: 8, max: 16, ideal: 16 },
          sampleRate: 48000,
        },
      };
      const mediaStream = await navigator.mediaDevices.getUserMedia(
        constraints,
      );
      mediaStream.type = "isAudio";
      return mediaStream;
    } catch (error) {
      this.handleException(error);
    }
  }

  async getCameraStream() {
    try {
      const constraints = {
        video: true,
        audio: true,
        // video: {
        //   width: { min: 1280, max: 1920, ideal: 1920 },
        //   height: { min: 720, max: 1080, ideal: 1080 },
        //   aspectRatio: 1.777777778,
        //   frameRate: { min: 15, max: 30, ideal: 30 },
        //   facingMode: "user",
        // },
        // audio: {
        //   echoCancellation: true,
        //   noiseSuppression: true,
        //   sampleSize: { min: 8, max: 16, ideal: 16 },
        //   sampleRate: 48000,
        // }
      };
      const mediaStream = await navigator.mediaDevices.getUserMedia(
        constraints,
      );
      mediaStream.type = "isCamera";
      return mediaStream;
    } catch (error) {
      this.handleException(error);
    }
  }

  async getScreenStream() {
    try {
      const mediaStream = await navigator.mediaDevices.getDisplayMedia();
      mediaStream.type = "isScreen";
      return mediaStream;
    } catch (error) {
      this.handleException(error);
    }
  }

  handleException(domException) {
    if (!(domException instanceof DOMException)) {
      return;
    }
    switch (domException.name) {
      case "AbortError":
      case "InvalidStateError":
      case "NotAllowedError":
      case "NotFoundError":
      case "NotReadableError":
      case "OverconstrainedError":
      case "TypeError":
      default: {
        let message = "";
        message += `Error Name: ${domException.name}\n`;
        message += `Error Message: ${domException.message}\n`;
        alert(message);
        break;
      }
    }
    return domException;
  }
}

export default UserDevice;
