class PeerConnection {
  constructor({ remotePeerId, walkie }) {
    this.remotePeerId = remotePeerId;
    this.walkie = walkie;
    this.peer = null;
    this.localDataChannel = null;
    this.remoteDataChannel = null;
    this.initiate();
  }

  initiate() {
    window.addEventListener("beforeunload", () => {
      this.close();
      this.terminiate();
    });

    const configuration = {
      iceServer: [
        {
          urls: [
            "stun:stun.l.google.com:19302",
            "stun:stun1.l.google.com:19302",
            "stun:stun2.l.google.com:19302",
            "stun:stun.l.google.com:19302?transport=udp",
          ],
        },
      ],
    };
    this.peer = new RTCPeerConnection(configuration);
    this.localDataChannel = this.peer.createDataChannel("chat");
    this.peer.onicecandidate = this.handleIceCandidate.bind(this);
    this.peer.ontrack = this.handleTrack.bind(this);
    this.peer.ondatachannel = this.handleDataChannel.bind(this);
    // this.peer.onnegotiationneeded = this.handleNegotiationNeeded.bind(this);
    // this.peer.onsignalingstatechange = this.handleSignalingStateChange.bind(this);
    this.peer.onconnectionstatechange = this.handleConnectionStateChange.bind(
      this,
    );
    // this.peer.oniceconnectionstatechange = this.handleIceConnectionStateChange.bind(this);
    // this.peer.onicegatheringstatechange = this.handleIceGatheringStateChange.bind(this);
  }

  terminate() {
    this.remotePeerId = null;
    this.walkie = null;
    this.peer.onicecandidate = null;
    this.peer.ontrack = null;
    this.peer.ondatachannel = null;
    this.peer.onnegotiationneeded = null;
    this.peer.onsignalingstatechange = null;
    this.peer.onconnectionstatechange = null;
    this.peer.oniceconnectionstatechange = null;
    this.peer.onicegatheringstatechange = null;
    if (this.localDataChannel !== null) {
      this.localDataChannel.onopen = null;
      this.localDataChannel.onclose = null;
      this.localDataChannel.onmessage = null;
    }
    this.remoteDataChannel = null;
    if (this.remoteDataChannel !== null) {
      this.remoteDataChannel.onopen = null;
      this.remoteDataChannel.onclose = null;
      this.remoteDataChannel.onmessage = null;
    }
    this.remoteDataChannel = null;
    this.peer.close();
    this.peer = null;
  }

  handleIceCandidate(rtcPeerConnectionIceEvent) {
    console.warn(
      `${this.remotePeerId} - handleIceCandidate:: rtcPeerConnectionIceEvent`,
      rtcPeerConnectionIceEvent,
    );
    if (rtcPeerConnectionIceEvent.candidate) {
      const candidateEvent = new CustomEvent("send_ice_candidate", {
        detail: {
          iceCandidate: rtcPeerConnectionIceEvent.candidate,
          toSocketId: this.remotePeerId,
        },
      });
      console.log(candidateEvent);
      this.walkie.dispatchEvent(candidateEvent);
    } else {
      console.warn(`No candidate, ICE gathering has finished`);
    }
  }

  handleTrack(rtcTrackEvent) {
    console.warn(
      `${this.remotePeerId} - handleTrack:: rtcTrackEvent`,
      rtcTrackEvent,
    );
    const streamId = rtcTrackEvent.streams[0].id;
    rtcTrackEvent.track.contentHint = streamId;
    console.log(rtcTrackEvent);
    const event = new CustomEvent("receive_track", {
      detail: {
        stream: rtcTrackEvent.streams[0],
        remotePeerId: this.remotePeerId,
      },
    });
    this.walkie.dispatchEvent(event);
  }

  handleDataChannel(rtcDataChannelEvent) {
    console.warn(
      `${this.remotePeerId} - handleDataChannel:: rtcDataChannelEvent`,
      rtcDataChannelEvent,
    );
    const rtcDataChannel = rtcDataChannelEvent.channel;
    this.remoteDataChannel = rtcDataChannel;

    this.remoteDataChannel.onopen = (event) => {
      console.warn(`${this.remotePeerId} - handleDataChannel:: onopen`, event);
    };

    this.remoteDataChannel.onclose = (event) => {
      console.warn(`${this.remotePeerId} - handleDataChannel:: onclose`, event);
    };

    this.remoteDataChannel.onmessage = (event) => {
      console.warn(
        `${this.remotePeerId} - handleDataChannel:: onmessage`,
        event,
      );
      const { data } = event;
      const messageEvent = new CustomEvent("receive_message", {
        detail: {
          data,
        },
      });
      this.walkie.dispatchEvent(messageEvent);
    };
  }

  handleNegotiationNeeded() {
    console.warn(`${this.remotePeerId} - handleNegotiationNeeded:: `);
    if (this.peer.localDescription === null) {
      return;
    }
    if (this.peer.restartIce !== undefined) {
      this.peer.restartIce();
    } else {
      this.peer
        .createOffer()
        .then((descriptionInit) => {
          const rtcDescription = new RTCSessionDescription(descriptionInit);
          return this.peer.setLocalDescription(rtcDescription);
        })
        .then(() => {
          const event = new CustomEvent("send_offer", {
            detail: {
              offer: this.peer.localDescription,
            },
          });
          this.walkie.dispatchEvent(event);
        })
        .catch((error) => {
          console.warn(
            `${this.remotePeerId} - handleNegotiationNeeded:: error`,
            error,
          );
        });
    }
  }

  handleSignalingStateChange() {
    console.warn(`${this.remotePeerId} - handleSignalingStateChange:: `);
    switch (this.peer.signalingState) {
      case "stable": {
        console.warn(
          `${this.remotePeerId} - handleSignalingStateChange:: "stable"`,
        );
        break;
      }
      case "have-local-offer": {
        console.warn(
          `${this.remotePeerId} - handleSignalingStateChange:: "have-local-offer"`,
        );
        break;
      }
      case "have-remote-offer": {
        console.warn(
          `${this.remotePeerId} - handleSignalingStateChange:: "have-remote-offer"`,
        );
        break;
      }
      case "have-local-pranswer": {
        console.warn(
          `${this.remotePeerId} - handleSignalingStateChange:: "have-local-pranswer"`,
        );
        break;
      }
      case "have-remote-pranswer": {
        console.warn(
          `${this.remotePeerId} - handleSignalingStateChange:: "have-remote-pranswer"`,
        );
        break;
      }
      default: {
        console.warn(
          `${this.remotePeerId} - handleSignalingStateChange:: "${this.peer.signalingState}"`,
        );
      }
    }
  }

  async handleConnectionStateChange() {
    console.warn(`${this.remotePeerId} - handleConnectionStateChange:: `);
    console.log(this);
    switch (this.peer.connectionState) {
      case "new": {
        console.warn(
          `${this.remotePeerId} - handleConnectionStateChange:: "new"`,
        );
        break;
      }
      case "connecting": {
        console.warn(
          `${this.remotePeerId} - handleConnectionStateChange:: "connecting"`,
        );
        break;
      }
      case "connected": {
        console.warn(
          `${this.remotePeerId} - handleConnectionStateChange:: "connected"`,
        );
        break;
      }
      case "disconnected": {
        console.warn(
          `${this.remotePeerId} - handleConnectionStateChange:: "disconnected"`,
        );
        const event = new CustomEvent("remote-peer-disconnected", {
          detail: {
            remotePeerId: this.remotePeerId,
          },
        });
        this.walkie.dispatchEvent(event);
        this.peer.close();
        break;
      }
      case "failed": {
        console.warn(
          `${this.remotePeerId} - handleConnectionStateChange:: "failed"`,
        );
        await this.retry();
        break;
      }
      case "closed": {
        console.warn(
          `${this.remotePeerId} - handleConnectionStateChange:: "closed"`,
        );
        break;
      }
      default: {
        console.warn(
          `${this.remotePeerId} - handleConnectionStateChange:: "${this.peer.connectionState}"`,
        );
        break;
      }
    }
  }

  async handleIceConnectionStateChange() {
    console.warn(`${this.remotePeerId} - handleIceConnectionStateChange:: `);
    switch (this.peer.iceConnectionState) {
      case "new": {
        console.warn(
          `${this.remotePeerId} - handleIceConnectionStateChange:: "new"`,
        );
        break;
      }
      case "checking": {
        console.warn(
          `${this.remotePeerId} - handleIceConnectionStateChange:: "checking"`,
        );
        break;
      }
      case "connected": {
        console.warn(
          `${this.remotePeerId} - handleIceConnectionStateChange:: "connected"`,
        );
        break;
      }
      case "completed": {
        console.warn(
          `${this.remotePeerId} - handleIceConnectionStateChange:: "completed"`,
        );
        break;
      }
      case "failed": {
        console.warn(
          `${this.remotePeerId} - handleIceConnectionStateChange:: "failed"`,
        );
        // await this.retry();
        break;
      }
      case "disconnected": {
        console.warn(
          `${this.remotePeerId} - handleIceConnectionStateChange:: "disconnected"`,
        );
        // const event = new CustomEvent("remote-peer-disconnected", {
        //   detail: {
        //     remotePeerId: this.remotePeerId,
        //   }
        // });
        // this.walkie.dispatchEvent(event);
        // this.peer.close();
        break;
      }
      case "closed": {
        console.warn(
          `${this.remotePeerId} - handleIceConnectionStateChange:: "closed"`,
        );
        break;
      }
      default: {
        console.warn(
          `${this.remotePeerId} - handleIceConnectionStateChange:: "${this.peer.iceConnectionState}"`,
        );
      }
    }
  }

  handleIceGatheringStateChange() {
    console.warn(`${this.remotePeerId} - handleIceGatheringStateChange:: `);
    switch (this.peer.iceGatheringState) {
      case "new": {
        console.warn(
          `${this.remotePeerId} - handleIceGatheringStateChange:: "new"`,
        );
        break;
      }
      case "gathering": {
        console.warn(
          `${this.remotePeerId} - handleIceGatheringStateChange:: "gathering"`,
        );
        break;
      }
      case "complete": {
        console.warn(
          `${this.remotePeerId} - handleIceGatheringStateChange:: "complete"`,
        );
        break;
      }
      default: {
        console.warn(
          `${this.remotePeerId} - handleIceGatheringStateChange:: "${this.peer.iceGatheringState}"`,
        );
      }
    }
  }

  async retry() {
    console.warn(`${this.remotePeerId} retry:: `);
    const sdpOffer = await this.peer.createOffer({
      iceRestart: true,
    });
    await this.peer.setLocalDescription(sdpOffer);
    const offerEvent = new CustomEvent("send_offer", {
      detail: {
        sdpOffer,
        toSocketId: this.remotePeerId,
      },
    });
    this.walkie.dispatchEvent(offerEvent);
  }

  createDataChannel(label, rtcDataChannelInit) {
    return this.peer.createDataChannel(label, rtcDataChannelInit);
  }

  sendData(data) {
    console.warn(`${this.remotePeerId} - sendData:: `, data);
    if (this.localDataChannel.readyState === "open") {
      this.localDataChannel.send(JSON.stringify(data));
    } else {
      console.warn(`${this.remotePeerId} - sendData:: `, this.localDataChannel);
    }
  }

  async addIceCandidate(iceCandidateInit) {
    const rtcIceCandidate = new RTCIceCandidate(iceCandidateInit);
    await this.peer.addIceCandidate(rtcIceCandidate);
  }

  addStream(stream) {
    if (!stream) {
      console.warn("PeerConnection:: addStream:: Invalid Stream");
      return;
    }
    console.log(`${this.remotePeerId} addStream:: `, stream);
    const mediaInfo = {
      streamId: stream.id,
      streamType: stream.type,
      tracks: [],
    };

    stream.getTracks().forEach((track) => {
      this.peer.addTrack(track, stream);
      mediaInfo.tracks.push({
        trackId: track.id,
        trackKind: track.kind,
      });
    });

    const event = new CustomEvent("send_media_info", {
      detail: {
        mediaInfo,
        toSocketId: this.remotePeerId,
      },
    });
    this.walkie.dispatchEvent(event);
  }

  removeTrack(rtpSender) {
    this.peer.removeTrack(rtpSender);
  }

  async createOffer() {
    const options = {
      voiceActivityDetection: false,
    };
    return await this.peer.createOffer(options);
  }

  async createAnswer() {
    const options = {
      voiceActivityDetection: false,
    };
    return await this.peer.createAnswer(options);
  }

  async setLocalDescription(descriptionInit) {
    const rtcDescription = new RTCSessionDescription(descriptionInit);
    await this.peer.setLocalDescription(rtcDescription);
  }

  async setRemoteDescription(descriptionInit) {
    const rtcDescription = new RTCSessionDescription(descriptionInit);
    await this.peer.setRemoteDescription(rtcDescription);
  }

  getTransceivers() {
    return this.peer.getTransceivers();
  }

  getSenders() {
    return this.peer.getSenders();
  }

  getReceivers() {
    return this.peer.getReceivers();
  }

  stopTransceiver(rtpTransceiver) {
    rtpTransceiver.stop();
  }

  getConfiguration() {
    return this.peer.getConfiguration();
  }

  setConfiguration(rtcConfiguration) {
    this.peer.setConfiguration(rtcConfiguration);
  }

  close() {
    console.warn(`${this.remotePeerId} close:: `);
    this.terminate();
  }
}

export default PeerConnection;
