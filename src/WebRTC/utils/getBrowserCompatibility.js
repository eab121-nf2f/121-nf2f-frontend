const Screen = navigator?.mediaDevices?.getDisplayMedia;
const Camera = navigator?.mediaDevices?.getUserMedia;

export { Screen, Camera };
