import * as ACTION_TYPES from "./constants/ACTION_TYPES";
import EventTarget from "./EventTarget.js";
import PeerConnection from "./PeerConnection.js";
import Recorder from "./Recorder.js";
import Signaller from "./Signaller.js";
import styleConfig from "./style.json";
import createCollapsibleElement from "./UI-Elements/Collapsible/Collapsible.js";
import { removeAllElement } from "./UI-Elements/common";
import uiConfig from "./UI-Elements/config";
import createDraggableElement from "./UI-Elements/Draggable/Draggable.js";
import createTransitionBar from "./UI-Elements/Transition/Transition";
import UserDevice from "./UserDevice.js";
import { getBrowserCompatibility } from "./utils";

const ROLE = {
  HOST: "HOST",
  PARTICIPANT: "PARTICIPANT",
  VIEWER: "VIEWER",
};

class MeetingWidget {
  constructor({
    socketURL,
    recorderOptions = {},
    screenContainer = null,
    cameraContainer = null,
    role = ROLE.HOST,
  }) {
    this.role = role;
    this.walkie = new EventTarget();
    this.signaller = new Signaller({
      walkie: this.walkie,
      socketURL,
    });
    this.peerConnections = {};
    this.peerMediaInfo = {};
    this.peerDetails = [];
    this.userDevice = new UserDevice({
      video: true,
      audio: true,
      screen: true,
    });
    this.screenContainer = screenContainer;
    this.cameraContainer = cameraContainer;
    this.screenRecorder = null;
    this.cameraRecorder = null;
    this.recorderOptions = {
      screen: recorderOptions.screen,
      camera: recorderOptions.camera,
    };
    this.audioStream = null;
    this.cameraStream = null;
    this.screenStream = null;
    this.faceVideoContainer = null;
    this.screenVideoContainer = null;
    this.started = false;
    this.joined = false;
    this.onmessage = null;
    this.onNotification = null;
    this.currentSelectedPeer = "";
    this.initiate();
  }

  initiate() {
    window.addEventListener("beforeunload", () => {
      this.close();
      this.terminiate();
    });

    this.listenSignaller();
    this.listenPeerConnection();
    // createTransitionBar();
  }

  terminiate() {
    this.walkie = null;
    this.signaller = null;
    this.peerConnections = null;
    this.peerMediaInfo = null;
    this.userDevice = null;
    this.audioStream = null;
    this.cameraStream = null;
    this.screenStream = null;
    this.localFaceContainer = null;
    this.localScreenContainer = null;
    this.remoteFaceContainer = null;
    this.started = false;
    this.joined = false;
    this.onmessage = null;
    this.screenRecorder = null;
    this.cameraRecorder = null;
    this.currentSelectedPeer = null;
  }

  getRemotePeerList() {
    return Object.keys(this.peerConnections);
  }

  getAllPeersList() {
    const getAllUsersEvent = new CustomEvent("ROOM/GetUsers");
    this.walkie.dispatchEvent(getAllUsersEvent);
  }

  selectedRemotePeer(socketId) {
    this.currentSelectedPeer = socketId;
  }

  sendMessageData(messageData, toSocketId = this.currentSelectedPeer) {
    console.log(this.peerConnections);
    // if (toSocketId !== null && toSocketId !== undefined && toSocketId !== "") {
    //   const currentPeer = this.findPeerConnection(toSocketId);
    //   if (currentPeer !== undefined) {
    //     currentPeer.sendData(messageData);
    //   }
    // } else {
    //   for (const remotePeerId in this.peerConnections) {
    //     this.peerConnections[remotePeerId].sendData(messageData);
    //   }
    // }
    const ioPassEvent = new CustomEvent(ACTION_TYPES.SEND_DATA_SOCKETIO, {
      detail: {
        data: messageData,
      },
    });
    this.walkie.dispatchEvent(ioPassEvent);
  }

  showBottomNotificationBar(message) {
    createTransitionBar(message);
  }

  // eslint-disable-next-line no-unused-vars
  createVideoElement(stream, container, socketId, isMuted = false) {
    try {
      console.log(stream);
      let dataSocketId = "socket-id-";
      if (socketId === undefined) {
        dataSocketId += this.signaller.socketId;
      } else {
        dataSocketId += socketId;
      }
      const video = document.createElement("video");
      video.srcObject = stream;
      video.setAttribute("id", stream.id);
      video.setAttribute("data-socket-id", dataSocketId);
      // video.setAttribute("muted", isMuted);
      for (const attributeKey in styleConfig.video.attribute) {
        video.setAttribute(
          attributeKey,
          styleConfig.video.attribute[attributeKey],
        );
      }
      for (const styleKey in styleConfig.video.style) {
        video.style.setProperty(styleKey, styleConfig.video.style[styleKey]);
      }
      container.appendChild(video);
      video.play();
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  pauseCamera = (muteAudio = true) => {
    const isPausedCameraStream = this.cameraStream
      ?.getTracks()
      .some((track) => track.enabled === false);
    if (!isPausedCameraStream) {
      this.mute(this.cameraStream, true, muteAudio);
    }
  };

  pauseScreen = (muteAudio = true) => {
    const isPausedScreenStream = this.screenStream
      ?.getTracks()
      .some((track) => track.enabled === false);
    if (!isPausedScreenStream) {
      this.mute(this.screenStream, true, muteAudio);
    }
  };

  resumeCamera = (muteAudio = true) => {
    const isPausedCameraStream = this.cameraStream
      ?.getTracks()
      .some((track) => track.enabled === false);
    if (isPausedCameraStream) {
      this.unmute(this.cameraStream, true, muteAudio);
    }
  };

  resumeScreen = (muteAudio = true) => {
    const isPausedScreenStream = this.screenStream
      ?.getTracks()
      .some((track) => track.enabled === false);
    if (isPausedScreenStream) {
      this.unmute(this.screenStream, true, muteAudio);
    }
  };

  muteCamera = () => {
    const isMutedCameraStream = this.cameraStream
      ?.getAudioTracks()
      .some((track) => track.enabled === false);
    if (!isMutedCameraStream) {
      this.mute(this.cameraStream, false, true);
    }
  };

  muteScreen = () => {
    const isMutedScreenStream = this.screenStream
      ?.getAudioTracks()
      .some((track) => track.enabled === false);
    if (!isMutedScreenStream) {
      this.mute(this.screenStream, false, true);
    }
  };

  unmuteCamera = () => {
    const isMutedCameraStream = this.cameraStream
      ?.getAudioTracks()
      .some((track) => track.enabled === false);
    if (isMutedCameraStream) {
      this.unmute(this.cameraStream, false, true);
    }
  };

  unmuteScreen = () => {
    const isMutedScreenStream = this.screenStream
      ?.getAudioTracks()
      .some((track) => track.enabled === false);
    if (isMutedScreenStream) {
      this.unmute(this.screenStream, false, true);
    }
  };

  mute = (stream, muteVideo = true, muteAudio = true) => {
    if (muteAudio) {
      stream.getAudioTracks().forEach((track) => (track.enabled = false));
    }
    if (muteVideo) {
      stream.getTracks().forEach((track) => (track.enabled = false));
    }
  };

  unmute = (stream, unmuteVideo = true, unmuteAudio = true) => {
    if (unmuteAudio) {
      stream.getAudioTracks().forEach((track) => (track.enabled = true));
    }
    if (unmuteVideo) {
      stream.getTracks().forEach((track) => (track.enabled = true));
    }
  };

  startRecording = () => {
    const onStreamRecordScreen = this.recorderOptions?.screen?.onStreamRecord;
    const onStreamRecordCamera = this.recorderOptions?.camera?.onStreamRecord;

    if (onStreamRecordScreen || onStreamRecordCamera) {
      console.warn(
        "[Conference] | Record | Service not available as auto-controlled",
        onStreamRecordScreen,
        onStreamRecordCamera,
      );
      return;
    }
    if (this.recorderOptions?.screen) {
      if (!this.screenRecorder) {
        console.warn("[Conference] | Record | screenRecorder not found");
      } else {
        this.screenRecorder.mediaRecorder.start();
      }
    }
    if (this.recorderOptions?.camera) {
      if (!this.cameraRecorder) {
        console.warn("[Conference] | Record | cameraRecorder not found");
      } else {
        this.cameraRecorder.mediaRecorder.start();
      }
    }
  };

  stopRecoding = () => {
    if (this.screenRecorder) {
      this.screenRecorder.mediaRecorder.stop();
    }
    if (this.cameraRecorder) {
      this.cameraRecorder.mediaRecorder.stop();
    }
  };

  // ? We use the following `this` properties
  // ? this.cameraStream
  // ? this.screenStream
  // ? this.recorderOptions
  createRecorders = () => {
    // const addedRecorder = [];
    // for (const option in this.recorderOptions) {
    //   const { type, audio } = option;
    //   const isRecorderAdded = addedRecorder.find(
    //     (existRecorder) => existRecorder.type === type,
    //   );
    //   if (isRecorderAdded) {
    //     continue;
    //   }
    //   let targetStream = new MediaStream();
    //   switch (type) {
    //     case "Camera":
    //       targetStream = this.cameraStream;
    //       break;
    //     case "Screen":
    //       targetStream = this.screenStream;
    //       break;
    //     default:
    //       break;
    //   }
    //   const recorder = new Recorder({ stream: targetStream });
    //   this.recorders.push(recorder);
    // }
    const {
      screen: screenRecorderOption,
      camera: cameraRecorderOption,
    } = this.recorderOptions;

    const onStreamRecordScreen = this.recorderOptions?.screen?.onStreamRecord;
    const onStreamRecordCamera = this.recorderOptions?.camera?.onStreamRecord;

    if (cameraRecorderOption && this.cameraStream) {
      this.cameraRecorder = new Recorder({ stream: this.cameraStream });
      if (onStreamRecordCamera) {
        this.cameraRecorder.start();
      }
    }
    if (screenRecorderOption && this.screenStream) {
      this.screenRecorder = new Recorder({ stream: this.screenStream });
      if (onStreamRecordScreen) {
        this.screenRecorder.start();
      }
    }
  };

  async destroyVideoElement(dataSocketId) {
    const selector = `[data-socket-id="socket-id-${dataSocketId}"]`;
    console.log(selector);
    const videos = document.querySelectorAll(selector);
    videos.forEach((video) => {
      video.parentNode.removeChild(video);
      console.log(video);
    });
    this.peerMediaInfo[dataSocketId] = [];
  }

  async start() {
    if (this.started === false) {
      this.started = true;
      this.faceVideoContainer = createDraggableElement();
      createCollapsibleElement(this.faceVideoContainer, uiConfig.collapsible);
      // this.audioStream = await this.userDevice.getAudioStream();
      const isScreenCaptureSupported = getBrowserCompatibility.Screen;
      const isCameraCaptureSupported = getBrowserCompatibility.Camera;
      if (isScreenCaptureSupported && this.role === ROLE.HOST) {
        this.screenStream = await this.userDevice.getScreenStream();
        // await this.createVideoElement(
        //   this.screenStream,
        //   this.faceVideoContainer,
        // );
      }
      if (isCameraCaptureSupported) {
        this.cameraStream = await this.userDevice.getCameraStream();
        await this.createVideoElement(
          this.cameraStream,
          this.faceVideoContainer,
        );
        const localFaceVideoContainer = document.getElementById(
          "local-face-video-container",
        );
        console.log(localFaceVideoContainer);
        await this.createVideoElement(
          this.cameraStream,
          localFaceVideoContainer,
        );
      }
      this.createRecorders();
    }
  }

  join(roomId) {
    if (this.joined === false) {
      this.joined = true;
      const event = new CustomEvent("join_room", {
        detail: {
          roomId,
        },
      });
      this.walkie.dispatchEvent(event);
    }
  }

  close() {
    console.warn("MeetingWidget.js close:: ");
    for (const remotePeerId in this.peerConnections) {
      const currentPeer = this.findPeerConnection(remotePeerId);
      this.destroyVideoElement(currentPeer.remotePeerId);
      currentPeer.close();
    }
    this.cameraStream.getTracks().forEach((track) => {
      track.stop();
    });
    this.screenStream.getTracks().forEach((track) => {
      track.stop();
    });
    removeAllElement(this.faceVideoContainer);
    this.destroyVideoElement(this.signaller.socketId);

    const event = new Event("close_meeting");
    this.walkie.dispatchEvent(event);
  }

  quit() {
    const event = new Event("quit_meeting");
    this.walkie.dispatchEvent(event);
  }

  findPeerConnection(remoteSocketId) {
    return this.peerConnections[remoteSocketId];
  }

  editUser = (userData) => {
    const editUserEvent = new CustomEvent("USER/Edit", {
      detail: {
        userData,
      },
    });
    this.walkie.dispatchEvent(editUserEvent);
  };

  listenSignaller() {
    this.walkie.addEventListener("new_participant", async (event) => {
      console.warn("MeetingWidget.js listenSignaller:: new_participant");
      const { participantId } = event.detail;
      const currentPeer = this.findPeerConnection(participantId);
      if (currentPeer === undefined) {
        const peerConnection = new PeerConnection({
          remotePeerId: participantId,
          walkie: this.walkie,
        });
        peerConnection.addStream(this.cameraStream);
        peerConnection.addStream(this.screenStream);
        // peerConnection.addStream(this.audioStream);
        const sdpOffer = await peerConnection.createOffer();
        await peerConnection.setLocalDescription(sdpOffer);
        const offerEvent = new CustomEvent("send_offer", {
          detail: {
            sdpOffer,
            toSocketId: participantId,
          },
        });
        this.walkie.dispatchEvent(offerEvent);
        this.peerConnections[participantId] = peerConnection;
      }
    });

    this.walkie.addEventListener("receive_offer", async (event) => {
      console.warn("MeetingWidget.js listenSignaller:: receive_offer");
      const { fromSocketId, sdpOffer } = event.detail;
      const currentPeer = this.findPeerConnection(fromSocketId);
      if (currentPeer === undefined) {
        const peerConnection = new PeerConnection({
          remotePeerId: fromSocketId,
          walkie: this.walkie,
        });
        await peerConnection.setRemoteDescription(sdpOffer);
        peerConnection.addStream(this.cameraStream);
        peerConnection.addStream(this.screenStream);
        // peerConnection.addStream(this.audioStream);
        const sdpAnswer = await peerConnection.createAnswer();
        await peerConnection.setLocalDescription(sdpAnswer);
        const answerEvent = new CustomEvent("send_answer", {
          detail: {
            sdpAnswer,
            toSocketId: fromSocketId,
          },
        });
        this.walkie.dispatchEvent(answerEvent);
        this.peerConnections[fromSocketId] = peerConnection;
      } else {
        currentPeer.setRemoteDescription(sdpOffer);
        const sdpAnswer = await currentPeer.createAnswer();
        await currentPeer.setLocalDescription(sdpAnswer);
        const answerEvent = new CustomEvent("send_answer", {
          detail: {
            sdpAnswer,
            toSocketId: fromSocketId,
          },
        });
        this.walkie.dispatchEvent(answerEvent);
      }
    });

    this.walkie.addEventListener("receive_answer", async (event) => {
      console.warn("MeetingWidget.js listenSignaller:: receive_answer");
      const { fromSocketId, sdpAnswer } = event.detail;
      const currentPeer = this.findPeerConnection(fromSocketId);
      if (currentPeer !== undefined) {
        await currentPeer.setRemoteDescription(sdpAnswer);
      }
    });

    this.walkie.addEventListener("receive_candidate", async (event) => {
      console.warn("MeetingWidget.js listenSignaller:: receive_candidate");
      const { fromSocketId, iceCandidate } = event.detail;
      const currentPeer = this.findPeerConnection(fromSocketId);
      if (currentPeer !== undefined) {
        await currentPeer.addIceCandidate(iceCandidate);
      }
    });

    this.walkie.addEventListener("receive_media_info", async (event) => {
      console.warn("MeetingWidget.js listenSignaller:: receive_media_info");
      const { fromSocketId, mediaInfo } = event.detail;
      if (this.peerMediaInfo[fromSocketId] === undefined) {
        this.peerMediaInfo[fromSocketId] = [];
        this.peerMediaInfo[fromSocketId].push(mediaInfo);
      } else {
        this.peerMediaInfo[fromSocketId].push(mediaInfo);
      }
    });

    this.walkie.addEventListener("ACK/ROOM/GetUsers", async (event) => {
      console.warn("MeetingWidget.js listenSignaller:: ACK/ROOM/GetUsers");
      const users = event?.detail?.users;
      this.peerDetails = users;
      this.onReceivedPeerData(this.peerDetails);
    });
  }

  listenPeerConnection() {
    this.walkie.addEventListener("receive_track", (event) => {
      const { stream, remotePeerId } = event.detail;
      const currentPeerMediaInfo = this.peerMediaInfo[remotePeerId];
      currentPeerMediaInfo.forEach((mediaInfo) => {
        if (mediaInfo.streamId === stream.id) {
          switch (mediaInfo.streamType) {
            case "isScreen": {
              this.createVideoElement(
                stream,
                this.screenContainer
                  ? this.screenContainer
                  : document.getElementById("root"),
                remotePeerId,
              );
              break;
            }
            case "isCamera": {
              const remoteFaceVideo = document.getElementById(stream.id);
              console.log(remoteFaceVideo);
              if (remoteFaceVideo === null) {
                this.createVideoElement(
                  stream,
                  this.faceVideoContainer,
                  remotePeerId,
                );
              }
              break;
            }
            case "isAudio": {
              this.createVideoElement(
                stream,
                this.faceVideoContainer,
                remotePeerId,
              );
              break;
            }
            default: {
              console.log("default");
              this.createVideoElement(
                stream,
                this.faceVideoContainer,
                remotePeerId,
              );
            }
          }
        }
      });
    });

    this.walkie.addEventListener("receive_message", (event) => {
      const { data } = event.detail;
      this.onmessage(data);
    });

    this.walkie.addEventListener("notification", (event) => {
      const data = event.detail;
      const { fromSocketId, ...otherData } = data;
      const theName =
        this.peerDetails.find((user) => user?.id === fromSocketId) ||
        fromSocketId;
      this.onNotification({
        fromSocketId: theName,
        ...otherData,
      });
    });

    this.walkie.addEventListener("remote-peer-disconnected", (event) => {
      const { remotePeerId } = event.detail;
      this.destroyVideoElement(remotePeerId);
      delete this.peerConnections[remotePeerId];
    });

    this.walkie.addEventListener(
      ACTION_TYPES.RECEIVE_DATA_SOCKETIO,
      (event) => {
        console.warn(
          "MeetingWidget:: ACTION_TYPES.RECEIVE_DATA_SOCKETIO:: ",
          event,
        );
        this.onmessage(event?.detail);
      },
    );
  }
}

export default MeetingWidget;
