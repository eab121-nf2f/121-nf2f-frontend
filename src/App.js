// #region styling
import "typeface-source-sans-pro";

import { responsiveFontSizes, ThemeProvider } from "@material-ui/core/styles";
import React from "react";
import { Provider as ReduxProvider } from "react-redux";
// #endregion
// #region redux
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

import Router from "./page";
// import TestComponent from './component/Test';
// #endregion redux
import reducer from "./reducer";
import theme from "./theme/theme";

export const store = createStore(reducer, applyMiddleware(thunk));
const responsiveTheme = responsiveFontSizes(theme);

function App() {
  return (
    <ReduxProvider store={store}>
      <ThemeProvider theme={responsiveTheme}>
        <Router />
        {/* <TestComponent /> */}
      </ThemeProvider>
    </ReduxProvider>
  );
}

export default App;
