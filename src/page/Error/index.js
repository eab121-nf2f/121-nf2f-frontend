import { Button, Typography } from "@material-ui/core";
import React from "react";
import { useTranslation } from "react-i18next";
import { Redirect } from "react-router-dom";

import { ERROR_PAGE_NOT_FOUND } from "../../constant/page";
import { ROUTE_AGENT, ROUTE_LOGIN } from "../../constant/route";
import styles from "./Error.module.css";

function ErrorPage(props) {
  const { t } = useTranslation();

  // ! Temporary Solution
  if (true) {
    return <Redirect to={{ pathname: `${ROUTE_AGENT}${ROUTE_LOGIN}` }} />;
  }

  return (
    <div className={styles.container}>
      <div>
        <Typography variant="h1" display="inline" className={styles.typoBold}>
          {t(`${ERROR_PAGE_NOT_FOUND}.TITLE_BOLD`)}
          <Typography variant="h1" display="inline">
            {t(`${ERROR_PAGE_NOT_FOUND}.TITLE_NORMAL`)}
          </Typography>
        </Typography>
      </div>
      <div className={styles.descriptionContainer}>
        <Typography variant="h1">
          {t(`${ERROR_PAGE_NOT_FOUND}.DESCRIPTION`)}
        </Typography>
      </div>
      {/* <div>
        <Button color="primary" variant="contained">
          {t("BUTTON.HOME")}
        </Button>
      </div> */}
    </div>
  );
}

export default ErrorPage;
