import AddIcon from "@material-ui/icons/Add";
import RefreshIcon from "@material-ui/icons/Refresh";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";

import { EAPP_ACTION_RESET_APPLICATION } from "../../action/applicationAction";
import { LANDING } from "../../constant/page";
import { ROUTE_EAPP, ROUTE_UPLOADPROPOSAL } from "../../constant/route";
import styles from "./LandingPage.module.css";

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  roomId: state.socket.roomId,
});

const mapDispatchToProps = dispatch => ({
  resetApplication: () => {
    dispatch({
      type: EAPP_ACTION_RESET_APPLICATION,
    });
  },
});

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t, history, resetApplication } = this.props;

    return (
      <div className={styles.LandingPage}>
        <div
          className={styles.LeftSection}
          onClick={() => {
            resetApplication();
            history.push(
              `${ROUTE_EAPP}${ROUTE_UPLOADPROPOSAL}/${this.props.roomId}`,
            );
          }}
        >
          <div className={styles.TitleWrapper}>
            <p className={styles.Title}>{t(`${LANDING}.NEW`)}</p>
            <p className={styles.Subtitle}>{t(`${LANDING}.EAPPLICATION`)}</p>
          </div>
          <AddIcon className={styles.AddIcon} fontSize="large" />
        </div>
        <Link to={"/summary"} className={styles.Link}>
          <div className={styles.RightSection}>
            <div className={styles.TitleWrapper}>
              <p className={styles.Title}>{t(`${LANDING}.EXISTING`)}</p>
              <p className={styles.Subtitle}>{t(`${LANDING}.EAPPLICATION`)}</p>
            </div>
            <RefreshIcon className={styles.RefreshIcon} fontSize="large" />
          </div>
        </Link>
      </div>
    );
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(LandingPage)),
);
