import { Typography } from "@material-ui/core";
import clsx from "clsx";
import React, { Component, Fragment } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import webSocket from "socket.io-client";

import {
  changeAddressBase64Action,
  changeAddressFileNameAction,
  changeIdCardBase64Action,
  changeIdCardFileNameAction,
} from "../../action/applicationAction";
import {
  disableNextBtnSupportingDocAction,
  enableNextBtnSupportingDocAction,
} from "../../action/nextBtnAction";
import {
  disableStepSupportingDocAction,
  enableStepSupportingDocAction,
} from "../../action/stepperAction";
import { ReactComponent as UploadIcon } from "../../assets/images/icons/upload.svg";
import { BroadcastTopbar } from "../../component";
import config from "../../config/config.json";
import {
  EAPP_SUPPORTINGDOCUMENT,
  EAPP_UPLOADPROPOSAL,
} from "../../constant/page";
import styles from "./SupportingDocument.module.css";

const mapSTateToProps = state => ({
  roomId: state.socket.roomId,
  addressFileBase64: state.application.addressFileBase64,
  idCardFileBase64: state.application.idCardFileBase64,
  shareMode: state.socket.shareMode,
  identity: state.auth.identity,
  controlMode: state.socket.controlMode,
  idCardFileName: state.application.idCardFileName,
  addressFileName: state.application.addressFileName,
});

const mapDispatchToProps = dispatch => ({
  changeIDCardFileHandler: idCardFileBase64 => {
    dispatch(changeIdCardBase64Action(idCardFileBase64));
  },
  changeAddressFileHandler: addressFileBase64 => {
    dispatch(changeAddressBase64Action(addressFileBase64));
  },
  enableNextBtnSupportingDocHandler: () => {
    dispatch(enableNextBtnSupportingDocAction());
  },
  disableNextBtnSupportingDocHandler: () => {
    dispatch(disableNextBtnSupportingDocAction());
  },
  enableStepSupportingDocHandler: () => {
    dispatch(enableStepSupportingDocAction());
  },
  disableStepSupportingDocHandler: () => {
    dispatch(disableStepSupportingDocAction());
  },
  changeIdCardFileNameHandler: idCardFileName => {
    dispatch(changeIdCardFileNameAction(idCardFileName));
  },
  changeAddressFileNameHandler: addressFileName => {
    dispatch(changeAddressFileNameAction(addressFileName));
  },
});

class SupportingDocument extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedAddressFile: null,
      selectedAddressFileName: "",
      selectedIDFile: null,
      selectedIDFileName: "",
      ws: null,
      username: "Test User",
    };
    this.myRef = React.createRef();
    this.mousePointerRef = React.createRef();
  }

  componentDidMount = () => {
    this.connectWebSocket();
  };

  connectWebSocket = () => {
    const webSocketInstance = webSocket(
      `${config.socketHost}/eapp/supportdocs`,
    );
    const data = {
      username: this.state.username,
      roomId: this.props.roomId,
    };
    webSocketInstance.emit("joinRoom", data);

    webSocketInstance.on("newClientJoin", data => {
      console.log(data);
    });

    webSocketInstance.on("scrollEventToOther", data => {
      if (this.myRef !== null && this.myRef !== undefined) {
        this.myRef.scrollTo({
          top: data.position.y,
          left: data.position.x,
          behavior: "smooth",
        });
      }
    });

    webSocketInstance.on("mouseClickEventToOther", data => {
      this.mousePointerRef.current.style.display = "";
      this.mousePointerRef.current.style.left = `${data.mousePointerX}px`;
      this.mousePointerRef.current.style.top = `${data.mousePointerY}px`;
      this.mousePointerRef.current.style.display = "block";
      setTimeout(() => {
        this.mousePointerRef.current.style.display = "";
      }, 500);
    });

    webSocketInstance.on("uploadAddressFileEventToOther", data => {
      console.log("uploadAddressFileEventToOther");
      console.log(data);
      this.setState({
        selectedAddressFile: data.selectedAddressFile,
        selectedAddressFileName: data.selectedAddressFileName,
      });
      this.props.changeAddressFileHandler(data.addressFileBase64);
      this.props.changeAddressFileNameHandler(data.selectedAddressFileName);
    });

    webSocketInstance.on("uploadIDCardFileEventToOther", data => {
      console.log("uploadIDCardFileEventToOther");
      console.log(data);
      this.setState({
        selectedIDFile: data.selectedIDFile,
        selectedIDFileName: data.selectedIDFileName,
      });
      this.props.changeIDCardFileHandler(data.idCardFileBase64);
      this.props.changeIdCardFileNameHandler(data.selectedIDFileName);
    });

    this.setState({ ws: webSocketInstance });
  };

  onScrollHandler = e => {
    const data = {
      roomId: this.props.roomId,
      position: {
        x: e.target.scrollLeft,
        y: e.target.scrollTop,
      },
    };
    this.state.ws.emit("scrollEvent", data);
  };

  onMouseDownHandler = e => {
    this.mousePointerRef.current.style.display = "";
    this.mousePointerRef.current.style.left = `${e.clientX}px`;
    this.mousePointerRef.current.style.top = `${e.clientY}px`;
    this.mousePointerRef.current.style.display = "block";
    setTimeout(() => {
      this.mousePointerRef.current.style.display = "";
    }, 500);
    const data = {
      roomId: this.props.roomId,
      mousePointerX: e.clientX,
      mousePointerY: e.clientY,
    };
    this.state.ws.emit("mouseClickEvent", data);
  };

  isDisableNext = () => {
    console.log(this.props.addressFileBase64);
    console.log(this.props.idCardFileBase64);
    if (
      this.props.addressFileBase64 !== "" &&
      this.props.idCardFileBase64 !== ""
    ) {
      this.props.enableNextBtnSupportingDocHandler();
      this.props.enableStepSupportingDocHandler();
    } else {
      this.props.enableStepSupportingDocHandler();
      this.props.disableStepSupportingDocHandler();
    }
  };

  selectAddressHandler = e => {
    if (e.target.files[0] !== undefined) {
      const fileToLoad = e.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = fileLoadedEvent => {
        const base64 = fileLoadedEvent.target.result;
        this.setState({ base64File: base64 });
        this.props.changeAddressFileHandler(base64);
        this.props.changeAddressFileNameHandler(fileToLoad.name);
        this.isDisableNext();

        const data = {
          roomId: this.props.roomId,
          selectedAddressFile: fileToLoad,
          selectedAddressFileName: fileToLoad.name,
          addressFileBase64: base64,
        };
        this.state.ws.emit("uploadAddressFileEvent", data);
      };
      fileReader.readAsDataURL(fileToLoad);
      if (e.target.files !== null) {
        this.setState({
          selectedAddressFile: e.target.files[0],
          selectedAddressFileName: e.target.files[0].name,
        });
      }
    }
  };

  selectIDHandler = e => {
    if (e.target.files[0] !== undefined) {
      const fileReader = new FileReader();
      const fileToLoad = e.target.files[0];
      fileReader.onload = fileLoadedEvent => {
        const base64 = fileLoadedEvent.target.result;
        this.setState({ base64File: base64 });
        this.props.changeIDCardFileHandler(base64);
        this.props.changeIdCardFileNameHandler(fileToLoad.name);
        this.isDisableNext();

        const data = {
          roomId: this.props.roomId,
          selectedIDFile: fileToLoad,
          selectedIDFileName: fileToLoad.name,
          idCardFileBase64: base64,
        };
        this.state.ws.emit("uploadIDCardFileEvent", data);
      };
      fileReader.readAsDataURL(fileToLoad);
      console.log(e.target.files[0]);
      if (e.target.files !== null) {
        this.setState({
          selectedIDFile: e.target.files[0],
          selectedIDFileName: e.target.files[0].name,
        });
      }
    }
  };

  render() {
    const { t } = this.props;

    return (
      <div
        className={[
          styles.SupportingDocument,
          this.props.shareMode ? "ShareMode" : "NotShareMode",
          this.props.controlMode ? "ControlMode" : "NotControlMode",
        ].join(" ")}
        onScroll={this.onScrollHandler}
        onClick={this.onMouseDownHandler}
        ref={ref => (this.myRef = ref)}
      >
        {this.props.shareMode && this.props.identity === "agent" ? <BroadcastTopbar /> : null}
        <div className={"mousePointer"} ref={this.mousePointerRef}></div>

        <div className={styles.ContentBox}>
          <div className={styles.Title}>
            {t(`${EAPP_SUPPORTINGDOCUMENT}.TITLE`)}
          </div>
          <div>
            <div className={styles.RowStyle}>
              <div className={styles.Number}>
                {t(`${EAPP_SUPPORTINGDOCUMENT}.FIRST`)}
              </div>
              <div className={styles.Subtitle}>
                {t(`${EAPP_SUPPORTINGDOCUMENT}.ADDRESS_PROOF`)}
              </div>
            </div>
            <div className={styles.RowStyle}>
              <div className={styles.VerticalLine2}></div>
              <div className={styles.UploadBox}>
                <input
                  type="file"
                  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps"
                  className={styles.InputFile}
                  onChange={this.selectAddressHandler}
                />
                <div
                  className={clsx(styles.UploadIconAndText, {
                    [styles.hidden]: this.props.addressFileBase64,
                  })}
                >
                  <UploadIcon />
                  <p className={styles.FileNameText}>
                    {this.props.addressFileName ? (
                      this.props.addressFileName
                    ) : (
                      <Fragment>
                        <Typography display="inline">
                          {t(`${EAPP_SUPPORTINGDOCUMENT}.DRAG_DROP`)}
                          <Typography
                            display="inline"
                            className={styles.highlightColor}
                          >
                            {t(`${EAPP_SUPPORTINGDOCUMENT}.BROWSE`)}
                          </Typography>
                          {t(`${EAPP_SUPPORTINGDOCUMENT}.FILE`)}
                        </Typography>
                      </Fragment>
                    )}
                  </p>
                  {this.props.addressFileBase64 && (
                    <a
                      className={styles.downloadText}
                      download={this.state.selectedAddressFileName}
                      href={this.props.addressFileBase64}
                    >
                      {t(`${EAPP_SUPPORTINGDOCUMENT}.DOWNLOAD`)}
                    </a>
                  )}
                </div>
              </div>
            </div>
          </div>

          <div>
            <div className={styles.RowStyle}>
              <div className={styles.Number}>
                {t(`${EAPP_SUPPORTINGDOCUMENT}.SECOND`)}
              </div>
              <div className={styles.Subtitle}>
                {t(`${EAPP_SUPPORTINGDOCUMENT}.ID_CARD`)}
              </div>
            </div>
            <div className={styles.RowStyle}>
              <div className={styles.VerticalLine2}></div>
              <div className={styles.UploadBox}>
                <input
                  type="file"
                  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps"
                  className={styles.InputFile}
                  onChange={this.selectIDHandler}
                />
                <div
                  className={clsx(styles.UploadIconAndText, {
                    [styles.hidden]: this.props.idCardFileBase64,
                  })}
                >
                  <UploadIcon />
                  <p className={styles.FileNameText}>
                    {this.props.idCardFileName ? (
                      this.props.idCardFileName
                    ) : (
                      <Fragment>
                        <Typography display="inline">
                          {t(`${EAPP_SUPPORTINGDOCUMENT}.DRAG_DROP`)}
                          <Typography
                            display="inline"
                            className={styles.highlightColor}
                          >
                            {t(`${EAPP_SUPPORTINGDOCUMENT}.BROWSE`)}
                          </Typography>
                          {t(`${EAPP_SUPPORTINGDOCUMENT}.FILE`)}
                        </Typography>
                      </Fragment>
                    )}
                  </p>
                  {this.props.idCardFileBase64 && (
                    <a
                      className={styles.downloadText}
                      download={this.state.selectedIDFileName}
                      href={this.props.idCardFileBase64}
                    >
                      {t(`${EAPP_SUPPORTINGDOCUMENT}.DOWNLOAD`)}
                    </a>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  mapSTateToProps,
  mapDispatchToProps,
)(withTranslation()(SupportingDocument));
