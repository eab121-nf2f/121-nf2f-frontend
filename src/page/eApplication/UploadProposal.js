import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import clsx from "clsx";
import React, { Component, Fragment } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import webSocket from "socket.io-client";

import {
  changeProductIdAction,
  changeProposalBase64Action,
  changeProposalNameAction,
  changeSelectedProductAction,
} from "../../action/applicationAction";
import {
  disableNextBtnUploadProposalAction,
  enableNextBtnUploadProposalAction,
} from "../../action/nextBtnAction";
import {
  changeRoomIdAction,
  chnageClientLoginSuccessRedirectToAction,
} from "../../action/socketAction";
import {
  disableStepApplicationAction,
  enableStepApplicationAction,
} from "../../action/stepperAction";
import { getProductList, UploadApplicationProposal } from "../../api";
import { ReactComponent as UploadIcon } from "../../assets/images/icons/upload.svg";
import BroadcastTopbar from "../../component/BroadcastTopBar";
import config from "../../config/config.json";
import randomRoomId from "../../utils/randomRoomId";
import styles from "./UploadProposal.module.css";

const mapStateToProps = state => ({
  productId: state.application.productId,
  proposalPDFBase64: state.application.proposalPDFBase64,
  isAuthenticated: state.auth.isAuthenticated,
  identity: state.auth.identity,
  roomId: state.socket.roomId,
  shareMode: state.socket.shareMode,
  controlMode: state.socket.controlMode,
  proposalName: state.application.proposalName,
  selectedProduct: state.application.selectedProduct,
});

const mapDispatchToProps = dispatch => ({
  changeProductIdHandler: productId => {
    dispatch(changeProductIdAction(productId));
  },
  changeProposalBase64Handler: proposalPDFBase64 => {
    dispatch(changeProposalBase64Action(proposalPDFBase64));
  },
  changeRoomIdHandler: roomId => {
    dispatch(changeRoomIdAction(roomId));
  },
  chnageClientLoginSuccessRedirectToHandler: redirectToURL => {
    dispatch(chnageClientLoginSuccessRedirectToAction(redirectToURL));
  },
  enableNextButtonHandler: () => {
    dispatch(enableNextBtnUploadProposalAction());
  },
  disableNextButtonHandler: () => {
    dispatch(disableNextBtnUploadProposalAction());
  },
  enableStepApplicationHandler: () => {
    dispatch(enableStepApplicationAction());
  },
  disableStepApplicationHandler: () => {
    dispatch(disableStepApplicationAction());
  },
  changeProposalNameHandler: proposalName => {
    dispatch(changeProposalNameAction(proposalName));
  },
  changeSelectedProductHandler: selectedProduct => {
    dispatch(changeSelectedProductAction(selectedProduct));
  },
});

class UploadProposal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      selectedProduct: "",
      selectedProductId: "",
      isOpenDropdown: false,
      selectedFile: null,
      selectedFileName: "",
      base64File: "",
      socketId: "",
      mosuePosition: { x: 0, y: 0 },
    };
    this.myRef = React.createRef();
    this.mousePointerRef = React.createRef();
    this.ws = webSocket(`${config.socketHost}/eapp/upload`);
  }

  componentDidMount = () => {
    this.props.changeRoomIdHandler(this.props.roomId);
    getProductList()
      .then(res => {
        console.log(res);
        this.setState({ productList: res.data });
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
    this.connectWebSocket();
  };

  connectWebSocket = () => {
    const data = {
      roomId: this.props.roomId,
    };
    this.ws.emit("joinRoom", data);

    this.ws.on("newClientJoin", data => {
      console.log(data);
      const syncData = {
        roomId: this.props.roomId,
        productId: this.props.productId,
        selectedProduct: this.props.selectedProduct,
        proposalName: this.props.proposalName,
        proposalPDFBase64: this.props.proposalPDFBase64,
      };
      this.ws.emit("syncData", syncData);
    });

    // this.ws.on("receiveSyncData", data => {
    //   console.log(data);
    //   this.props.changeProposalBase64Handler(data.proposalPDFBase64);
    //   this.props.changeProposalNameHandler(data.proposalName);
    //   this.props.changeProductIdHandler(data.productId);
    //   this.props.changeSelectedProductHandler(data.selectedProduct);
    // });

    this.ws.on("scrollEventToOther", data => {
      if (this.myRef !== null && this.myRef !== undefined) {
        this.myRef.scrollTo({
          top: data.position.y,
          left: data.position.x,
          behavior: "smooth",
        });
      }
    });

    this.ws.on("mouseClickEventToOther", data => {
      this.mousePointerRef.current.style.display = "";
      this.mousePointerRef.current.style.left = `${data.mousePointerX}px`;
      this.mousePointerRef.current.style.top = `${data.mousePointerY}px`;
      this.mousePointerRef.current.style.display = "block";
      setTimeout(() => {
        this.mousePointerRef.current.style.display = "";
      }, 500);
    });

    this.ws.on("openDropdownEventToOther", data => {
      this.setState({ isOpenDropdown: data.isOpenDropdown });
    });

    this.ws.on("selectProductEventToOther", data => {
      this.setState({
        selectedProduct: data.selectedProduct,
        selectedProductId: data.selectedProductId,
        isOpenDropdown: data.isOpenDropdown,
      });
      this.props.changeSelectedProductHandler(data.product);
    });

    this.ws.on("uploadProposalPDFEventToOther", data => {
      this.setState({
        selectedFile: data.selectedFile,
        selectedFileName: data.selectedFileName,
      });
      this.props.changeProposalBase64Handler(data.proposalPDFBase64);
      this.props.changeProposalNameHandler(data.selectedFileName);
    });
  };

  onScrollHandler = e => {
    const data = {
      roomId: this.props.roomId,
      position: {
        x: e.target.scrollLeft,
        y: e.target.scrollTop,
      },
    };
    this.ws.emit("scrollEvent", data);
  };

  onMouseDownHandler = e => {
    this.mousePointerRef.current.style.display = "";
    this.mousePointerRef.current.style.left = `${e.clientX}px`;
    this.mousePointerRef.current.style.top = `${e.clientY}px`;
    this.mousePointerRef.current.style.display = "block";
    setTimeout(() => {
      this.mousePointerRef.current.style.display = "";
    }, 500);
    const data = {
      roomId: this.props.roomId,
      mousePointerX: e.clientX,
      mousePointerY: e.clientY,
    };
    this.ws.emit("mouseClickEvent", data);
  };

  isDisableNext = () => {
    console.log(this.props.productId);
    console.log(this.props.proposalPDFBase64);
    if (this.props.productId !== "" && this.props.proposalPDFBase64 !== "") {
      this.props.enableNextButtonHandler();
      this.props.enableStepApplicationHandler();
    } else {
      this.props.disableNextButtonHandler();
      this.props.disableStepApplicationHandler();
    }
  };

  toggleDropdown = () => {
    this.setState({ isOpenDropdown: !this.state.isOpenDropdown });
    const data = {
      roomId: this.props.roomId,
      isOpenDropdown: !this.state.isOpenDropdown,
    };
    this.ws.emit("openDropdownEvent", data);
  };

  changeProductHandler = value => {
    this.setState({
      selectedProduct: value.name.en_US,
      selectedProductId: value._id,
      isOpenDropdown: !this.state.isOpenDropdown,
    });

    this.props.changeSelectedProductHandler(value);
    this.props.changeProductIdHandler(value._id);
    const data = {
      roomId: this.props.roomId,
      selectedProduct: value.name.en_US,
      selectedProductId: value._id,
      isOpenDropdown: !this.state.isOpenDropdown,
      product: value,
    };
    this.ws.emit("selectProductEvent", data);
    this.isDisableNext();
  };

  selectFileHandler = e => {
    if (e.target.files[0] !== undefined) {
      const fileToLoad = e.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = fileLoadedEvent => {
        const base64 = fileLoadedEvent.target.result;
        this.setState({ base64File: base64 });
        this.props.changeProposalBase64Handler(base64);
        this.props.changeProposalNameHandler(fileToLoad.name);
        this.isDisableNext();

        const data = {
          roomId: this.props.roomId,
          selectedFile: fileToLoad,
          selectedFileName: fileToLoad.name,
          proposalPDFBase64: base64,
        };
        this.ws.emit("uploadProposalPDFEvent", data);
      };
      fileReader.readAsDataURL(fileToLoad);
      if (e.target.files !== null) {
        this.setState({
          selectedFile: e.target.files[0],
          selectedFileName: e.target.files[0].name,
        });
        this.props.changeProposalNameHandler(e.target.files[0].name);
      }
    }
  };

  render() {
    const { t } = this.props;
    return (
      <div
        className={[styles.UploadProposal,
        (this.props.shareMode ? "ShareMode" : "NotShareMode"),
        (this.props.controlMode ? "ControlMode" : "NotControlMode")
        ].join(" ")}
        onScroll={this.onScrollHandler}
        onClick={this.onMouseDownHandler}
        ref={ref => (this.myRef = ref)}
      >
        {this.props.shareMode && this.props.identity === "agent" ? <BroadcastTopbar /> : null}
        <div className={"mousePointer"} ref={this.mousePointerRef}></div>

        <div className={styles.ContentBox}>
          <div className={styles.Title}>{t(`EAPP_UPLOADPROPOSAL.TITLE`)}</div>
          <div>
            <div className={styles.RowStyle}>
              <div className={styles.Number}>1</div>
              <div className={styles.Subtitle}>
                {t(`EAPP_UPLOADPROPOSAL.SUBTITLE_1`)}
              </div>
            </div>
            <div className={styles.RowStyle}>
              <div className={styles.VerticalLine1}></div>
              <div className={styles.DropdownBox}>
                <div
                  className={styles.DropdownText}
                  onClick={this.toggleDropdown}
                >
                  <div className={styles.Text}>
                    {this.props.selectedProduct ? this.props.selectedProduct.name[(this.props.i18n.language ? this.props.i18n.language : "en_US")] : ""}
                  </div>
                  <div>
                    {this.state.isOpenDropdown ? (
                      <ExpandLessIcon />
                    ) : (
                        <ExpandMoreIcon />
                      )}
                  </div>
                </div>

                {this.state.isOpenDropdown && (
                  <ul className={styles.DropdownList}>
                    {this.state.productList.map(product => (
                      <li
                        key={product._id}
                        className={styles.DropdownItem}
                        onClick={() => {
                          this.changeProductHandler(product);
                        }}
                      >
                        {
                          product.name[
                          this.props.i18n.language
                            ? this.props.i18n.language
                            : "en_US"
                          ]
                        }
                      </li>
                    ))}
                  </ul>
                )}
              </div>
            </div>
          </div>
          <div>
            <div className={styles.RowStyle}>
              <div className={styles.Number}>2</div>
              <div className={styles.Subtitle}>
                {t(`EAPP_UPLOADPROPOSAL.SUBTITLE_2`)}
              </div>
            </div>
            <div className={styles.RowStyle}>
              <div className={styles.VerticalLine2}></div>
              <div className={styles.UploadBox}>
                <input
                  type="file"
                  accept="application/pdf"
                  className={styles.InputFile}
                  onChange={this.selectFileHandler}
                />
                <div
                  className={clsx(styles.UploadIconAndText, {
                    [styles.hiddenText]: !this.props.proposalPDFBase64,
                  })}
                >
                  <UploadIcon />
                  <p className={styles.FileNameText}>
                    {this.props.proposalPDFBase64 ? (
                      this.props.proposalName
                    ) : (
                        <Fragment>
                          {t(`EAPP_UPLOADPROPOSAL.UPLOAD_BOX_TEXT_START`)}
                          <span>
                            {t(`EAPP_UPLOADPROPOSAL.UPLOAD_BOX_TEXT_BROWSE`)}
                          </span>
                          {t(`EAPP_UPLOADPROPOSAL.UPLOAD_BOX_TEXT_END`)}
                        </Fragment>
                      )}
                  </p>
                  {this.props.proposalPDFBase64 && (
                    <a
                      download={this.state.selectedFileName}
                      href={this.props.proposalPDFBase64}
                    >
                      {t(`EAPP_UPLOADPROPOSAL.DOWNLOAD`)}
                    </a>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withTranslation()(UploadProposal)),
);
