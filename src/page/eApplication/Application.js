import clsx from "clsx";
import React, { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import webSocket from "socket.io-client";

import { onCafeSaveIntercept } from "../../action/applicationAction";
import { BUTTON_ACTION_SETONCLICK } from "../../action/buttonAction";
import {
  disableNextBtnApplicationAction,
  enableNextBtnApplicationAction,
} from "../../action/nextBtnAction";
import {
  disableStepSignatureAction,
  enableStepSignatureAction,
} from "../../action/stepperAction";
import { getCafeResponse } from "../../api";
import { BroadcastTopbar, CafeForm } from "../../component";
import config from "../../config/config.json";
import { ROUTE_EAPP, ROUTE_SIGNATURE } from "../../constant/route";
import styles from "./Application.module.css";

const { cafe: cafeConfig } = config;
const webSocketInstance = webSocket(`${config.socketHost}/eapp/application`);

export default function Application(props) {
  const cafe = useSelector(state => state.cafe);
  const socket = useSelector(state => state.socket);
  const auth = useSelector(state => state.auth);
  const { i18n } = useTranslation();
  const dispatch = useDispatch();
  const [ws] = useState(webSocketInstance);
  const [valueMap, setValueMap] = useState({});
  const cafeRef = React.useRef(null);
  const [pageIndex, setPageIndex] = useState(0);
  const [isFilled, setIsFilled] = useState(cafe.isCompleted);
  const mousePointerRef = useRef(null);
  //   const history = useHistory();

  useEffect(() => {
    console.warn("Application:: useEffect:: Redux:: ", cafe);
  }, [cafe]);

  useEffect(() => {
    const data = {
      roomId: socket.roomId,
    };
    ws.emit("joinRoom", data);
  }, [socket.roomId]);

  const changeNewValueMap = newValueMap => {
    // console.warn("newValueMap:: ", newValueMap);
    const data = {
      roomId: socket.roomId,
      newValueMap,
    };
    ws.emit("newValueMapEvent", data);
  };

  useEffect(() => {
    ws.on("newValueMapEventToOther", data => {
      // console.log(data);
      // if (valueMap !== data.newValueMap) {
        setValueMap(data.newValueMap);
      // };
    }, [valueMap]);

    ws.on("scrollEventToOther", data => {
      console.log(data);
      document.getElementById("QuestionFormSection").scrollTo({
        top: data.position.y,
        left: data.position.x,
        behavior: "smooth",
      });
    });

    ws.on("mouseClickEventToOther", data => {
      mousePointerRef.current.style.display = "";
      mousePointerRef.current.style.left = `${data.mousePointerX}px`;
      mousePointerRef.current.style.top = `${data.mousePointerY}px`;
      mousePointerRef.current.style.display = "block";
      setTimeout(() => {
        mousePointerRef.current.style.display = "";
      }, 500);
    });

    ws.on("onClickSidebarEventToShare", data => {
      console.log(data);
      setPageIndex(data.sidebarIndexPage);
    });
  });

  const onScrollHandler = e => {
    const data = {
      roomId: socket.roomId,
      position: {
        x: e.target.scrollLeft,
        y: e.target.scrollTop,
      },
    };
    console.log(data);
    ws.emit("scrollEvent", data);
  };

  const onMouseDownHandler = e => {
    mousePointerRef.current.style.display = "";
    mousePointerRef.current.style.left = `${e.clientX}px`;
    mousePointerRef.current.style.top = `${e.clientY}px`;
    mousePointerRef.current.style.display = "block";
    setTimeout(() => {
      mousePointerRef.current.style.display = "";
    }, 500);
    const data = {
      roomId: socket.roomId,
      mousePointerX: e.clientX,
      mousePointerY: e.clientY,
    };
    ws.emit("mouseClickEvent", data);
  };

  const onClickSidebarHandler = num => {
    setPageIndex(num);
    const data = {
      roomId: socket.roomId,
      sidebarIndexPage: num,
    };
    console.log(data);
    ws.emit("onClickSidebarEvent", data);
  };

  useEffect(() => {
    const dom = document.getElementById("QuestionFormSection");
    console.log(dom);
    if (dom !== null) {
      dom.addEventListener("scroll", onScrollHandler);
    }
    const sidebarItem1 = document.getElementById("vertical_page_0");
    if (sidebarItem1 !== null) {
      sidebarItem1.addEventListener("click", () => onClickSidebarHandler(0));
    }
    const sidebarItem2 = document.getElementById("vertical_page_1");
    if (sidebarItem2 !== null) {
      sidebarItem2.addEventListener("click", () => onClickSidebarHandler(1));
    }
    const sidebarItem3 = document.getElementById("vertical_page_2");
    if (sidebarItem3 !== null) {
      sidebarItem3.addEventListener("click", () => onClickSidebarHandler(2));
    }
  });

  const applicationFormID = cafe.formId;

  console.warn("Application:: [i18n]:: language:: ", i18n.language);

  const onCafeFormSubmitted = async cafeSubmitResponse => {
    // ? If the application saved
    // ? we first fetch the latest changes
    console.warn("onApplicationSaved:: ", cafeSubmitResponse);
    const { responseID } = cafeSubmitResponse;
    dispatch(onCafeSaveIntercept(responseID, applicationFormID));
    // ? After that we redirect the page for user
    // history.push(`${ROUTE_EAPP}${ROUTE_SIGNATURE}`);
    // if (cafe.isCompleted) {
    // dispatch(enableNextBtnApplicationAction());
    // dispatch(enableStepSignatureAction());
    // } else {
    //   dispatch(disableNextBtnApplicationAction());
    //   dispatch(disableStepSignatureAction());
    // }
  };

  console.warn("i18n | Language:: ", i18n.language);

  const doSave = async () => {
    // console.warn("refs:: ", cafeRef.current.content.save());
    const response = await cafeRef.current.content.save();
    console.warn("doSave:: ", response);
  };

  useEffect(() => {
    if (cafeRef.current) {
      dispatch({
        type: BUTTON_ACTION_SETONCLICK,
        onClick: doSave,
      });
    }
  }, [cafeRef.current]);

  useEffect(() => {
    console.log("applicaiton:: ", cafe.isCompleted);
    if (isFilled) {
      dispatch(enableNextBtnApplicationAction());
      dispatch(enableStepSignatureAction());
    }
  }, [isFilled]);

  console.warn("cafeReducer:: ", cafe);

  return (
    <div
      className={clsx(styles.container, {
        ShareMode: socket.shareMode,
        NotShareMode: !socket.shareMode,
        ControlMode: socket.controlMode,
        NotControlMode: !socket.controlMode,
      })}
      onClick={onMouseDownHandler}
    >
      {/* <div className={(socket.shareMode ? "ShareMode": "NotShareMode")}></div> */}
      {socket.shareMode && auth.identity === "agent" ? (
        <BroadcastTopbar />
      ) : null}
      <div className={"mousePointer"} ref={mousePointerRef}></div>

      <CafeForm
        innerRef={cafeRef}
        className={styles.cafeForm}
        formID={applicationFormID}
        responseID={cafe.responseId}
        language={i18n.language}
        onDone={onCafeFormSubmitted}
        isWebUI={true}
        pageIndex={pageIndex}
        valueMap={valueMap}
        onChangeValueMap={changeNewValueMap}
        onAllMandatoryFilled={() => {
          setIsFilled(true);
        }}
      />
    </div>
  );
}
