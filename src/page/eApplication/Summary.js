import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import {
  deleteApplication,
  getApplicationList,
  getProductList,
} from "../../api";
import { ReactComponent as AddIcon } from "../../assets/images/icons/add.svg";
import ClientCard from "../../component/ClientCard/ClientCard";
import styles from "./Summary.module.css";

const mapStateToProps = state => ({
  roomId: state.socket.roomId,
});

class Summary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      applicationList: [],
      productList: [],
      showStatus: "all",
    };
  }

  componentDidMount = () => {
    getApplicationList()
      .then(res => {
        console.log(res.data);
        this.setState({ applicationList: res.data });
      })
      .catch(err => {
        console.log(err);
      });
    getProductList()
      .then(res => {
        console.log(res);
        this.setState({ productList: res.data });
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    const { t } = this.props;

    let displayList = null;
    if (this.state.showStatus === "all") {
      displayList = this.state.applicationList;
    } else {
      displayList = this.state.applicationList.filter(
        application => application.status === this.state.showStatus,
      );
    }

    const inProgressNum = this.state.applicationList.filter(
      application => application.status === "I",
    ).length;

    const submittedNum = this.state.applicationList.filter(
      application => application.status === "S",
    ).length;

    const deleteNum = this.state.applicationList.filter(
      application => application.status === "D",
    ).length;

    const allNum = inProgressNum + submittedNum + deleteNum;

    return (
      <div className={styles.ApplicationSummery}>
        <div className={styles.ToolSection}>
          <div className={styles.StatusSection}>
            <div
              className={[
                styles.Status,
                this.state.showStatus === "all" ? styles.StatusSelect : null,
              ].join(" ")}
              onClick={() => this.setState({ showStatus: "all" })}
            >
              {t("EAPP_SUMMARY.ALL")}
              <sup>({allNum})</sup>
            </div>
            <div
              className={[
                styles.Status,
                this.state.showStatus === "I" ? styles.StatusSelect : null,
              ].join(" ")}
              onClick={() => this.setState({ showStatus: "I" })}
            >
              {t("EAPP_SUMMARY.IN_PROGRESS")}
              <sup>({inProgressNum})</sup>
            </div>
            <div
              className={[
                styles.Status,
                this.state.showStatus === "S" ? styles.StatusSelect : null,
              ].join(" ")}
              onClick={() => this.setState({ showStatus: "S" })}
            >
              {t("EAPP_SUMMARY.SUBMITTED")}
              <sup>({submittedNum})</sup>
            </div>
            <div
              className={[
                styles.Status,
                this.state.showStatus === "D" ? styles.StatusSelect : null,
              ].join(" ")}
              onClick={() => this.setState({ showStatus: "D" })}
            >
              {t("EAPP_SUMMARY.DELETED")}
              <sup>({deleteNum})</sup>
            </div>
          </div>
          <div className={styles.AddIconBox}>
            <Link to={`/eapp/upload/${this.props.roomId}`}>
              <AddIcon className={styles.AddIcon} />
            </Link>
          </div>
        </div>
        <div className={styles.ClientCardSection}>
          {displayList.map((application, index) => {
            let productName = "";
            for (let i = 0; i < this.state.productList.length; i++) {
              if (this.state.productList[i]._id === application.productId) {
                productName = this.state.productList[i].name[
                  this.props.i18n.language || "en_US"
                ];
              }
            }
            const onClickDelete = applicationId => async () => {
              const deleteResponse = await deleteApplication(applicationId);
              if (deleteResponse.status === 200) {
                // ? We fetch data again
                getApplicationList()
                  .then(res => {
                    console.log(res.data);
                    this.setState({ applicationList: res.data });
                  })
                  .catch(err => {
                    console.log(err);
                  });
                getProductList()
                  .then(res => {
                    console.log(res);
                    this.setState({ productList: res.data });
                    console.log(res.data);
                  })
                  .catch(err => {
                    console.log(err);
                  });
              }
            };
            return (
              <ClientCard
                key={index}
                status={application.status}
                clientName={application.clientName}
                applicationNumber={application.policyNumber || application._id}
                productName={productName}
                creationDate={application.createdDate}
                lastUpdatedDate={application.updatedDate}
                onClickDelete={onClickDelete(application._id)}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(withTranslation()(Summary));

// [
//   {
//     status: "in-progress",
//     clientName: "DAVID CHAN",
//     applicationNumber: "A000001",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "submitted",
//     clientName: "DAVID CHAN",
//     applicationNumber: "A000002",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "deleted",
//     clientName: "DAVID CHAN",
//     applicationNumber: "A000003",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "in-progress",
//     clientName: "DAVID CHAN",
//     applicationNumber: "B000001",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "submitted",
//     clientName: "DAVID CHAN",
//     applicationNumber: "B000002",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "deleted",
//     clientName: "DAVID CHAN",
//     applicationNumber: "B000003",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "in-progress",
//     clientName: "DAVID CHAN",
//     applicationNumber: "C000001",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "submitted",
//     clientName: "DAVID CHAN",
//     applicationNumber: "C000002",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "deleted",
//     clientName: "DAVID CHAN",
//     applicationNumber: "C000003",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
//   {
//     status: "in-progress",
//     clientName: "DAVID CHAN",
//     applicationNumber: "D000001",
//     productName: "QDAP",
//     creationDate: "2020/02/10 10:21:21",
//     lastUpdatedDate: "2020/02/10 10:21:21",
//   },
// ]
