import clsx from "clsx";
import React, { useEffect, useState, useRef } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import webSocket from "socket.io-client";

import config from "../../config/config.json";
import { onApplicationSignIntercept } from "../../action/applicationAction";
import { ApplicationSideBar, SignHandler, SignViewer, BroadcastTopbar } from "../../component";
import { PDF_DISCLOSURE, PDF_PROPOSAL } from "../../constant/attachment";
import { ROUTE_EAPP, ROUTE_SUPPORTINGDOCUMENT } from "../../constant/route";
import styles from "./Signature.module.css";
import { enableNextBtnSignatureAction } from "../../action/nextBtnAction";
import { enableStepSupportingDocAction, chnageStepNumberAction } from "../../action/stepperAction";

const webSocketInstance = webSocket(`${config.socketHost}/eapp/sign`);

const getPdf = (pdfArr, pdfKey, propKey = "data") => {
  const pdfItem = (pdfArr || []).find(item => item.key === pdfKey);
  console.warn("getPdf:: ", propKey, pdfItem);
  return pdfItem ? pdfItem[propKey] : null;
};

export default function Signature(props) {
  const { t } = useTranslation();
  const application = useSelector(state => state.application);
  const socket = useSelector(state => state.socket);
  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();

  console.warn("Signature:: application:: ", application);

  const [step, setStep] = useState(0);
  const mousePointerRef = useRef(null);

  const signedProposal = getPdf(application.applicationPdfs, PDF_PROPOSAL);
  const signedDisclosure = getPdf(application.applicationPdfs, PDF_DISCLOSURE);
  const templateProposal = getPdf(application.proposalTemplates, PDF_PROPOSAL);
  const templateDisclosure = getPdf(
    application.proposalTemplates,
    PDF_DISCLOSURE,
  );
  const templateProposalSignRules = getPdf(
    application.proposalTemplates,
    PDF_PROPOSAL,
    "rule",
  );
  const templateDisclosureSignRules = getPdf(
    application.proposalTemplates,
    PDF_DISCLOSURE,
    "rule",
  );

  const isProposalSigned = !!signedProposal;
  const isDisclosureSigned = !!signedDisclosure;

  const onPdfSigned = docId => sessionId => {
    console.warn(
      "Signature:: onPdfSigned:: dispatch:: onApplicationSignIntercept",
      docId,
      sessionId,
    );
    dispatch(onApplicationSignIntercept(docId, sessionId));
  };

  const dataList = [
    {
      key: PDF_PROPOSAL,
      label: t("EAPP_SIGNATURE.PROPOSAL"),
      disabled: false,
      completed: isProposalSigned,
      templateSrc: templateProposal,
      templateSignRules: templateProposalSignRules,
      dataSrc: signedProposal,
      onClick: () => {
        setStep(0);
      },
      onSave: onPdfSigned(PDF_PROPOSAL),
    },
    {
      key: PDF_DISCLOSURE,
      label: t("EAPP_SIGNATURE.DISCLOSURE"),
      disabled: !isDisclosureSigned,
      completed: isDisclosureSigned,
      templateSrc: templateDisclosure,
      templateSignRules: templateDisclosureSignRules,
      dataSrc: signedDisclosure,
      onClick: () => {
        setStep(1);
      },
      onSave: onPdfSigned(PDF_DISCLOSURE),
    },
  ];

  const signFrameJSX = (dataList || []).map((item, index) => {
    if (item.completed) {
      return (
        <SignViewer
          className={clsx(styles.iframe, {
            [styles.hidden]: index !== step,
          })}
          dataSrc={item.dataSrc}
        />
      );
    }
    return (
      <SignHandler
        key={item.key}
        className={clsx(styles.iframe, {
          [styles.hidden]: index !== step,
        })}
        docID={item.key}
        dataSrc={item.templateSrc}
        signRules={item.templateSignRules}
        onSave={item.onSave}
      />
    );
  });

  useEffect(() => {
    const data = {
      roomId: socket.roomId,
    };
    webSocketInstance.emit("joinRoom", data);
  }, [socket.roomId]);

  useEffect(() => {
    webSocketInstance.on("mouseClickEventToOther", data => {
      mousePointerRef.current.style.display = "";
      mousePointerRef.current.style.left = `${data.mousePointerX}px`;
      mousePointerRef.current.style.top = `${data.mousePointerY}px`;
      mousePointerRef.current.style.display = "block";
      setTimeout(() => {
        mousePointerRef.current.style.display = "";
      }, 500);
    });

    webSocketInstance.on("scrollEventToOther", data => {
      const { contentWindow } = document.getElementsByClassName(
        "Signature_iframe__1guWa",
      )[0];
      if (contentWindow !== null || contentWindow !== undefined) {
        contentWindow.postMessage(data, "*");
      }
    });
  }, [socket.roomId]);

  const onMouseDownHandler = e => {
    mousePointerRef.current.style.display = "";
    mousePointerRef.current.style.left = `${e.clientX}px`;
    mousePointerRef.current.style.top = `${e.clientY}px`;
    mousePointerRef.current.style.display = "block";
    setTimeout(() => {
      mousePointerRef.current.style.display = "";
    }, 500);
    const data = {
      roomId: socket.roomId,
      mousePointerX: e.clientX,
      mousePointerY: e.clientY,
    };
    console.log("mouseClickEvent:: ", data);
    webSocketInstance.emit("mouseClickEvent", data);
  };

  useEffect(() => {
    // ? Stepper auto update step
    if (dataList[step].completed && step < dataList.length - 1) {
      setStep(step + 1);
      return;
    }
    if (step === dataList.length - 1) {
      history.push(`${ROUTE_EAPP}${ROUTE_SUPPORTINGDOCUMENT}/${socket.roomId}`);
      dispatch(enableNextBtnSignatureAction());
      dispatch(enableStepSupportingDocAction());
      dispatch(chnageStepNumberAction(4));
    }
  }, [application]);

  useEffect(() => {
    window.addEventListener('message', event => {
      // console.log(event.data.position);
      const data = {
        roomId: socket.roomId,
        event: "scrollEvent",
        position: event.data.position,
      };
      console.log(data);
      webSocketInstance.emit("scrollEvent", data);
    });
  }, [socket.roomId]);

  return (
    <div className={[styles.container,
    (socket.shareMode ? "ShareMode" : "NotShareMode"),
    (socket.controlMode ? "ControlMode" : "NotControlMode")
    ].join(" ")}
      onClick={onMouseDownHandler}
    >
      {/* <div className={(socket.shareMode ? "ShareMode" : "NotShareMode")}></div> */}
      {socket.shareMode && auth.identity === "agent" ? <BroadcastTopbar /> : null}
      <div className={"mousePointer"} ref={mousePointerRef}></div>

      <ApplicationSideBar
        className={styles.sideBar}
        dataList={dataList}
        step={step}
        setStep={setStep}
      />
      {signFrameJSX}
    </div>
  );
}
