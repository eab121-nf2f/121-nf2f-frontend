import { Button, Typography } from "@material-ui/core";
import React, { useState, useRef, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { Link, Redirect, Route, withRouter } from "react-router-dom";
import webSocket from "socket.io-client";

import config from "../../config/config.json";
import { BroadcastTopbar } from "../../component";
// import { Link } from "react-route-dom";
import { EAPP_SUBMIT } from "../../constant/page";
import styles from "./Submit.module.css";

const webSocketInstance = webSocket(`${config.socketHost}/eapp/submit`);

export default function Submit(props) {
  const { t, i18n } = useTranslation();
  const application = useSelector(state => state.application);
  const socket = useSelector(state => state.socket);
  const auth = useSelector(state => state.auth);
  const mousePointerRef = useRef(null);

  useEffect(() => {
    const data = {
      roomId: socket.roomId,
    };
    webSocketInstance.emit("joinRoom", data);
  }, [socket.roomId]);

  useEffect(() => {
    webSocketInstance.on("mouseClickEventToOther", data => {
      mousePointerRef.current.style.display = "";
      mousePointerRef.current.style.left = `${data.mousePointerX}px`;
      mousePointerRef.current.style.top = `${data.mousePointerY}px`;
      mousePointerRef.current.style.display = "block";
      setTimeout(() => {
        mousePointerRef.current.style.display = "";
      }, 500);
    });
  });

  const onMouseDownHandler = e => {
    mousePointerRef.current.style.display = "";
    mousePointerRef.current.style.left = `${e.clientX}px`;
    mousePointerRef.current.style.top = `${e.clientY}px`;
    mousePointerRef.current.style.display = "block";
    setTimeout(() => {
      mousePointerRef.current.style.display = "";
    }, 500);
    const data = {
      roomId: socket.roomId,
      mousePointerX: e.clientX,
      mousePointerY: e.clientY,
    };
    webSocketInstance.emit("mouseClickEvent", data);
  };

  return (
    <div
      className={[
        styles.container,
        socket.shareMode ? "ShareMode" : "NotShareMode",
        socket.controlMode ? "ControlMode" : "NotControlMode",
      ].join(" ")}
      onClick={onMouseDownHandler}
    >
      {socket.shareMode && auth.identity === "agent" ? (
        <BroadcastTopbar />
      ) : null}
      <div className={"mousePointer"} ref={mousePointerRef}></div>

      <div className={styles.centerWrapper}>
        <div>
          <Typography variant="h1">{t(`${EAPP_SUBMIT}.TITLE`)}</Typography>
        </div>
        <div>
          <Typography variant="h1" className={styles.policyNumberText}>{`${t(
            `${EAPP_SUBMIT}.POLICY_NUMBER`,
          )}: ${application.policyNumber}`}</Typography>
        </div>
        <div>
          {/* <Link to={"/eapp/landing"}>
            <Button color="primary" variant="contained">
              {t("BUTTON.SUBMIT_NEW_CASE")}
            </Button>
          </Link> */}
          <Link to={"/summary"}>
            <Button
              classes={{ root: styles.buttonGap }}
              color="primary"
              variant="contained"
            >
              {t("BUTTON.BACK_HOME")}
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
}
