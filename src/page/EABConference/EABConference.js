import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import DetectRTC from "detectrtc";
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { useHistory } from "react-router-dom";
import SignaturePad from "react-signature-canvas";
// import RTCMultiConnection from "rtcmulticonnection";

import FullScreenIcon from "../../assets/images/icons/full-screen-icon.png";
import convertTimeFormat from "../../utils/convertTimeFormat";
import styles from "./EABConference.module.css";

import MeetingWidget from "../../WebRTC/MeetingWidget";

import config from "../../config/config.json"


const RTC_INIT_SIGN = "INIT_SIGN";
const RTC_INIT_SELFIE = "INIT_SELFIE";

const RTC_FINISH_SIGN = "FINISH_SIGN";
const RTC_FINISH_SELFIE = "FINISH_SELFIE";

const RTC_CLEAR = "CLEAR";

const RTC_ACK_INIT_SIGN = "ACK_INIT_SIGN";
const RTC_ACK_INIT_SELFIE = "ACK_INIT_SELFIE";

const RTC_ACK_FINISH_SIGN = "ACK_FINISH_SIGN";
const RTC_ACK_FINISH_SELFIE = "ACK_FINISH_SELFIE";

const RTC_ACK_CLEAR = "ACK_CLEAR";

// {/* 878 start */}
// const initScreenRTCConnection = ({setIsOpenErrorPopUp}) => {
//   const screenRTCConnection = new RTCMultiConnection();
//   screenRTCConnection.dontCaptureUserMedia = true;
//   screenRTCConnection.socketURL =
//     "https://rtcmulticonnection.herokuapp.com:443/";
//   screenRTCConnection.session = {
//     screen: true,
//   };
//   screenRTCConnection.bandwidth = {
//     screen: 300,
//   };
//   screenRTCConnection.sdpConstraints.mandatory = {
//     OfferToReceiveAudio: true,
//     OfferToReceiveVideo: true,
//   };

//   screenRTCConnection.iceServers = [
//     {
//       urls: [
//         "stun:116.92.187.68:3478",
//         "stun:stun1.l.google.com:19302",
//         "stun:stun2.l.google.com:19302",
//         "stun:stun.l.google.com:19302?transport=udp",
//       ],
//     },
//   ];

//   screenRTCConnection.onstream = function(e) {
//     e.mediaElement.controls = false;
//     if (e.type === "local") {
//       e.mediaElement.style.display = "none";
//     }

//     const parentNode = screenRTCConnection.videosContainer;
//     parentNode.insertBefore(e.mediaElement, parentNode.firstChild);
//     const played = e.mediaElement.play();

//     if (typeof played !== "undefined") {
//       played
//         .catch(() => {
//           /** * iOS 11 doesn't allow automatic play and rejects ***/
//         })
//         .then(() => {
//           setTimeout(() => {
//             e.mediaElement.play();
//           }, 2000);
//         });
//       return;
//     }

//     setTimeout(() => {
//       e.mediaElement.play();
//     }, 2000);
//   };

//   screenRTCConnection.onstreamended = function(event) {
//     const mediaElement = document.getElementById(event.streamid);
//     if (mediaElement) {
//       mediaElement.parentNode.removeChild(mediaElement);
//     }
//   };

//   screenRTCConnection.onMediaError = function(e) {
//     if (e.message === "Concurrent mic process limit.") {
//       if (DetectRTC.audioInputDevices.length <= 1) {
//         alert(
//           "Please select external microphone. Check github issue number 483.",
//         );
//         return;
//       }

//       const secondaryMic = DetectRTC.audioInputDevices[1].deviceId;
//       screenRTCConnection.mediaConstraints.audio = {
//         deviceId: secondaryMic,
//       };

//       screenRTCConnection.join(screenRTCConnection.sessionid);
//     }
//   };

//   screenRTCConnection.onerror = function(e) {
//     console.error("screenRTCConnection.onerror:: e", e);
//     setIsOpenErrorPopUp(true);
//   };

//   screenRTCConnection.onEntireSessionClosed = function(e) {
//     console.error("screenRTCConnection.onEntireSessionClosed:: e", e);
//     setIsOpenErrorPopUp(true);
//   };

//   screenRTCConnection.onclose = function(e) {
//     setIsOpenErrorPopUp(true);
//   };

//   return screenRTCConnection;
// };

// const initDataRTCConnection = ({
//   setPhoto,
//   setSignature,
//   setIsOpenSelfiePopUp,
//   setIsOpenSignaturePopUp,
//   setIsOpenErrorPopUp,
// }) => {
//   const dataRTCConnection = new RTCMultiConnection();
//   dataRTCConnection.socketURL = "https://rtcmulticonnection.herokuapp.com:443/";
//   dataRTCConnection.session = {
//     data: true,
//   };
//   dataRTCConnection.iceServers = [
//     {
//       urls: [
//         "stun:116.92.187.68:3478",
//         "stun:stun1.l.google.com:19302",
//         "stun:stun2.l.google.com:19302",
//         "stun:stun.l.google.com:19302?transport=udp",
//       ],
//     },
//   ];
//   dataRTCConnection.onmessage = function(e) {
//     console.warn("dataRTCConnection.onmessage:: ", e);
//     switch (e.data.type) {
//       case RTC_INIT_SELFIE: {
//         setIsOpenSelfiePopUp(true);
//         dataRTCConnection.send({
//           type: RTC_ACK_INIT_SELFIE,
//         });
//         break;
//       }
//       case RTC_INIT_SIGN: {
//         setIsOpenSignaturePopUp(true);
//         dataRTCConnection.send({
//           type: RTC_ACK_INIT_SIGN,
//         });
//         break;
//       }
//       case RTC_CLEAR: {
//         setPhoto(null);
//         setSignature(null);
//         setIsOpenSelfiePopUp(false);
//         setIsOpenSignaturePopUp(false);
//         dataRTCConnection.send({
//           type: RTC_ACK_CLEAR,
//         });
//         break;
//       }
//       default: {
//         break;
//       }
//     }
//   };

//   dataRTCConnection.onerror = function(e) {
//     console.error("dataRTCConnection.onerror:: e", e);
//     setIsOpenErrorPopUp(true);
//   };

//   dataRTCConnection.onEntireSessionClosed = function(e) {
//     console.error("dataRTCConnection.onEntireSessionClosed:: e", e);
//     setIsOpenErrorPopUp(true);
//   };

//   dataRTCConnection.onclose = function(e) {
//     setIsOpenErrorPopUp(true);
//   };

//   return dataRTCConnection;
// };

// const initFaceRTCConnection = ({ setStartTime, setIsOpenErrorPopUp }) => {
//   const faceRTCConnection = new RTCMultiConnection();
//   faceRTCConnection.socketURL = "https://rtcmulticonnection.herokuapp.com:443/";
//   faceRTCConnection.session = {
//     audio: true,
//     video: true,
//     // screen: true,
//   };
//   faceRTCConnection.sdpConstraints.mandatory = {
//     OfferToReceiveAudio: true,
//     OfferToReceiveVideo: true,
//   };
//   faceRTCConnection.iceServers = [
//     {
//       urls: [
//         "stun:116.92.187.68:3478",
//         "stun:stun1.l.google.com:19302",
//         "stun:stun2.l.google.com:19302",
//         "stun:stun.l.google.com:19302?transport=udp",
//       ],
//     },
//   ];

//   faceRTCConnection.onstream = function(e) {
//     e.mediaElement.controls = false;
//     let parentNode = null;
//     if (e.type === "local") {
//       parentNode = document.getElementById("local-face-video-container");
//     } else {
//       setStartTime(Date.now());
//       // document.getElementById("redDot").style.display = "inline-block";
//       document.getElementById("timerRecorder").style.display = "block";
//       parentNode = faceRTCConnection.videosContainer;
//     }
//     // const parentNode = faceRTCConnection.videosContainer;
//     // const parentNode = document.getElementById("local-face-video-container");
//     parentNode.insertBefore(e.mediaElement, parentNode.firstChild);
//     const played = e.mediaElement.play();

//     if (typeof played !== "undefined") {
//       played
//         .catch(() => {
//           /** * iOS 11 doesn't allow automatic play and rejects ***/
//         })
//         .then(() => {
//           setTimeout(() => {
//             e.mediaElement.play();
//           }, 2000);
//         });
//       return;
//     }

//     setTimeout(() => {
//       e.mediaElement.play();
//     }, 2000);
//   };

//   faceRTCConnection.onstreamended = function(event) {
//     const mediaElement = document.getElementById(event.streamid);
//     if (mediaElement) {
//       mediaElement.parentNode.removeChild(mediaElement);
//     }
//   };

//   faceRTCConnection.onMediaError = function(e) {
//     if (e.message === "Concurrent mic process limit.") {
//       if (DetectRTC.audioInputDevices.length <= 1) {
//         alert(
//           "Please select external microphone. Check github issue number 483.",
//         );
//         return;
//       }

//       const secondaryMic = DetectRTC.audioInputDevices[1].deviceId;
//       faceRTCConnection.mediaConstraints.audio = {
//         deviceId: secondaryMic,
//       };

//       faceRTCConnection.join(faceRTCConnection.sessionid);
//     }
//   };

//   faceRTCConnection.onerror = function(e) {
//     console.error("faceRTCConnection.onerror:: e", e);
//     setIsOpenErrorPopUp(true);
//   };

//   faceRTCConnection.onEntireSessionClosed = function(e) {
//     console.error("faceRTCConnection.onEntireSessionClosed:: e", e);
//     setIsOpenErrorPopUp(true);
//   };

//   faceRTCConnection.onclose = function(e) {
//     setIsOpenErrorPopUp(true);
//   };

//   return faceRTCConnection;
// };
// {/* 878 end */}

const initWebRTC = async ({
  roomId,
  setIsOpenSelfiePopUp,
  setIsOpenSignaturePopUp,
  setPhoto,
  setSignature,
  setIsOpenErrorPopUp,
}) => {
  const meeting = new MeetingWidget({
    role: "PARTICIPANT",
    socketURL: config.socketHost,
    screenContainer: document.getElementById("screen-video-container"),
  });
  meeting.onmessage = function(data) {
    // console.log(data);
    // const data = JSON.parse(rawData);
    console.log(`CHECK:: ${RTC_INIT_SELFIE}` === data.type);
    switch (data.type) {
      case RTC_INIT_SELFIE: {
        console.log("I am in");
        setIsOpenSelfiePopUp(true);
        meeting.sendMessageData({
          type: RTC_ACK_INIT_SELFIE,
        });
        break;
      }
      case RTC_INIT_SIGN: {
        setIsOpenSignaturePopUp(true);
        meeting.sendMessageData({
          type: RTC_ACK_INIT_SIGN,
        });
        break;
      }
      case RTC_CLEAR: {
        setPhoto(null);
        setSignature(null);
        setIsOpenSelfiePopUp(false);
        setIsOpenSignaturePopUp(false);
        meeting.sendMessageData({
          type: RTC_ACK_CLEAR,
        });
        break;
      }
      default: {
        console.warn("FUCK U EXCEPTIONAL")
        break;
      }
    }
  };

  meeting.onNotification = (data) => {
    console.log("App.js:: onNotification::", data);
    switch (data?.type) {
      case "remote_quit_meeting": {
        meeting.showBottomNotificationBar(
          `${data.fromSocketId} quit the meeting.`,
        );
        // setIsOpenErrorPopUp(true);
        break;
      }
      case "close_meeting": {
        meeting.showBottomNotificationBar(`The meeting has been closed.`);
        // setIsOpenErrorPopUp(true);
        break;
      }
      default: {
        meeting.showBottomNotificationBar(`Some notification`);
        // setIsOpenErrorPopUp(true);
        break;
      }
    }
  };

  await meeting.start();
  // TODO: I think we should add the ACK to notify it is success?
  meeting.join(roomId);
  return meeting;
};

// const initWebRTCDataChannel = (meeting) => {
//   const dataChannel = meeting.createDataChannel();
//   return dataChannel;
// }

const EABConference = () => {
  // const [isFullScreenMode, setIsFullScreenMode] = useState(false);
  const [isOpenWaitingMessage, setIsOpenWaitingMessage] = useState(true);
  const [isOpenErrorPopUp, setIsOpenErrorPopUp] = useState(false);
  const [isOpenSelfiePopUp, setIsOpenSelfiePopUp] = useState(false
    );
  const [isOpenSignaturePopUp, setIsOpenSignaturePopUp] = useState(false);
  const socket = useSelector(state => state.socket);
  const meetingDetails = useSelector(state => state.meeting);
  const history = useHistory();
  const dragRef = useRef(null);
  const [mosueDownOffsetX, setMosueDownOffsetX] = useState(0);
  const [mosueDownOffsetY, setMosueDownOffsetY] = useState(0);
  const [photo, setPhoto] = useState(null);
  const [signature, setSignature] = useState(null);
  const [startTime, setStartTime] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const sigPad = useRef(null);


  // new code start
  const [meeting, setMeeting] = useState(null);

  useEffect(() => {
    async function waitInitalize() {
      const meeting = await initWebRTC({
        roomId: socket.roomId,
        setIsOpenSelfiePopUp,
        setIsOpenSignaturePopUp,
        setPhoto,
        setSignature,
        setIsOpenErrorPopUp,
      });
      console.warn("meetingDetails.clientName:: ", meetingDetails.clientName);
      setMeeting(meeting);
    }
    waitInitalize();
    console.log("EAB Conference");
  }, [socket.roomId]);

  useEffect(() => {
    if (!meeting) {
      return;
    }
    setTimeout(() => {
      meeting.editUser({
        name: meetingDetails.clientName,
      });
    }, 2000);
  }, [meeting])
  // new code end

  {/* 878 start */ }
  // const [screenRTCConnection] = useState(initScreenRTCConnection({setIsOpenErrorPopUp}));
  // const [dataRTCConnection] = useState(
  //   initDataRTCConnection({
  //     setPhoto,
  //     setSignature,
  //     setIsOpenSelfiePopUp,
  //     setIsOpenSignaturePopUp,
  //     setIsOpenErrorPopUp,
  //   }),
  // );
  // const [faceRTCConnection] = useState(initFaceRTCConnection({ setStartTime,setIsOpenErrorPopUp }));

  // useEffect(() => {
  //   faceRTCConnection.videosContainer = document.getElementById(
  //     "remote-face-video-container",
  //   );
  //   faceRTCConnection.openOrJoin(`${socket.roomId}_face`);
  //   screenRTCConnection.videosContainer = document.getElementById(
  //     "remote-screen-video-container",
  //   );
  //   screenRTCConnection.openOrJoin(`${socket.roomId}_screen`);
  //   dataRTCConnection.openOrJoin(`${socket.roomId}_data`);
  // }, []);

  // useEffect(() => {
  //   const timer = setInterval(() => {
  //     setCurrentTime(Date.now());
  //   }, 1000);

  //   window.addEventListener("orientationchange", orientationChangeHandler);

  //   return () => {
  //     clearInterval(timer);
  //     window.removeEventListener("orientationchange", orientationChangeHandler);
  //   };
  // }, []);

  // const orientationChangeHandler = () => {
  //   dragRef.current.style.top = `20px`;
  //   dragRef.current.style.right = `20px`;
  //   dragRef.current.style.bottom = ``;
  //   dragRef.current.style.left = ``;
  // };

  // const onDragEndHandler = e => {
  //   dragRef.current.style.top = `${e.clientY - mosueDownOffsetY}px`;
  //   dragRef.current.style.left = `${e.clientX - mosueDownOffsetX}px`;
  // };

  // const onDragStartHandler = e => {
  //   const rect = e.target.getBoundingClientRect();
  //   const x = e.clientX - rect.left; // x position within the element.
  //   const y = e.clientY - rect.top; // y position within the element.
  //   setMosueDownOffsetX(x);
  //   setMosueDownOffsetY(y);
  // };

  // const onTouchEndHandler = e => {
  //   dragRef.current.style.top = `${e.touches[0].clientY - mosueDownOffsetY}px`;
  //   dragRef.current.style.left = `${e.touches[0].clientX - mosueDownOffsetX}px`;
  // };

  // const onTouchStartHandler = e => {
  //   const rect = e.target.getBoundingClientRect();
  //   const x = e.touches[0].clientX - rect.left; // x position within the element.
  //   const y = e.touches[0].clientY - rect.top; // y position within the element.
  //   setMosueDownOffsetX(x);
  //   setMosueDownOffsetY(y);
  // };

  const takeSelfieHandler = () => {
    console.warn("takeSelfieHandler:: ");
    let video = document
      .getElementById("local-face-video-container")
      .querySelector("video");
    const canvas = document.getElementById("selfie-photo");
    canvas.style.boxShadow = "0px 0px 10px 10px #797979";
    canvas.style.opacity = "1";
    const context = canvas.getContext("2d");
    context.fillStyle = "white";
    context.fillRect(0, 0, canvas.width, canvas.height);

    if (video === null || video === undefined) {
      alert("Camera Error");
      const photoData = canvas.toDataURL("image/png");
      setPhoto(photoData);
      return;
    }
    video.style.visibility = "hidden";
    const canvasWidth = video.offsetWidth;
    const canvasHeight = video.offsetHeight;

    canvas.setAttribute("width", `${canvasWidth}px`);
    canvas.setAttribute("height", `${canvasHeight}px`);

    console.log(canvasWidth);
    console.log(canvasHeight);

    context.drawImage(video, 0, 0, canvasWidth, canvasHeight);

    // if (window.screen.orientation.type.includes("landscape")) {
    //   context.drawImage(video, 0, 0, canvasWidth, canvasHeight);
    // } else {
    //   context.drawImage(video, 0, 0, canvas.width, canvas.height);
    // }
    const photoData = canvas.toDataURL("image/png");
    setPhoto(photoData);
  };

  const retakeSelfieHandler = () => {
    const canvas = document.getElementById("selfie-photo");
    canvas.style.boxShadow = "";
    canvas.style.opacity = "0";
    const context = canvas.getContext("2d");
    context.fillStyle = "white";
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.clearRect(0, 0, canvas.width, canvas.height);
    const video = document
      .getElementById("local-face-video-container")
      .querySelector("video");
    video.style.visibility = "visible";
    setPhoto(null);
  };

  const selfieConfirmHandler = () => {
    meeting.sendMessageData({
      type: RTC_FINISH_SELFIE,
      base64: photo,
    });
    const canvas = document.getElementById("selfie-photo");
    canvas.style.boxShadow = "";
    const context = canvas.getContext("2d");
    context.fillStyle = "white";
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.clearRect(0, 0, canvas.width, canvas.height);
    setIsOpenSelfiePopUp(false);
    setIsOpenSignaturePopUp(true);
    const video = document
      .getElementById("local-face-video-container")
      .querySelector("video");
    video.style.visibility = "visible";
    setPhoto(null);
  };

  const clearSignatureHandler = () => {
    sigPad.current.clear();
    setSignature(null);
  };

  const signatureConfirmHandler = () => {
    sigPad.current.clear();
    meeting.sendMessageData({
      type: RTC_FINISH_SIGN,
      base64: signature,
    });
    setIsOpenSignaturePopUp(false);
  };

  const signBeginHandler = () => { };

  const signEndHandler = () => {
    const signatureCanvas = sigPad.current.getCanvas();
    // const context = signatureCanvas.getContext("2d");
    const signatureData = signatureCanvas.toDataURL("image/png");
    setSignature(signatureData);
  };

  // const redirectClinetHandler = () => {
  //   closeAllConnection();
  //   history.push(`/client/login/${socket.roomId}`);
  // };

  // const closeAllConnection = () => {
  //   screenRTCConnection.attachStreams.forEach(localStream => {
  //     localStream.stop();
  //   });
  //   faceRTCConnection.attachStreams.forEach(localStream => {
  //     localStream.stop();
  //   });
  //   dataRTCConnection.attachStreams.forEach(localStream => {
  //     localStream.stop();
  //   });
  //   screenRTCConnection.closeSocket();
  //   faceRTCConnection.closeSocket();
  //   dataRTCConnection.closeSocket();
  // };

  // const closeWaitingMessageHandler = () => {
  //   setIsOpenWaitingMessage(false);
  // };

  // const enterFullScreenModeHandler = () => {
  //   console.log("enterFullScreenModeHandler");
  //   const eabConference = document.getElementById("EAB-Conference");
  //   eabConference.classList.add(styles.FullScreen);
  //   setIsFullScreenMode(true);
  // }

  // const leaveFullScrrenModeHandler = () => {
  //   console.log("leaveFullScrrenModeHandler");
  //   const eabConference = document.getElementById("EAB-Conference");
  //   eabConference.classList.remove(styles.FullScreen);
  //   setIsFullScreenMode(false);
  // }
  {/* 878 end */ }

  return (
    <>
      <div className={styles.EABConference} id="EAB-Conference">
        {/* <div
          ref={dragRef}
          className={styles.FaceVideoBox}
          id="remote-face-video-container"
          draggable={true}
          onDragEnd={onDragEndHandler}
          onDragStart={onDragStartHandler}
          onTouchMove={onTouchEndHandler}
          onTouchStart={onTouchStartHandler}
        >
          <div className={styles.TimeRecorderContainer} id="timerRecorder">
            <span id="timer" className={styles.TimeRecorder}>
              {convertTimeFormat(currentTime - startTime)}
            </span>
            <div className={styles.Datetime}>{new Date().toLocaleString()}</div>
          </div>
        </div> */}
        <div className={styles.Centering}>
          <p className={styles.loadingText}>
            You are now being connected to the meeting.
          </p>
          <p className={styles.loadingText}>Please wait.</p>
        </div>
        <div
          className={styles.ScreenVideoBox}
          id="screen-video-container"
        ></div>
      </div>

      {/* 878 start */}
      <Dialog
        fullWidth
        open={isOpenSelfiePopUp}
        maxWidth={"md"}
        aria-labelledby="shareDialog-title"
        keepMounted
      >
        <DialogTitle
          variant="h1"
          display="block"
          className={styles.SelfieTitleContainer}
        >
          <Typography
            variant="h1"
            display="inline"
            className={styles.SelfieTitleText}
          >
            Agent has request for a self portrail photo
          </Typography>
        </DialogTitle>
        <DialogContent className={styles.SelfieContentContainer}>
          <div id="local-face-video-container" className={styles.CenterBox}>
            <canvas id="selfie-photo"></canvas>
          </div>
        </DialogContent>
        <DialogActions className={styles.SelfieActionsContainer}>
          {photo === null ? (
            <Button
              color="primary"
              variant="contained"
              onClick={takeSelfieHandler}
            >
              Take Photo
            </Button>
          ) : (
            <>
              <Button
                color="primary"
                variant="outlined"
                onClick={retakeSelfieHandler}
              >
                Retake
              </Button>
              <Button
                color="primary"
                variant="contained"
                onClick={selfieConfirmHandler}
              >
                Confirm
              </Button>
            </>
          )}
        </DialogActions>
      </Dialog>

      <Dialog
        open={isOpenSignaturePopUp}
        maxWidth={"md"}
        aria-labelledby="shareDialog-title"
        keepMounted
      >
        <DialogTitle
          variant="h1"
          display="block"
          className={styles.SignatureTitleContainer}
        >
          <Typography
            variant="h1"
            display="inline"
            className={styles.SignatureTitleText}
          >
            Agent has requested for your signature
          </Typography>
        </DialogTitle>

        <DialogContent className={styles.SignatureContentContainer}>
          <SignaturePad
            ref={sigPad}
            penColor="black"
            canvasProps={{
              className: styles.SignatureBox,
              style: {
                border: "1px solid #E0E0E0",
                boxSizing: "border-box",
                borderRadius: "2px",
                // width: "100%",
                // height: 270,
              },
            }}
            // onBegin={signBeginHandler}
            onEnd={signEndHandler}
          />
        </DialogContent>
        <DialogActions className={styles.SignatureActionsContainer}>
          <Button
            color="primary"
            variant="outlined"
            onClick={clearSignatureHandler}
          >
            Clear
          </Button>
          <Button
            color="primary"
            variant="contained"
            onClick={signatureConfirmHandler}
          >
            Confirm
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={isOpenErrorPopUp}
        maxWidth={"md"}
        aria-labelledby="shareDialog-title"
        keepMounted
      >
        <DialogTitle
          variant="h1"
          display="block"
          className={styles.ErrorTitleContainer}
        >
          <Typography
            variant="h1"
            display="inline"
            className={styles.ErrorTitleText}
          >
            The meeting has been disconnected!
          </Typography>
        </DialogTitle>
        <DialogActions className={styles.ErrorDialogAction}>
          <Button
            color="primary"
            variant="contained"
            // onClick={redirectClinetHandler}
          >
            OK
          </Button>
        </DialogActions>
      </Dialog>
      {/* 878 end */}
    </>
  );
};

export default withRouter(EABConference);
