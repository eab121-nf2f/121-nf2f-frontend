import { Button, TextField, Typography } from "@material-ui/core";
import ChatIcon from "@material-ui/icons/Chat";
import ShareIcon from "@material-ui/icons/Share";
import React from "react";
import { useTranslation } from "react-i18next";

import { ReactComponent as LinkIcon } from "../assets/images/icons/link.svg";
import { ReactComponent as PlusIcon } from "../assets/images/icons/plus.svg";
import { BroadcastTopbar, Fab, IconButton } from "../component";
import { FAB_ICON } from "../constant/component";
// import { shallowEqual, useSelector } from "react-redux";

function ComponentExample() {
  const { t, i18n } = useTranslation();

  const changeLanguage = event => {
    i18n.changeLanguage(event.target.value);
  };

  return (
    <>
      <fieldset>
        <legend>Language Actions</legend>
        <div>
          <button value="zh_CN" onClick={changeLanguage}>
            zh_CN
          </button>
          <button value="zh_TW" onClick={changeLanguage}>
            zh_TW
          </button>
          <button value="en_US" onClick={changeLanguage}>
            en_US
          </button>
        </div>
      </fieldset>
      <fieldset>
        <legend>Button</legend>
        <div>
          <Button color="primary" variant="contained">
            {t("BUTTON.NEXT")}
          </Button>
        </div>
        <br />
        <div>
          <Button color="primary" variant="outlined">
            {t("BUTTON.SAVE")}
          </Button>
        </div>
        <br />
        <div>
          <Button color="secondary" variant="contained">
            Accept
          </Button>
        </div>
        <br />
        <div>
          <Button color="primary" variant="text">
            BTN2
          </Button>
        </div>
        <br />
        <div>
          <Button color="secondary" variant="contained">
            Accept
          </Button>
        </div>
        <br />
        <div>
          <Button color="secondary" variant="contained" disabled>
            Disable
          </Button>
        </div>
        <br />
        <div>
          <Button color="primary" variant="text">
            BTN2
          </Button>
        </div>
      </fieldset>
      <fieldset>
        <legend>Fab</legend>
        <Fab styleType={FAB_ICON} icon={<ChatIcon />}>
          Chat
        </Fab>
        <br />
        <Fab styleType={FAB_ICON} icon={<ShareIcon />}>
          Share
        </Fab>
      </fieldset>
      <fieldset>
        <legend>IconButton</legend>
        <code>{`defaultProps = { color: "secondary" }`}</code>
        <div>
          <IconButton color="primary">
            <LinkIcon />
          </IconButton>
        </div>
        <br />
        <div>
          <IconButton>
            <PlusIcon />
          </IconButton>
        </div>
        <br />
        <div>
          <IconButton transparent>
            <PlusIcon />
          </IconButton>
        </div>
      </fieldset>
      <fieldset>
        <legend>Broadcast toolBar</legend>
        <BroadcastTopbar />
      </fieldset>
      <fieldset>
        <legend>TextField</legend>
        <TextField variant="outlined" color="secondary" value={"lol"} />
      </fieldset>
    </>
  );
}

export default ComponentExample;
