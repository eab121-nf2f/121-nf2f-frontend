import {
  Button,
  InputAdornment,
  TextField,
  Typography,
} from "@material-ui/core";
import ErrorIcon from "@material-ui/icons/Error";
import clsx from "clsx";
import React, { Component, Fragment } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import {
  createLoginError,
  loginThunkAction,
  TYPE_LOGIN_FAILURE,
} from "../../action/authAction";
import {
  changeRoomIdAction,
  startControlModeAction,
} from "../../action/socketAction";
import Modal from "../../component/Modal/Modal";
import ResetPasswordDialog from "../../component/ResetPasswordDialog/ResetPasswordDialog";
import { LOGIN_AGENT, LOGIN_CLIENT } from "../../constant/page";
import randomRoomId from "../../utils/randomRoomId";
import styles from "./Agent.module.css";

const mapStateToProps = state => ({
  error: state.auth.error,
});

const mapDispatchToProps = dispatch => ({
  onLoginError: (id, message) => {
    dispatch({
      type: TYPE_LOGIN_FAILURE,
      payload: {
        error: {
          id,
          message,
        },
      },
    });
  },
  loginHandler: async (username, password, callback) => {
    await dispatch(loginThunkAction(username, password, callback));
  },
  changeRoomIdHandler: roomId => {
    dispatch(changeRoomIdAction(roomId));
  },
  startControlModeHandler: () => {
    console.log("startControlModeAction");
    dispatch(startControlModeAction());
  },
});

class AgentLoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      isOpenResetPassword: false,
    };
  }

  changeUsernameHandler = e => {
    this.setState({ username: e.target.value });
  };

  changePasswordHandler = e => {
    this.setState({ password: e.target.value });
  };

  onSubmitValidation = (username, password) => {
    const { t } = this.props;
    if (typeof username !== "string" || typeof password !== "string") {
      return createLoginError({
        message: t(`ERROR_MESSAGE.INVALID_FORMAT`),
        id: ["username", "password"],
        status: "error",
      });
    }
    if (username.length < 1 && password.length < 1) {
      return createLoginError({
        message: t(`ERROR_MESSAGE.INVALID_USERNAME_PASSWORD`),
        id: ["username", "password"],
        status: "error",
      });
    }
    if (username.length < 1) {
      return createLoginError({
        message: t(`ERROR_MESSAGE.EMPTY_USERNAME`),
        id: ["username"],
        status: "error",
      });
    }
    if (password.length < 1) {
      return createLoginError({
        message: t(`ERROR_MESSAGE.EMPTY_PASSWORD`),
        id: ["password"],
        status: "error",
      });
    }
    return {
      status: true,
    };
  };
  clickedLogin = async () => {
    const {
      loginHandler,
      history,
      changeRoomIdHandler,
      startControlModeHandler,
      onLoginError,
    } = this.props;
    const { username, password } = this.state;

    // ? Validation
    const validationResult = this.onSubmitValidation(username, password);
    if (validationResult.status === "error") {
      onLoginError(validationResult.id, validationResult.message);
      return;
    }
    // ? Login Thunk
    await loginHandler(username, password, () => {
      history.push("/landing");
      const roomId = randomRoomId();
      changeRoomIdHandler(roomId);
      startControlModeHandler();
    });
  };

  getError = (id = null) => {
    const { error } = this.props;
    const { id: errorIdList, message } = error || {};
    // if ((errorIdList || []).indexOf(id) > -1) {
    //   return
    // }
    return message;
  };

  render() {
    const { t } = this.props;
    const ResetPassword = (
      <Modal>
        <ResetPasswordDialog
          close={() => this.setState({ isOpenResetPassword: false })}
        />
      </Modal>
    );

    return (
      <Fragment>
        {this.state.isOpenResetPassword && ResetPassword}
        <div className={styles.AgentLoginPage}>
          <div className={styles.LeftSection}>
            <p className={styles.Title}> {t(`${LOGIN_AGENT}.LOGIN`)}</p>
            <div className={styles.InputBox}>
              <TextField
                fullWidth
                variant="outlined"
                color="secondary"
                value={this.state.username}
                onChange={this.changeUsernameHandler}
                placeholder={t(`${LOGIN_AGENT}.USERNAME`)}
                error={this.getError()}
                InputProps={
                  this.getError()
                    ? {
                        endAdornment: (
                          <InputAdornment
                            className={clsx(styles.endAdornment, {
                              [styles.showError]: this.getError(),
                            })}
                            position="end"
                          >
                            <ErrorIcon />
                          </InputAdornment>
                        ),
                      }
                    : null
                }
              />
            </div>
            <div className={styles.InputBox}>
              <TextField
                fullWidth
                type="password"
                variant="outlined"
                color="secondary"
                value={this.state.password}
                onChange={this.changePasswordHandler}
                placeholder={t(`${LOGIN_AGENT}.PASSWORD`)}
                error={this.getError()}
                InputProps={
                  this.getError()
                    ? {
                        endAdornment: (
                          <InputAdornment
                            className={clsx(styles.endAdornment, {
                              [styles.showError]: this.getError(),
                            })}
                            position="end"
                          >
                            <ErrorIcon />
                          </InputAdornment>
                        ),
                      }
                    : null
                }
              />
            </div>
            <div>
              <Typography color="error" className={styles.LoginError}>
                {this.getError()}
              </Typography>
            </div>
            <p
              className={styles.ForgotPassword}
              onClick={() => this.setState({ isOpenResetPassword: true })}
            >
              {t(`${LOGIN_AGENT}.FORGOT_PASSWORD`)}
            </p>
            <div className={styles.LoginButton}>
              <Button
                fullWidth
                color="primary"
                variant="contained"
                onClick={this.clickedLogin}
              >
                {t(`${LOGIN_AGENT}.LOGIN`)}
              </Button>
            </div>
          </div>
          <div className={styles.RightSection}>
            <div className={styles.RightSectionContainer}>
              <div className={styles.TitleWrapper}>
                <p className={styles.Title}>
                  {t(`${LOGIN_AGENT}.TITLE_NORMAL`)}
                </p>
                <p className={styles.Subtitle}>
                  {t(`${LOGIN_AGENT}.TITLE_BOLD`)}
                </p>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withTranslation()(AgentLoginPage)),
);
