import {
  Button,
  CircularProgress,
  InputAdornment,
  TextField,
  Typography,
} from "@material-ui/core";
import ErrorIcon from "@material-ui/icons/Error";
import clsx from "clsx";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Redirect, withRouter } from "react-router-dom";

import {
  clientLoginThunkAction,
  createLoginError,
  tempClientAuthAction,
  TYPE_LOGIN_FAILURE,
} from "../../action/authAction";
import { changeMeetingNameAction } from "../../action/meetingAction";
import {
  changeRoomIdAction,
  startShareModeAction,
} from "../../action/socketAction";
import { getApplicationNumber } from "../../api";
import { LOGIN_CLIENT } from "../../constant/page";
import { eventEmitList } from "../../sockects/wsBroadTopbar";
import styles from "./Client.module.css";

const mapStateToProps = state => ({
  roomId: state.socket.roomId,
  clientLoginSuccessRedirectTo: state.socket.clientLoginSuccessRedirectTo,
  wsBroadTopBar: state.ws.wsBroadTopBar,
  error: state.auth.error,
});

const mapDispatchToProps = dispatch => ({
  changeRoomIdHandler: roomId => {
    dispatch(changeRoomIdAction(roomId));
  },
  tempClientAuthHandler: () => {
    dispatch(tempClientAuthAction(true, "client"));
  },
  clientLoginHandler: async (roomId, password, callback) => {
    await dispatch(clientLoginThunkAction(roomId, password, callback));
  },
  startShareModeHandler: () => {
    dispatch(startShareModeAction());
  },
  onLoginError: (id, message) => {
    dispatch({
      type: TYPE_LOGIN_FAILURE,
      payload: {
        error: {
          id,
          message,
        },
      },
    });
  },
  changeMeetingNameHandler: meetingName => {
    dispatch(changeMeetingNameAction(meetingName));
  },
});

class ClientLoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      applicationNumber: null,
      isLoading: true,
      meetingName: null,
    };
  }

  getAppNumber = async sessionId => {
    const { history } = this.props;
    const response = await getApplicationNumber(sessionId);
    const { status, message, data: responseData } = response;
    console.warn("Client:: Response Status:: ", response);
    if (status === "error" || status === 400) {
      console.warn("Client:: Response Error:: ", message);
      this.setState({
        ...this.state,
        isLoading: false,
      });
      history.push(`/404`);
      // history.push(`/eab-conference`);
      return;
    }
    console.log(responseData);
    const { applicationNumber, meetingName } = responseData;
    this.setState({
      ...this.state,
      applicationNumber,
      isLoading: false,
      meetingName,
    });
    this.props.changeMeetingNameHandler(meetingName);
  };

  componentDidMount = () => {
    const { roomId } = this.props.match.params;
    this.props.changeRoomIdHandler(roomId);

    console.warn("Client:: getAppNumber:: ", roomId);
    this.getAppNumber(roomId);
  };

  changeUsernameHandler = e => {
    this.setState({ username: e.target.value });
  };

  changePasswordHandler = e => {
    this.setState({ password: e.target.value });
  };

  onSubmitValidation = password => {
    const { t } = this.props;
    if (typeof password !== "string") {
      return createLoginError({
        message: t(`ERROR_MESSAGE.INVALID_FORMAT`),
        id: ["username", "password"],
        status: "error",
      });
    }
    if (password.length < 1) {
      return createLoginError({
        message: t(`ERROR_MESSAGE.EMPTY_PASSWORD`),
        id: ["password"],
        status: "error",
      });
    }
    return {
      status: true,
    };
  };
  loginHandler = async () => {
    const { t, onLoginError } = this.props;
    const { password } = this.state;

    try {
      const validationResult = this.onSubmitValidation(password);
      if (validationResult.status === "error") {
        onLoginError(validationResult.id, validationResult.message);
        return;
      }
      this.props.clientLoginHandler(
        this.props.roomId,
        this.state.password,
        () => {
          const data = {
            roomId: this.props.roomId,
          };
          this.props.wsBroadTopBar.emit(eventEmitList.JOIN_NSP, data);
          this.props.startShareModeHandler();
          // this.props.history.push(`/eab-conference/${this.props.roomId}`);
          this.props.history.push(`/client/name/${this.props.roomId}`);
        },
      );
    } catch (err) {
      window.alert("Join Room Failure");
      // this.props.history.push(`/client/login/${this.props.roomId}`);
      this.setState({ password: "" });
    }
  };

  getError = (id = null) => {
    const { error } = this.props;
    const { id: errorIdList, message } = error || {};
    // if ((errorIdList || []).indexOf(id) > -1) {
    //   return
    // }
    return message;
  };

  render() {
    const { t } = this.props;
    const { applicationNumber, isLoading, meetingName } = this.state;

    if (isLoading) {
      return (
        <div className={styles.Loading}>
          <CircularProgress />
        </div>
      );
    }
    return (
      <div className={styles.ClientLoginPage}>
        <div className={styles.LeftSection}>
          <p className={styles.Title}>
            {t(`${LOGIN_CLIENT}.APPLICATION_NUMBER`)}
          </p>
          {/* <div className={clsx(styles.TextColor, styles.MobileControlVisible)}>
            <p className={styles.Title}>{t(`${LOGIN_CLIENT}.TITLE_NORMAL`)}</p>
            <p className={styles.Subtitle}>{t(`${LOGIN_CLIENT}.TITLE_BOLD`)}</p>
          </div> */}
          {/* <p className={styles.Subtitle}>{applicationNumber}</p> */}
          <p className={clsx(styles.Subtitle, styles.TextColor)}>
            {meetingName || "Conference Meeting"}
          </p>
          <div className={styles.InputBox}>
            <TextField
              fullWidth
              variant="outlined"
              color="secondary"
              type="password"
              value={this.state.password}
              onChange={this.changePasswordHandler}
              placeholder={t(`${LOGIN_CLIENT}.PASSWORD`)}
              error={this.getError()}
              InputProps={
                this.getError()
                  ? {
                      endAdornment: (
                        <InputAdornment
                          className={clsx(styles.endAdornment, {
                            [styles.showError]: this.getError(),
                          })}
                          position="end"
                        >
                          <ErrorIcon />
                        </InputAdornment>
                      ),
                    }
                  : null
              }
            />
          </div>
          <div>
            <Typography color="error" className={styles.LoginError}>
              {this.getError()}
            </Typography>
          </div>
          <div className={styles.LoginButton}>
            <Button
              fullWidth
              color="primary"
              variant="contained"
              onClick={this.loginHandler}
            >
              {t(`${LOGIN_CLIENT}.BUTTON_ENTER`)}
            </Button>
          </div>
        </div>
        <div className={styles.RightSection}>
          <div className={styles.RightSectionContainer}>
            <div className={clsx(styles.TextColor, styles.MobileControlHidden)}>
              <p className={styles.Title}>
                {t(`${LOGIN_CLIENT}.TITLE_NORMAL`)}
              </p>
              <p className={styles.Subtitle}>
                {t(`${LOGIN_CLIENT}.TITLE_BOLD`)}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withTranslation()(ClientLoginPage)),
);
