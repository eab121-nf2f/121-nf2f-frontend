import { Button, CircularProgress, TextField } from "@material-ui/core";
import clsx from "clsx";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { changeParticipantNameAction } from "../../action/meetingAction";
import { LOGIN_CLIENT_NAME } from "../../constant/page";
import styles from "./ClientName.module.css";

const mapStateToProps = state => ({
  meetingName: state.meeting.meetingName,
  clientName: state.meeting.clientName,
  roomId: state.socket.roomId,
});

const mapDispatchToProps = dispatch => ({
  changeParticipantName: meetingName => {
    dispatch(changeParticipantNameAction(meetingName));
  },
});

class ClientNamePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clientName: "",
      isShowInputError: false,
      isLoading: false,
    };
  }

  changeClientNameHandler = e => {
    if (e.target.value === "") {
      this.setState({
        clientName: e.target.value,
        isShowInputError: true,
      });
    } else {
      this.setState({
        clientName: e.target.value,
        isShowInputError: false,
      });
    }
  };

  enterHandler = () => {
    if (this.state.clientName === "") {
      this.setState({ isShowInputError: true });
    } else {
      this.props.changeParticipantName(this.state.clientName);
      this.props.history.push(`/eab-conference/${this.props.roomId}`);
    }
  };

  render() {
    const { t } = this.props;
    const { isLoading, isShowInputError } = this.state;

    if (isLoading) {
      return (
        <div className={styles.Loading}>
          <CircularProgress />
        </div>
      );
    }

    return (
      <div className={styles.ClientNamePage}>
        <div className={styles.LeftSection}>
          <div className={styles.TextColor}>
            <p className={styles.Title}>Joining</p>
            <p className={styles.Subtitle}>
              <span>{t(`${LOGIN_CLIENT_NAME}.MEETING`)}</span>
              <span>{this.props.meetingName}</span>
            </p>
          </div>
          <div className={styles.InputMessage}>
            {t(`${LOGIN_CLIENT_NAME}.INPUT_MESSAGE`)}
          </div>
          <div className={styles.InputBox}>
            <TextField
              fullWidth
              variant="outlined"
              color="secondary"
              type="text"
              value={this.state.clientName}
              onChange={this.changeClientNameHandler}
              placeholder={t(`${LOGIN_CLIENT_NAME}.CLIENT_NAME`)}
            />
            {isShowInputError && (
              <p className={styles.InputErrorMessage}>
                Empty Name! Please enter your name.
              </p>
            )}
          </div>
          <div className={styles.EnterButton}>
            <Button
              fullWidth
              color="primary"
              variant="contained"
              onClick={this.enterHandler}
            >
              {t(`${LOGIN_CLIENT_NAME}.BUTTON_ENTER`)}
            </Button>
          </div>
        </div>
        <div className={styles.RightSection}>
          <div className={styles.RightSectionContainer}>
            <div className={clsx(styles.TextColor, styles.MobileControlHidden)}>
              <p className={styles.Title}>
                {t(`${LOGIN_CLIENT_NAME}.TITLE_NORMAL`)}
              </p>
              <p className={styles.Subtitle}>
                {t(`${LOGIN_CLIENT_NAME}.TITLE_BOLD`)}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ClientNamePage)),
);
