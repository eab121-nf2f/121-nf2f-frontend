import { CircularProgress } from "@material-ui/core";
import React, { lazy, Suspense } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";

import Header from "../component/Header/Header";
import ProtectedRoute from "../component/ProtectedRoute";
import {
  ROUTE_AGENT,
  ROUTE_APPLICATION,
  ROUTE_CLIENT,
  ROUTE_COMPONENT,
  ROUTE_EAPP,
  ROUTE_EXAMPLE,
  ROUTE_LANDING,
  ROUTE_LOGIN,
  ROUTE_SIGNATURE,
  ROUTE_SUBMIT,
  ROUTE_SUMMARY,
  ROUTE_SUPPORTINGDOCUMENT,
  ROUTE_UPLOADPROPOSAL,
} from "../constant/route";
import OpenExistingEApplication from "./eApplication/Summary";
import styles from "./index.module.css";
import LandingPage from "./LandingPage/LandingPage";
import AgentLoginPage from "./Login/Agent";
import ClientLoginPage from "./Login/Client";
import ClientNamePage from "./Login/ClientName";

const ExamplePage = lazy(() => import("./example"));

const EappStepper = lazy(() =>
  import("../component/ApplicationStepper/ApplicationStepper"),
);
const EappApplicationPage = lazy(() => import("./eApplication/Application"));
const EappSignaturePage = lazy(() => import("./eApplication/Signature"));
const EappSummaryPage = lazy(() => import("./eApplication/Summary"));
const EappSupportDocsPage = lazy(() =>
  import("./eApplication/SupportingDocument"),
);
const EappUploadPage = lazy(() => import("./eApplication/UploadProposal"));
const EappSubmitPage = lazy(() => import("./eApplication/Submit"));
const EappFooter = lazy(() =>
  import("../component/ApplicationFooter/ApplicationFooter"),
);

const EABConference = lazy(() => import("./EABConference/EABConference"));
const ErrorPage = lazy(() => import("./Error"));

const loadingWrapper = (
  <div className={styles.container}>
    <CircularProgress />
  </div>
);

const EAppRouter = ({ match }) => (
  <>
    <ProtectedRoute path={`${ROUTE_EAPP}`} component={EappStepper} />
    <ProtectedRoute
      path={`${ROUTE_EAPP}${ROUTE_APPLICATION}/:roomId`}
      component={EappApplicationPage}
    />
    <ProtectedRoute
      path={`${ROUTE_EAPP}${ROUTE_SIGNATURE}/:roomId`}
      component={EappSignaturePage}
    />
    <ProtectedRoute
      path={`${ROUTE_EAPP}${ROUTE_SUPPORTINGDOCUMENT}/:roomId`}
      component={EappSupportDocsPage}
    />
    <ProtectedRoute
      path={`${ROUTE_EAPP}${ROUTE_UPLOADPROPOSAL}/:roomId`}
      component={EappUploadPage}
    />
    <ProtectedRoute
      path={`${ROUTE_EAPP}${ROUTE_SUBMIT}/:roomId`}
      component={EappSubmitPage}
    />
    <ProtectedRoute path={`${ROUTE_EAPP}`} component={EappFooter} />
  </>
);

const Midway = () => (
  <Router>
    <Header />
    <Suspense fallback={loadingWrapper}>
      <Switch>
        {/* UnProtected Route */}
        <Route
          path={`${ROUTE_EXAMPLE}${ROUTE_COMPONENT}`}
          component={ExamplePage}
        />
        <Route
          path={`${ROUTE_AGENT}${ROUTE_LOGIN}`}
          component={AgentLoginPage}
        />
        <Route
          path={`${ROUTE_CLIENT}${ROUTE_LOGIN}/:roomId`}
          component={ClientLoginPage}
        />
        {/* Protected Route */}
        <ProtectedRoute path={`${ROUTE_SUMMARY}`} component={EappSummaryPage} />
        <ProtectedRoute path={`${ROUTE_LANDING}`} component={LandingPage} />
        <Route path={`${ROUTE_EAPP}`} component={EAppRouter} />
        <ProtectedRoute path={`/eab-conference/:roomId`} component={EABConference} />
        <ProtectedRoute path={"/client/name/:roomId"} component={ClientNamePage}/>
        <Redirect to={`/client/login/`} />
        {/* <ProtectedRoute path={"/test"} component={TestComponent} /> */}
        {/* EAPP Router */}
        {/* <ProtectedRoute path={`${ROUTE_EAPP}`} component={EappStepper} />
        <ProtectedRoute
          path={`${ROUTE_EAPP}${ROUTE_APPLICATION}/:roomId`}
          component={EappApplicationPage}
        />
        <ProtectedRoute
          path={`${ROUTE_EAPP}${ROUTE_SIGNATURE}/:roomId`}
          component={EappSignaturePage}
        />
        <ProtectedRoute
          path={`${ROUTE_EAPP}${ROUTE_SUPPORTINGDOCUMENT}/:roomId`}
          component={EappSupportDocsPage}
        />
        <ProtectedRoute
          path={`${ROUTE_EAPP}${ROUTE_UPLOADPROPOSAL}/:roomId`}
          component={EappUploadPage}
        />
        <ProtectedRoute
          path={`${ROUTE_EAPP}${ROUTE_SUBMIT}/:roomId`}
          component={EappSubmitPage}
        />
        <ProtectedRoute path={`${ROUTE_EAPP}`} component={EappFooter} /> */}
        <Route component={ErrorPage} />
      </Switch>
    </Suspense>
    {/* <Redirect to="/agent/login" /> */}
  </Router>
);

Midway.propTypes = {};

const defaultProps = {};

export default Midway;
