import i18n from "i18next";
// import LanguageDetector from "i18next-browser-languagedetector";
// import Backend from "i18next-xhr-backend";
import { initReactI18next } from "react-i18next";

import en_US from "./store/en_US";
import zh_CN from "./store/zh_CN";
import zh_TW from "./store/zh_TW";

const textData = {
  zh_TW: {
    translation: zh_TW,
  },
  zh_CN: {
    translation: zh_CN,
  },
  en_US: {
    translation: en_US,
  },
};
console.warn("textData:: ", textData);

i18n
  // load translation using xhr -> see /public/locales (i.e. https://github.com/i18next/react-i18next/tree/master/example/react/public/locales)
  // learn more: https://github.com/i18next/i18next-xhr-backend
  //   .use(Backend)
  //   // detect user language
  //   // learn more: https://github.com/i18next/i18next-browser-languageDetector
  //   .use(LanguageDetector)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    resources: textData,
    fallbackLng: "en_US",
    debug: true,
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
  });

export default i18n;
