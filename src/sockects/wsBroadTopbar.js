import webSocket from "socket.io-client";

import config from "../config/config.json";
import { store } from "../App";
import { 
  endShareModeAction,
  startShareModeAction,
  endControlModeAction,
  startControlModeAction,
} from "../action/socketAction";

// event for listing
export const eventListenList = {
  NEW_CLIENT_JOIN: "NEW_CLIENT_JOIN",
  CLOSE_SHARE_MODE: "CLOSE_SHARE_MODE",
  START_SHARE_MODE: "START_SHARE_MODE",
  END_CONTROL_MODE: "END_CONTROL_MODE",
  START_CONTROL_MODE: "START_CONTROL_MODE",
};

// event for emitting
export const eventEmitList = {
  JOIN_NSP: "JOIN_NSP",
  CLOSE_CLIENT_SHARE_MODE: "CLOSE_CLIENT_SHARE_MODE",
  START_CLIENT_SHARE_MODE: "START_CLIENT_SHARE_MODE",
  END_CLIENT_CONTROL_MODE: "END_CLIENT_CONTROL_MODE",
  START_CLIENT_CONTROL_MODE: "START_CLIENT_CONTROL_MODE",
};

export const wsBroadTopBar = webSocket(
  `${config.socketHost}/eapp/broadcastTopBar`,
);

wsBroadTopBar.on(eventListenList.NEW_CLIENT_JOIN, data => {
  console.log(data);
});

wsBroadTopBar.on(eventListenList.CLOSE_SHARE_MODE, data => {
  console.log(data);
  store.dispatch(endShareModeAction());
});

wsBroadTopBar.on(eventListenList.START_SHARE_MODE, data => {
  console.log(data);
  store.dispatch(startShareModeAction());
});

wsBroadTopBar.on(eventListenList.END_CONTROL_MODE, data => {
  console.log(data);
  store.dispatch(endControlModeAction());
});

wsBroadTopBar.on(eventListenList.START_CONTROL_MODE, data => {
  console.log(data);
  store.dispatch(startControlModeAction());
});
